FROM ubuntu:18.04
# TODO: switch to using minideb, a smaller debian-based Docker base image.

# TODO: figure out how to get curl to do partial transfers and then resume on failure
# Also, it'd be nice if it didn't actually have to re-download the file...
RUN apt-get update && \
    apt-get install -y curl && \
    curl -Lo vscode-1.26.deb --progress-bar --url https://go.microsoft.com/fwlink/?LinkID=760868
    # TODO once we can get the download to complete, we need to dpkg -i it!
    # TODO that might be it for the essential/basic setup... everything else can come later (like installing extensions)

# Setup a non-root user along with sudo rights
RUN export uid=1000 gid=1000 && \
    mkdir -p /home/developer && \
    echo "developer:x:${uid}:${gid}:Developer,,,:/home/developer:/bin/bash" >> /etc/passwd && \
    echo "developer:x:${uid}:" >> /etc/group && \
    mkdir -p /etc/sudoers.d/ && \
    echo "developer ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/developer && \
    chmod 0440 /etc/sudoers.d/developer && \
    chown ${uid}:${gid} -R /home/developer

USER developer
ENV HOME /home/developer

CMD [ "code" ]
