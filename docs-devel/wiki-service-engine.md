# Wiki Service Engine

A core component of ARF Wiki2; the Wiki Service Engine (WSE) is a modular task execution application that runs in the background while you use your wiki. It's primary responsibility is to enable the execution of tasks for:

- Content Enrichment
- Markup/Markdown Syntax Validation
- Git Commit Sync execution
- ...
