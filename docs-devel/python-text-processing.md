# python text processing

## Options

- [TextBlob](https://textblob.readthedocs.io/en/dev/)
    - Can generate WordNets, Spelling Correction, Language Transaction (via Google Translate), Word/Phrase Frequency, Sentiment Analysis, and more.
    - ...
