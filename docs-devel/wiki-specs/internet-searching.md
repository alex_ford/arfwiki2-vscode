# Internet Searching

[TOC]

## Overview

Typora includes a built-in context menu search capability which ARF Wiki utilizes. By default, you have the "Search Google" option. ARF Wiki installs a few more:

* "Search Amazon" - search for products via keyword just by selecting the terms in your wiki.
* "Search Wikipedia"
* "Search Google Maps"
* "Translate using Google"
* "Google Image Search"
* "Other Image Search"