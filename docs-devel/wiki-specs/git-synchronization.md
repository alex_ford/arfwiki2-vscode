# Git Synchronization

Leverages the installed Git on the host operating system.

Uses git-sync-daemon.py that runs in the background and continuously keeps things up-to-date with the git server.

Best practice is to separate "wiki data" from other types of wiki files.
