# Displaying Errors and Warnings

[TOC]

## Overview

ARF Wiki has limited ability to interact with users, and so here are a few of the "unique"/"clever" ways that I've come up with.

## Insert Into Page

### Example 1

<span style="background: yellow"> WARNING: something isn't quite right here... </span>

<span style="background: red; font-weight: bold;"> ERROR: there is a problem with this page... </span>

### Pro/Con Table

|                             FOR                              |                           AGAINST                            |
| :----------------------------------------------------------: | :----------------------------------------------------------: |
| Displays the message to the user so they can know what's wrong. | Places "temporary" content on a page which must be filtered before sending to VCS. |
|               Relatively simple to implement.                | With Typora, unsure how to indicate which pages have errors on them without risking structural damage to the file tree. |
|                                                              |                                                              |

