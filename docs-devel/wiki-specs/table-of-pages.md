# Table of Pages

[TOC]

## Overview

A special ARF Wiki markup tag that instructs the system to replace the markup with a Table of Pages according to the parameters.

## Basic Usage

```
[[TOP]]
```



## Advanced Usage

```
[[TOP]]{
    depth: 2
}
```

## Parameter Reference

...