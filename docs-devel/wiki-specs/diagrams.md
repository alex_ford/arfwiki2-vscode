# Diagrams

- [Diagrams](#diagrams)
  - [Overview](#overview)
  - [Mermaid](#mermaid)
    - [Features](#features)
    - [Example: Flowchart](#example-flowchart)
    - [Example: Sequence](#example-sequence)
    - [Example: Gantt Chart](#example-gantt-chart)
  - [Sequence.js (js-sequence-diagrams)](#sequencejs-js-sequence-diagrams)
    - [Features](#features)
    - [Example](#example)
  - [Flowchart.js](#flowchartjs)
    - [Features](#features)
    - [Example](#example)
  - [ASCII Flow / ASCII Flow 2 / ASCII Flow Infinity](#ascii-flow--ascii-flow-2--ascii-flow-infinity)
    - [Floor Plan Example](#floor-plan-example)
  - [ASCII Art Markup](#ascii-art-markup)
  - [Other External Tools](#other-external-tools)
  - [Wish List](#wish-list)
    - [Charts and Graphs](#charts-and-graphs)
    - [Computing Network Topology Diagrams](#computing-network-topology-diagrams)
    - [Floor Plan Diagrams](#floor-plan-diagrams)
    - [Mind Maps](#mind-maps)
    - [Messaging and Event Diagrams](#messaging-and-event-diagrams)
  - [See Also](#see-also)

## Overview

Typora comes with a few diagram libraries right out of the box! Additionally, there are some external tools that work well with the ARF Wiki content model which are detailed here.

## Mermaid

https://mermaidjs.github.io/

### Features

- [Flowchart Diagrams](https://mermaidjs.github.io/flowchart.html)
- [Sequence Diagrams](https://mermaidjs.github.io/sequenceDiagram.html)
- [Gantt Chart Diagrams](https://mermaidjs.github.io/demos.html)
- Loops, Styling/Coloring, Notes

Here is an online [Live Editor](https://mermaidjs.github.io/mermaid-live-editor/#/edit/eyJjb2RlIjoiZ3JhcGggVERcbkFbQ2hyaXN0bWFzXSAtLT58R2V0IG1vbmV5fCBCKEdvIHNob3BwaW5nKVxuQiAtLT4gQ3tMZXQgbWUgdGhpbmt9XG5DIC0tPnxPbmV8IERbTGFwdG9wXVxuQyAtLT58VHdvfCBFW2lQaG9uZV1cbkMgLS0-fFRocmVlfCBGW2ZhOmZhLWNhciBDYXJdXG4iLCJtZXJtYWlkIjp7InRoZW1lIjoiZGVmYXVsdCJ9fQ) for Mermaid Markdown!

### Example: Flowchart

- [ ] ==TODO== insert an example Mermaid Flowchart diagram here

```mermaid

```

### Example: Sequence

- [ ] ==TODO== insert an example Sequence diagram here

```mermaid

```

### Example: Gantt Chart

- [ ] ==TODO== insert an example Mermaid Gantt Chart diagram here

```mermaid

```

## Sequence.js (js-sequence-diagrams)

https://bramp.github.io/js-sequence-diagrams/

### Features

- Sequence Diagrams
- Solid Lines, Dashed Lines, Annotated Lines
- Note Elements
- Shows entity label at top and bottom of diagram
- "Simple" and "Hand Drawn" themes

### Example

- [ ] ==TODO== insert an example Sequence.js diagram here

```sequence

```



## Flowchart.js

http://flowchart.js.org/

### Features

- Flowchart Diagrams
- Element URL Linking
- Annotated Lines
- Start, Stop, Operation, Subroutine, Condition, InputOutput, Parallel elements
- Coloring elements and lines
- Style Classes
- Style Lines to use dashed, different widths, colors, etc.
- Less friendly to read and manipulate than the other libraries

### Example

- [ ] ==TODO== insert an example Flowchart.js diagram here

```flow

```

## ASCII Flow / ASCII Flow 2 / ASCII Flow Infinity

http://asciiflow.com/

This website is an online editor for ASCII art! It can be used to draw all sorts of things, using nothing but plain text. Use the editor to make your drawing, then copy it into a preformatted block, along with the link to the diagram for easy editing later.

https://github.com/lewish/asciiflow2

It is available as open source software, licensed under GPLv3.0. It is written primarily in JavaScript.

### Floor Plan Example

- [ ] ==TODO== insert an example floor plan created with ASCII Flow here

## ASCII Art Markup

==IDEA==

What if there was some "markup" we could use to represent arbitrary drawings, that would then be converted into ASCII Art?

`[ a box ]` is then turned into:

```asciiarmor
|-------|
| a box |
|-------|
```

Take it a step further, we could send it through something like http://ditaa.sourceforge.net/ which will create an image form.

So basically, we have a three layer idea:

[ ASCII Art Markup ] --> [ ASCII Art Diagram ] --> [ ASCII Art Image ]

Or more concisely: [ Markup ] --> [ Diagram ] --> [ Image ]

The point of the ASCII Art Markup language is to provide an abstracted syntax for users to declare their diagrams.

This first diagram shows a very basic representation of two boxes and one directed link.

`[ box1 ]--->[ box2 ]`

```
|------|       |------|
| box1 | ----> | box2 |
|------|       |------|
```



This example shows how we could support multiple lines of text in an element.

`[ box1\with\multi\lines ]`

```
|-------|
| box1  |
| with  |
| multi |
| lines |
|-------|
```



And here is how we could have different "shapes".

`( box1 )`

```
/------\
| box1 |
\------/
```



The following example shows how we could declare nested elements.

`[ box1 [ box1a ] ]`

```
|-----------|
| box1      |
|  |-------||
|  | box1a ||
|  |-------||
|-----------|
```



In this next example, we declare a directed link between the interior `box1a` with the exterior `box2`.

`[ box1 [ box1a ]--->#box2 ][ box2 ]`

```
                 |------|
|-------------|  | box2 |
| box1        |  |------|
|  |-------|  |      ^
|  | box1a |---------|
|  |-------|  |
|-------------|
```



While not shown above, it's implied that the following also exist:

`<---` for the opposite direction of a directed link.

`<-->` for a bidirectional directed link.

`----` for a non-directional link, with line style: solid

`/ box1 \` creates an trapezoid shape with the top being smaller than the bottom.

`\ box1 /` creates a trapezoid shape with the top being larger than the bottom.

`/ box1 /` creates a parallelogram slanted to the right.

`\ box1 \` creates a parallelogram slanted to the left.

`{ box1 }` creates a 6-pointed start shape.

`>>` or `<<` creates a different arrow tip.

`>>>` or `<<<` creates a different arrow tip.

`---o` creates an 'interface' arrow tip.

`--<>` creates an 'generalization' arrow tip.

`--[]` creates an '...' arrow tip.

`---=` creates a '...' arrow tip.

`~~~~` creates a different line style: dashed

`>-->` ...

`>->->-` ...

`>---` ...

`====` creates a different line style: thick solid

`....` or `****` creates a different line style: dotted

`--some text goes here-->` labels the line with the specified text. Double up the line markup and affix/append the arrow tip markup as desired.

Some of the rules/guidelines/objectives:

- ASCII Art Markup is written on a single line, from left-to-right. (Single dimension.)
- Each line should represent as much of a single data flow as possible, although ultimately it doesn't really matter. (You could specify every two entities connected on separate lines.)
- Multiple lines are joined together to create the 2D diagram with multiple connectors and such between entities. `[ box ]` on the first line is the same as `[ box ]` on every line that follows.
- Conversion into a diagram of 2D is done automatically based on the 1D Markup.
- Not suitable for everything, but can definitely do:
  - NoUML Diagrams
  - Floor Plans
  - Directed (and Undirected) Acyclic Graphs (DAGs/UAGs)
  - Maps
  - Network Topology Diagrams
    - `( Internet )<-->[ ASUS 802.11ac Router ]<-->[ Synology DISKSTATION [ Docker [ git-server ] [ web-server ] [ elk ] ] ]`
    - `[ Synology DISKSTATION ]<-->[ VIRGON ]`
    - `[ Synology DISKSTATION ]<-->[ SGS8+ ]`



==IDEA==

## Other External Tools

You can of course use any diagramming tool that you wish and import a static image file, or SVG, into the wiki! Here are some recommendations:

- https://textik.com - Another Online ASCII Art drawing webapp.
- http://stable.ascii-flow.appspot.com/#Draw - The older version of the ASCII Flow online drawing webapp.
- http://asciiflow.com/ - as mentioned above, this is a pretty good one.
- https://www.draw.io/
  - Online flowchart and general diagramming tool.

## Wish List

### Charts and Graphs

Line, Bar, Straight, Curved, etc.

...

### Computing Network Topology Diagrams

Symbols and icons representing common entities that reside in a network, along with connectors that sufficiently describe the interaction between them.

...

### Floor Plan Diagrams

See [ASCII Flow](#ascii-flow) above for a way to use ASCII Art to make due for now.

...

### Mind Maps

( root ) ----> ( idea A ) ----> ( idea A1 )

( idea A ) ----> ( idea A2 )

( idea A ) ----> ( idea A3 ) ----> ( idea A3A)

( idea A3A ) ----> ( idea A3A1 )

( root ) ----> ( idea B )

( root ) ----> ( idea C )

```
( idea B ) <----                ---> ( idea A3 ) ---> ( idea A3A ) ---> ( idea A3A1 )
                \              /
              ( root ) ----> ( idea A ) ----> ( idea A1 )
                  /            \
  ( idea C ) <----              ----> (idea A2 )
```



### Messaging and Event Diagrams

...

## See Also

[SVG Support](./svg-support.md)
