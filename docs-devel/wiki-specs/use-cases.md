# Use Cases

[TOC]

## Overview

ARF Wiki is a powerful and flexible collection of software apps and tools which can be used in many different ways. Here are a few:

## As a Personal Knowledge Base

...

## As a Personal Information Manager

...

## As a Personal Assistant

...

## As a Notebook and Note Taking tool

...

## As a tool for Project Planning and Management

...

## As a Todo/Task List

...

## As reusable Checklists

...

## As an Event Planning tool

...

## As a Diary/Journal

...

## As a Life Log

...