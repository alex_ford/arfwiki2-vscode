# Wiki Quality Score

[TOC]

## Overview

...

## Rules and Metrics

| Rule/Metric                                                  | Rubric                                                       | Rationale                                                    |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| Count the number of "dead links" in the wiki.                | Less is better.                                              | Dead links are links which don't resolve to anything. They can either be to internal resources, or external resources. |
| Count the number of "internal links" in the wiki.            | More is better.                                              | Internal links indicate connections between topics and a more cohesive knowledge base. |
| Count the number of "external links" in the wiki.            | More is better.                                              | External links indicate connections to resources outside of this wiki. It's good to cite external sources. |
| Count the number of pages with less than 250 words.          | Fewer pages is better.                                       | Lots of pages with very little content isn't very useful.    |
| Count the number of pages with more than 2000 words.         | Fewer pages is better.                                       | Lots of pages with too much content is also not very useful. There is an ideal "in-between". |
| ? image to diagram ratio ?                                   |                                                              |                                                              |
| Count the number of pages in a section.                      | >20 is a warning<br />>30 is an error                        | Too many pages per section can indicate lack of specificity in organization. |
| How close do each of your pages look to the recommended base layout? | 1-5                                                          | Pages can generally look anyway you want them to, but this doesn't make them good. Pages that contain 1 or more of the recommended base layout are considered "better." |
| Count the number of words in a single sentence.              | >20 words is a warning                                       | Grammar.                                                     |
| Count the number of words in a single paragraph.             | >200 words is a warning                                      | Grammar.                                                     |
| Spell checker.                                               | Each misspelled or unidentified word counts against your page. |                                                              |
| Usage of the `<kbd>` HTML tag to represent keyboard buttons.<br /><kbd>Ctrl</kbd> |                                                              | Visual styles like this help readability.                    |
| Local link references/URIs are relative and not absolute     | Each absolute local link is a warning.                       | Local links generally mean that content will only be available on the specific system(s) where the absolute link is fixed. Relative links are better for portability, and so are highly preferred |
| Do console output code fences use recommended prompt and other syntax conventions? | Warning for each code fence that isn't compliant.            |                                                              |
| Similar data tables use the same default ordering and have all of the same fields. | -1 for each table that doesn't follow the guideline. In practice, this results in a minimum penalty of -2. (-1 for both tables in the comparison.) | Using similar structure (i.e. having the same fields) and being in a similar default ordering makes data tables more consistent and easily comparable at a glance. Sorting and filtering are "view" operations and do not count in violating this guideline. All tables are stored in their "natural ordering" when saved to file and when displayed on the page at initial loading. |