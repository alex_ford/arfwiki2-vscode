# Known Issues and Limitations

[TOC]

## Overview

This page lists all of the known issues currently in ARF Wiki.

## Auto Save causes focus to jump to the top

...

## AutoKey does not completely remove all of the trigger text

...

## Following links opens in new window instead of existing one

...

## Can't move pages from within Typora

...