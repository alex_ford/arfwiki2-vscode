# Tips and Tricks

[TOC]

## Overview

...

## Quick Hot Key Reference

| Function                                         | Hot Key                                                    | Provider |
| ------------------------------------------------ | ---------------------------------------------------------- | -------- |
| Selection to hyperlink using clipboard contents. | <kbd>Ctrl</kbd>+<kbd>K</kbd>                               | Typora   |
| Selection to key symbols.                        | <kbd>Ctrl</kbd>+<kbd>Shift</kbd>+<kbd>K</kbd>              | AutoKey  |
| Select all.                                      | <kbd>Ctrl</kbd>+<kbd>A</kbd>                               | Typora   |
| Select line/sentence.                            | <kbd>Ctrl</kbd>+<kbd>R</kbd>                               | Typora   |
| Select styled scope.                             | <kbd>Ctrl</kbd>+<kbd>E</kbd>                               | Typora   |
| Insert new row in table.                         | <kbd>Ctrl</kbd>+<kbd>Enter</kbd>                           | Typora   |
| Copy selection.                                  | <kbd>Ctrl</kbd>+<kbd>C</kbd>                               | Typora   |
| Copy as Markdown.                                | <kbd>Ctrl</kbd>+<kbd>Shift</kbd>+<kbd>C</kbd>              | Typora   |
| Copy as HTML.                                    | (None)                                                     | Typora   |
| Copy line of code.                               | <kbd>Ctrl</kbd>+<kbd>R</kbd>, <kbd>Ctrl</kbd>+<kbd>C</kbd> |          |
| Paste.                                           | <kbd>Ctrl</kbd>+<kbd>V</kbd>                               | Typora   |
| Paste as plain text.                             | <kbd>Ctrl</kbd>+<kbd>Shift</kbd>+<kbd>V</kbd>              | Typora   |
| Jump to top of page.                             |                                                            |          |
| Jump to bottom of page.                          |                                                            |          |
| Go to today's general log stream page.           |                                                            |          |

## Hyperlink Selection with URL on Clipboard

Type out the text you want in full, then find the URL you want to link up, select the text to be linked, and then press <kbd>Ctrl</kbd>+<kbd>K</kbd>. Typora will automatically convert the text into a Markdown link with the clipboard contents as the URL.

Example: ...

## Convert Key Sequence into Key Symbols

Type out the keyboard sequence you want, then select the entire sequence, and press <kbd>Ctrl</kbd>+<kbd>Shift</kbd>+<kbd>K</kbd>. This causes AutoKey to parse the selection and insert `<kbd>` tags around each key.

For example, the above keyboard sequence was initially written as: `Ctrl+Shift+K` before being turned into <kbd>Ctrl</kbd>+<kbd>Shift</kbd>+<kbd>K</kbd>.

AutoKey is the one actually doing this operation, so you must have that installed, along with the script enabled for this to work. (It's therefore, currently, only available on Linux systems since an equivalent AutoHotkey script hasn't been created yet.)

==FUTURE== 

## Open Today's Log Page

### Keyboard Shortcut

...

### Run the command line script

Windows

```powershell
PS1$> .\arfwiki-src\open-todays-log.ps1
```

Linux

```bash
$> ./arfwiki-src/open-todays-log.sh
```

Python Script

```shell
$> ./arfwiki-src/open-todays-log.py
```



### Click the Action Link on pages that include it

[ARF Wiki Home Page](../home.md)

