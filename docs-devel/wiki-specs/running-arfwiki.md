# Running ARF Wiki

The user can simply open Typora and browse to the root directory of the wiki.

But, a run script can also be provided that ensures Typora opens correctly, pointing at the root directory of the wiki.

```shell
$> typora /path/to/wiki/data
```

