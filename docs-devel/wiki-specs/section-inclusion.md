# Section Inclusion

This might be redundant with [Workspaces](./workspaces.md).

Basically, this feature allows individual instances of the Wiki to only download certain sections from the Git Repo.