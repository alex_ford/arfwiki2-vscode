# AutoKey and AutoHotkey

[TOC]

## Overview

AutoKey is a Linux application that allows you to store small routines, written in Python, that can do things like replace text when detected.

AutoHotkey is a Windows application that does the same, but is also quite a bit more powerful. It's essentially a Windows Automation Engine, with it's own AHK scripting language.

ARF Wiki includes a number of starter configs and scripts to help these apps compliment ARF Wiki.

## Hotstrings

| Hotstring   | Description                                   | Example              |
| ----------- | --------------------------------------------- | -------------------- |
| `-d-`       | Inserts current date.                         | 2018-08-21           |
| `-dt-`      | Inserts current date and time.                | 2018-08-21 17:13     |
| `-ds-`      | Inserts current date, in short form.          | 20180821             |
| `-dts-`     | Inserts current date and time, in short form. | 20180821T1714        |
| `-today-`   | Inserts today's date string.                  | Tuesday Aug 21, 2018 |
| -author-    |                                               |                      |
| -email-     |                                               |                      |
| -website-   |                                               |                      |
| -copyright- |                                               |                      |
|             |                                               |                      |

## Hot Keys

These are key bindings that when pressed activate a certain feature. Sometimes, they operate on a particular selection, or have polyglot behavior depending on context.

| Description                       | AutoKey (Linux)                               | AutoHotkey (Windows) |
| --------------------------------- | --------------------------------------------- | -------------------- |
| Wrap selection with `<kbd>` tags. | <kbd>Ctrl</kbd>+<kbd>Shift</kbd>+<kbd>K</kbd> | ==Not Implemented==  |
|                                   |                                               |                      |
|                                   |                                               |                      |

### AutoKey (Linux) Script Snippets

```python
# name: Wrap Selection with <kbd>
# Hotkey: Ctrl+Shift+K
text = clipboard.get_selection()
keyboard.send_key("<delete>")
# if selection is separated by + signs,
# treat as separate keyboard keys
terms = text.split("+")
output = ""
for t in terms:
    if output != "":
        output = output + "+"
    t = t.strip() # remove any residual whitespace
    output = output + "<kbd>" + t + "</kbd>"
keyboard.send_keys(output + " ")
```

