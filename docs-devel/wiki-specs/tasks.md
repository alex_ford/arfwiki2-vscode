# Tasks

[TOC]

The wiki can be used as a powerful task manager!

## General Syntax

Tasks start with the traditional markdown syntax for a checkbox, which is: `- [ ]`

After that, each part of the line represents something meaningful to the ARF Wiki Task Parser.

## Task Title/Subject

Keep this short and concise. Less than 140 characters is recommended! The title/subject appears immediately after the markdown checkbox syntax.

```markdown
- [ ] Title of my task
```



## Assignees

Assignments can be done by using the @ prefix, followed by the assignees name.

```markdown
- [ ] Title of my task @alex
```

Or their email address.

```markdown
- [ ] Title of my task @arf4188@gmail.com
```

Multiple assignees are also supported:

```markdown
- [ ] Title of my task @alex @erik @zazu
```

## Assignee References

When tagging someone using their name (e.g. `@alex`) ARF Wiki treats this as a person reference. Person references should be unique throughout the entire wiki. They are essentially links to more information about the person.

... TBD

```
@alex
	full-name: "Alex Ford"
	email: "arf4188@gmail.com"
	phone: "(617) 123-4567"
	address: "Boston, MA"
```

## Dates

### Due Date

Set Due Dates using the // prefix.

```markdown
- [ ] Title of my task //20180801
```

```markdown
- [ ] Title of my task //20180801T1300EST
```

### Start By Date

...

## Effort

### Progress Tracking

...

### Pomo/Chunk Tracking

...

## Priority

...

## Estimation

...

## Metadata

ARF Wiki can store metadata about your tasks which are "hidden" in the Typora editor, but are available to the processing scripts and daemons.

date-completed

date-created

tracker-links

## External Tracker Links

There are dozens of trackers available out there, and ARF Wiki does it's best to understand that and help integrate with them. Declare a tracker link by using the & prefix.

```
- [ ] Title of my task &https://jira.com/MTHN/MTHN-123
```

Tracker links can be clicked on to open the corresponding page on the tracker's website.

You can also combine the Linker/Linking capabilities here:

```
- [ ] Title of my task &MTHN-123
```

The above will be replaced with the full URL once the Linker Engine executes.

### API Integrations

UNDER INVESTIGATION

It may be possible to accomplish some limited "API Integration" with certain trackers. For example, Atlassian JIRA has a fairly well documented API, so it should be possible to do things like synchronize if a task is complete or not.

## Projects

Break up projects into separate sections.

## Searching and Filtering

==TBD==

- [ ] ==TODO== need a dirt simple way to collect all Tasks in the wiki

## Subtasks

Subtasks are supported, and are simply nested Tasks!

```
- [ ] A Parent Task @alex
	- [ ] A Level 1 Child Task @erik
		- [ ] A Level 2 Child Task @alex
```

## Dependencies

Tasks can be dependent on other tasks in a few different ways:

Sequential - Task A must be completed before Task B

==Others?==

Start Constrained - Task B cannot start until Task A starts.

End Constrained - Task B cannot finish until Task B finishes.

==How does the user declare these?==

## Repeating Tasks

Tasks can be repeating, in which case, the Task Engine will duplicate the task according to the configuration of that task.

Spawn - The next occurrence is created when the current one is completed.

Fixed Date - The next occurrence is created on a specific date, some number of days before that date.

...

==How does the user declare these?==

## Reminders

Users can set reminders on tasks.

==TBD==

See [Reminders on the Other Feature Ideas](other-feature-ideas.md) page.

## Notes

Notes can be added to tasks in two ways: 1) you can simply insert them directly below the task, on a newline. 2) you can create a dedicated Task Page for the task.

==TBD==

### Type 1

Type out your tasking on the first line. Then hold SHIFT and press ENTER to insert a new line without auto-starting the next task. Now type `>` followed by some text. Typora should quickly recognize this as a Quoted text area for your note to live.

- [ ] My Task With Notes

  > Notes notes notes. Insert notes for a task using the Quote markdown syntax.

### Type 2

Type out the tasking on the first line, surround the Title with the Markdown hyperlink syntax [ ] followed by (). Leave the parenthesis empty. The Linker Engine will detect this and automatically create a new page for your task.

- [ ] [My Task With Note Page]()