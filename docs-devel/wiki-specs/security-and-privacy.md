# Security and Privacy

[TOC]

## Overview

==TBD==

## Using ARF Wiki's Integrated Encryption

### Option 1: ...

## Using VeraCrypt with an Encrypted File/Volume

VeraCrypt is a great choice if you want to have more explicit control over access to your wiki files. Using VeraCrypt, you would essentially create one or more encrypted file containers. This is what you would end up checking into Git. (Maybe...) When you needed to access them, you'd load up VeraCrypt and unlock the encrypted file containers which will reveal the contents to ARF Wiki's engine(s).

You can also use VeraCrypt to simply encrypt an entire device or partition, and keep the entire wiki on that device/partition. While you wouldn't have granular control over the encryption of individuals pages and sections, you would get an effective way to protect the entire wiki in one fell swoop.

## Using the Instant Redaction feature

Instant Redaction is actually a visual trick using colors. ARF Wiki can quickly change the color of your typed text to equal the background color of the page, therefore making it impossible for someone to simply look over and see what was there.

ARF Wiki can use some kind of visual cue to help indicate that there is redacted content present.

Revealing the content could be as easy as highlighting it, or hovering over each word you want to see, etc.

The next line is an example of some redacted text:

<span style="color: #000000; background: #000000; border: 1px; border-style: solid; border-color: #FFFFFF;">Redacted! Can you see me?</span>