# Properties Box

[TOC]

## Overview

These are special elements added to pages which define key-values that the wiki engine can use.