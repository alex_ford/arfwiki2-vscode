# Wiki Conventions

[TOC]

## Overview

This page documents the conventions employed across ARF Wiki. It's important that a good number of these be followed, because it is how certain behaviors are defined and made to work.

## Conventions

Since this "wiki" is not supported by any software that specifically enforces wiki-like behavior or features, a lot of things will be done by convention.

### Pages

* Page filenames should always be lowercase and separated with hyphens.
  * Examples:
    * "Wiki Conventions" -> "wiki-conventions.md"
    * "Alex's Quotes" -> "alexs-quotes.md"
    * "ABC is an Acronym" -> "abc-is-an-acronym.md"
* Pages should display their canonical name as their title/H1 element as the first item on the page.
* There should be no other use of the H1 element on the page beyond the title.
* The next element on the page should be the Navigation Trail widget.
* The next element should be the Related Pages widget.
* The next element should be the Table of Contents widget.

### Sections

* Sections are folders that contain one or more Markdown pages.
* Section names are made to be all lowercase, no punctuation, and spaces replaced by hyphens. (Same as how page names are processed.)
* Sections should always contain a "home.md" file which represents the base page for the section. This is the only page where the title/H1 of the page does not map to the filename. (See Home Page below.)

### Home Page

* Each section has one page that is an exception to the above rules: the Home Page.
* The home page's name is always: `home.md`
* The home page's H1 is always the name of the section, converted to user-friendly form. (e.g. `some-page-name.md` becomes `Some Page Name`)
* If the home page has page metadata, the `page-slug` and `page-name` fields will be used.
* Links to the home page are always the name of the section.

### Special Sections

- There are a handful of Special Sections which ARF Wiki treats differently and applies slightly different rules.
- Special Sections cannot be deleted. (Attempting to do so will simply cause the wiki to restore them from version control, or from the scaffold.)
- Special Sections still have Home Pages. The initial content of these pages will be auto-generated and will include some details specific to why the section is "special". You are free to add/remove text from these pages if you wish.

| Special Section Name | Description                                                  |
| -------------------- | ------------------------------------------------------------ |
| logs                 | The logs special section is where daily chronological log pages are created. These are organized into a section hierarchy that breaks down the pages by YEAR > MONTH > DATE.md |
| options              | The Options Special Section is where all of the Option Pages live. These are special Markdown files which contain configuration settings that allow you to control how the wiki behaves. |
| people               | The People Special Section is where all People Pages live, and are automatically indexed and auto-linked to the People Tag (`@`) when you use it on a page. |
| pim                  | The Personal Information Management section is a general area for you to store sensitive, potentially private, information like passwords, credit card details, logins, etc. ARF Wiki enforces stricter policies over the sections and pages within, namely that they must always use the wiki's encryption feature. (You cannot remove the encryption, and you cannot create a page without it, while within the PIM Special Section.) |
| unfiled              | The Unfiled Special Section is simply a place for generated/created pages to go when the wiki doesn't know where else to put them. |
| wiki-specs           | The Design Docs and Specifications for the ARF Wiki project! |

### Console and Terminal Blocks

#### Windows

For Windows, the preferred terminal is **PowerShell**. You'll find most non-Python scripts will have a PowerShell variant in addition to others. These have the extension `.ps1`.

When using a code fence, specify the `powershell` language type.

```powershell
PS1 $> pwd
C:\Users\user\
```

#### Linux

For Linux, the preferred terminal is **Bash**. You'll find most non-Python scripts will have a Bash variant in addition to other variants. These end with the extension `.sh`.

When using a code fence, specify the `bash` language type.

```bash
user@host $> pwd
/home/user/
```

#### Condensed Text

Often, especially when providing the output of a console command, there is more text than is really needed. Use the `...` expression to indicate that text has been condensed for brevity's sake.

#### Summarized/Paraphrased Output Text

Instead of just omitting the text, sometimes you want to provide the "general idea" of what happened, but there's still too much text to leave in there. In this case, surround your summary text with the `...` expression, like so:

```bash
... this is a summary ...
```

#### Repeated Input Lines

When issue the same command several times in a row...

- [ ] ==TODO==

#### Comments

DO provide in-line comments for code within code fences. When providing comments, use the language's comment syntax.

Windows (Batch): `REM`

Windows (PowerShell): `#`

Linux (Bash): `#`

Python: `#`

Java: `/* */`

### Separate Input Lines from Output Lines

Only put <u>input lines</u> OR output lines inside a given code fence. Avoid mixing input and output lines, or doing anything that would otherwise cause a verbatim execution to fail.

#### Example

```bash
ls -la
```

```output
total 5636
drwxr-xr-x 45 alex alex    4096 Aug 24 09:47  .
... condensed ...
drwx------  2 alex alex    4096 Aug 23 22:31  .ssh
... condensed ...
```

### Copy-and-execute of Code Fences

One of the features here that is mostly made possible via convention is "copy-and-execute" of code. There are two basic versions of this:

#### By Line

Copy-and-execute by Line is when the user selects a single line of code within a code fence and then pastes that into a terminal or IDE in order to execute.

#### By Fence

Copy-and-execute by Fence is when the user selects the entire code fence and then pastes it into a terminal or IDE in order to execute.

#### General Goals

- User should *not* have to modify the copied code in order to remove extraneous text under normal use cases.
  - Exceptions: variables/placeholders are expected to be replaced by the user.
- User should be able to use the <kbd>Ctrl</kbd>+<kbd>R</kbd> hot key to select a code line.
- User should be able to use the <kbd>Ctrl</kbd>+<kbd>A</kbd> hot key to select the entire code fence.