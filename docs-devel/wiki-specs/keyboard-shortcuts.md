---
​---
layout: post
title: rock star
​---
---

See https://support.typora.io/Custom-Key-Binding/

## Typora Default

| Action/Command                      | Key Binding                           | Remarks                                                      |
| ----------------------------------- | ------------------------------------- | ------------------------------------------------------------ |
| New Page                            | <kbd>Ctrl</kbd>+N                     |                                                              |
| "Open Quickly", Search by page name | <kbd>Ctrl</kbd>+P                     |                                                              |
| "Jump to Top"                       | <kbd>Ctrl</kbd>+Home                  |                                                              |
| "Jump to End"                       | <kbd>Ctrl</kbd>+End                   |                                                              |
| "Switch Between Opened Documents"   | <kbd>Ctrl</kbd>+Tab                   |                                                              |
| "Hyperlink"                         | <kbd>Ctrl</kbd>+K                     |                                                              |
| Toggle Show/Hide Sidebar            | <kbd>Ctrl</kbd>+Shift+L               |                                                              |
| Show Find panel                     | <kbd>Ctrl</kbd>+F                     | Only works on the current Page.                              |
| Show Find and Replace panel         | <kbd>Ctrl</kbd>+H                     | Only works on the current Page.                              |
| Insert a new table                  | <kbd>Ctrl</kbd>+T                     |                                                              |
| Set heading level                   | <kbd>Ctrl</kbd>+1 - <kbd>Ctrl</kbd>+9 |                                                              |
| Toggle a code fence                 | <kbd>Ctrl</kbd>+Shift+K               |                                                              |
| Toggle block quote                  | <kbd>Ctrl</kbd>+Shift+Q               |                                                              |
| Toggle math block                   | <kbd>Ctrl</kbd>+Shift+M               |                                                              |
| Select Styled Scope                 | <kbd>Ctrl</kbd>+<kbd>E</kbd>          | Useful when selecting code that is displayed inline with non-code text. |
| Quick open a page by name.          | <kbd>Ctrl</kbd>+<kbd>P</kbd>          | Typora presents a small text input that features auto-complete for page names. Pressing Enter opens that page in a new Typora Window. |

## Customized

| Action/Command                       | Key Binding                                   | Remarks                                                      |
| ------------------------------------ | --------------------------------------------- | ------------------------------------------------------------ |
| "Reveal in Sidebar"                  | <kbd>Ctrl</kbd>+<kbd>Shift</kbd>+<kbd>R</kbd> |                                                              |
| "Open File Location"                 | <kbd>Ctrl</kbd>+<kbd>Shift</kbd>+<kbd>O</kbd> |                                                              |
| Toggle a new task item ("Task List") | <kbd>Ctrl</kbd>+<kbd>Shift</kbd>+<kbd>T</kbd> |                                                              |
| Toggle checked status of task item   | <kbd>Ctrl</kbd>+<kbd>Shift</kbd>+<kbd>J</kbd> |                                                              |
| Select Line/Sentence                 | <kbd>Ctrl</kbd>+<kbd>R</kbd>                  | Useful when selecting a single line of code in a multi-line code fence. |
|                                      |                                               |                                                              |



- [ ] ==TODO== auto replace keyboard button markup with `<kbd>`

- [ ] ==TODO== need a keyboard shortcut to "Jump to Today's Page"
- [ ] ==TODO== can keyboard shortcuts be used to implement a "Go Back" and "Go Forward" feature?

