# Emojis and Symbols

[TOC]

## Overview

Also known as "emojis", these are provided by Typora and the markup always begins with a colon, followed by a word, then ending with a colon. Here are some popular examples:

## Examples

| Emoji/Emoticon                 | Markup          | Emoji/Emoticon                                               | Markup           | Emoji                | Markup                 |
| ------------------------------ | --------------- | ------------------------------------------------------------ | ---------------- | -------------------- | ---------------------- |
| :smile:                        | `:smile:`       | :timer_clock:                                                |                  | :money_mouth_face:   | `:money_mouth_face:`   |
| :smiley:                       | `:smiley:`      | :thumbsup:                                                   |                  | :moneybag:           | `:moneybag:`           |
| :sweat_smile:                  | `:sweat_smile:` | :thumbsdown:                                                 |                  | :skull:              | `:dead:`               |
| :grin:                         | `:grin:`        | :laughing:                                                   |                  | :crossed_fingers:    | `:crossed_fingers:`    |
| :blush:                        | `:blush:`       | :heart:                                                      |                  | :poop:               | `:poop:`               |
| :stuck_out_tongue_closed_eyes: |                 | :revolving_hearts:                                           |                  | :honey_pot:          |                        |
| :angry:                        |                 | :broken_heart:                                               |                  | :eggplant:           | `:eggplant:`           |
| :rage:                         |                 | :heart_eyes:                                                 |                  | :smiling_imp:        | `:smiling_imp:`        |
| :alarm_clock:                  |                 | :kissing_heart:                                              |                  | :alien:              | `:alien:`              |
| :arrow_backward:               |                 | :kiss:                                                       |                  | :ghost:              | `:ghost:`              |
| :arrow_forward:                |                 | :joystick:                                                   |                  | :muscle:             | `:muscle:`             |
| :arrows_counterclockwise:      |                 | :computer:                                                   |                  | :ok_hand:            | :q                     |
| :cry:                          |                 | :ballot_box_with_check:                                      |                  | :o:                  | `:o:`                  |
| :tired_face:                   |                 | :soccer:                                                     |                  | :heavy_check_mark:   | `:heavy_check_mark:`   |
| :confounded:                   |                 | :volleyball:                                                 |                  | :x:                  | `:x:`                  |
| :mask:                         |                 | :football:                                                   |                  | :warning:            | `:warning:`            |
| :angel:                        |                 | :baseball:                                                   |                  | :information_source: | `:information_source:` |
| :pill:                         | `:pill:`        | :bulb:                                                       | `:bulb:`         | :grey_question:      | `:grey_question_mark:` |
| :nerd_face:                    | `:nerd_face:`   | :neutral_face:                                               | `:neutral_face:` | :newspaper:          | `:newspaper:`          |
| :white_large_square:           |                 | :black_large_square:                                         |                  | :large_blue_circle:  |                        |
| :computer_mouse:               |                 | :fleur_de_lis:                                               |                  | :four_leaf_clover:   |                        |
| :label:                        |                 | :ledger::green_book::blue_book: :orange_book: :books::closed_book: :open_book: :notebook: |                  | :hammer_and_pick:    |                        |
| :package:                      |                 | :v:                                                          |                  | :bar_chart:          |                        |
| :chart:                        |                 | :chart_with_downwards_trend::chart_with_upwards_trend:       |                  | :bookmark:           |                        |

## Typora's Built-in Markdown Reference

You can always consult Typora's Built-in [Markdown Reference](file:///C:\Program%20Files\Typora\resources\app\Docs).

## Additional Resources

Microsoft's [EmojiPedia](https://emojipedia.org/microsoft/) website. 

https://www.webfx.com/tools/emoji-cheat-sheet/

- Windows Logo, Mac Logo, Linux Logo, Linux Distro Logos (Ubuntu, etc.)

:open_file_folder: :file_folder: :balance_scale: :back: :arrow_right: :confused: :construction: 

:arrow_up: Go to Top

:arrow_down: Go to Bottom

:arrow_heading_up: Go to Next Section Above

:arrow_heading_down: Go to Next Section Below

:leftwards_arrow_with_hook: Go to Last Edit Position

:arrow_left: Go Back/Previous (Page)

:arrow_right: Go Forward/Next (Page)

:arrow_upper_left: Go to Home (Page)

:soon: :cake: :calendar: :briefcase: :camera: :camera_flash: :credit_card: :atm: :heavy_dollar_sign: :

:recycle: :toilet: :shower: :clipboard: :copyright: :registered: :tm: :eye_speech_bubble: :black_square_button: :bust_in_silhouette: :busts_in_silhouette: :gear: :hash: :taxi: :heavy_minus_sign: :heavy_plus_sign: :heavy_division_sign: :heavy_multiplication_x: :id: 

:hourglass: :hourglass_flowing_sand:

:house: Home, :school: School, :office: Office, 

:e-mail: :email: :spiral_notepad: :rainbow: :rainbow_flag: :paw_prints:  :inbox_tray: :incoming_envelope: :outbox_tray: 

:printer: :card_index_dividers: :diamond_shape_with_a_dot_inside: :cyclone: :tornado: :fire: :droplet: :sweat_drops:

:tropical_drink: :beer: :beers: :tea: :coffee: :sake: :wine_glass: :champagne: :oden:

:ok: :-1: :thumbsup: :+1: :thinking: :thought_balloon: :speech_balloon: 

:canada: :uk: :brazil: :india:

:bell: :no_bell: for toggling alarms/reminders?

:sound: :loud_sound: :speaker: :mute: for alarms/reminders?

:mag_right: :mag: :flashlight: for searching? 

:paperclip: for attachments? :link: for links?

:airplane:, :car:, :bus:, :train:, :train2:, :bike:, :walking:, :taxi:, :ship:, :rocket:, :helicopter:, :kick_scooter:, :motorcycle:

:shopping_cart:

:warning: :information_source: :sos:

:aries: :taurus: :cancer: :virgo: :scorpius: :capricorn:, :pisces:, :gemini:, :leo:, :libra:, :sagittarius:, :aquarius:, :ophiuchus:

:bangbang: :interrobang: :heavy_exclamation_mark: :heavy_heart_exclamation: :grey_exclamation: :grey_question: :question:

:white_check_mark:

:white_flag: :black_flag: :rainbow_flag: :triangular_flag_on_post:

## HTML Entities

HTML entity codes are built right into HTML and Typora can display them. Here's a table of some common ones:

| Symbol   | HTML Code            | Symbol   | HTML Code  |
| -------- | -------------------- | -------- | ---------- |
| &copy;   | `&copy;` or `&#169;` | &reg;    | `&reg;`    |
| &middot; | `&middot;`           | &trade;  | `&trade;`  |
| &bull;   | `&bull;`             | &curren; | `&curren;` |
| &hearts; | `&hearts;`           | &sect;   | `&sect;`   |
| &infin;  | `&infin;`            | &deg;    | `&deg;`    |
| &iexcl;  | `&iexcl;`            | &iquest; | `&iquest;` |

This [reference link](http://entitycode.com/) is a good site to find more.

## FontAwesome

https://fontawesome.com/how-to-use/on-the-web/setup/getting-started?using=web-fonts-with-css

Provides icons/symbols for things like:

- Windows logo &f17c; 
- Linux logo <img src="blob:https://fontawesome.com/7af8a23e-3821-4e39-b291-bf6fe6d99150"/>

==TBD==