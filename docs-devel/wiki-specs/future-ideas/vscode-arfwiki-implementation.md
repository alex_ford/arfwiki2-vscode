# Vscode Arfwiki Implementation

[TOC]

## Overview

Design Docs and Specifications for a possible implementation of ARFWIKI using VS Code.

## Feature Table

### General

| Feature                                                      | Typora-based                                                 | VSCode-based                                              |
| ------------------------------------------------------------ | ------------------------------------------------------------ | --------------------------------------------------------- |
| Live rendering while editing.                                | Yes.                                                         | ???????                                                   |
| Auto-save periodically.                                      | Yes, but has bugs.                                           |                                                           |
| Auto-save on navigating away from page.                      | Yes.                                                         |                                                           |
| Emojis.                                                      | Yes.                                                         | Yes, with:<br />`Markdown Emoji`<br />`Emoji Code`        |
| Auto insert emojis.                                          |                                                              |                                                           |
| Logos.<br />Easily insert common logos for companies, brands, websites, services, etc. |                                                              |                                                           |
| Auto insert logos.                                           |                                                              |                                                           |
| Table of Contents                                            | Yes.                                                         | Yes.... ???<br />Yes, with `Markdown Extended`.           |
| Table of Child Pages                                         |                                                              |                                                           |
| Open daily log file.                                         | No - but had impl ideas using AutoKey/AutoHotKey.            | Yes, using `open-daily-memo` ext.                         |
| Quick link paste.<br />Select text, press hot key to paste link, auto-creates correct Markdown. | Yes.                                                         | ???                                                       |
| Run code fences.                                             | No - impl ideas...                                           |                                                           |
| Snippets.                                                    | No - impl ideas...                                           | Yes,                                                      |
| Auto open editor to Wiki Home page.                          | No, haven't been able to figure this one out...              |                                                           |
| Auto open editor to Wiki Data directory.                     | Yes.                                                         |                                                           |
| Spell checker.                                               | Yes, features it's own built-in checker.                     | Yes, with:<br />`SpellChecker`                            |
| Multi tab view of pages.                                     | No.                                                          | Yes.                                                      |
| Sidebar navigation of sections and pages.                    | Yes.                                                         | Yes.                                                      |
| Split view to see pages side-by-side.                        | No, BUT - since Typora opens in new windows, you can arrange the windows to be side-by-side. | Yes.                                                      |
| Distribution                                                 | Downloadable binaries for Windows, Linux, and MacOS.         | Dockerfile which can be run on Windows, Linux, and MacOS. |
| Auto heading level insert for "same-level", "sub-level", "super-level".<br />When editing a page, inserting a heading at the same level will create a new heading with the same level as the previous one. Sub-level creates it as a smaller (h2->h3, etc.) heading, and super-level creates it as a larger (h3->h2) heading. |                                                              |                                                           |

### Code

| Feature                                                      | Typora-based                                                 | VSCode-based                                                 |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| Line numbers in code fences.                                 | Yes.                                                         |                                                              |
| Run line(s) of a code fence.                                 |                                                              |                                                              |
| Run entire code fence.                                       | No.                                                          | Yes, with:<br />```Markdown Script```<br />`Run Snippets in Markdown` |
| Copy line(s) of a code fence.                                |                                                              |                                                              |
| Copy entire code fence.                                      |                                                              |                                                              |
| Pin line(s) of a code fence.                                 |                                                              |                                                              |
| Pin entire code fence.                                       |                                                              |                                                              |
| "Output" support for code fence language.<br />Output code fences could be used to represent the output from a previous code fence. | Sorta. Typora doesn't prevent you from declaring any language you want for a code fence, but it also doesn't provide anything else for undefined languages. |                                                              |

### Exporting and Publishing

| Feature                                                    | Typora-based                | VSCode-based                                                 |
| ---------------------------------------------------------- | --------------------------- | ------------------------------------------------------------ |
| Export to PDF.                                             | Yes.                        | Yes, using `Markdown PDF`.<br />Yes, using `Markdown Converter`. |
| Export to HTML.                                            | Yes.                        | Yes, using `Markdown Converter`.                             |
| Export to Document Formats (.doc, .docx, .odx, .rtf, etc.) | Yes, with Pandoc installed. |                                                              |
| Export single page, page selection, section, wiki.         |                             | Maybe? with `Markdown Extended`.                             |
| Export to PNG, JPEG, other image formats.                  |                             | Yes, using `Markdown Extended`.<br />Yes, using `Markdown Converter`. |

### Diagrams and Advanced Rendering

| Feature                                      | Typora-based | VSCode-based                           |
| -------------------------------------------- | ------------ | -------------------------------------- |
| LaTeX Math rendering.                        | Yes.         | Yes, using `Markdown+Math`.            |
| Mermaid Diagram rendering.                   | Yes.         |                                        |
| Graphviz Diagram rendering.                  | No.          | Yes, with `Graphviz Markdown Preview`. |
| Sequence.js Sequence Diagram rendering.      | Yes.         |                                        |
| Flowchart.js Diagram rendering.              | Yes.         |                                        |
| Nomnoml UML Diagram rendering.               | No.          | Yes, with `Markdown Nomnoml Support`.  |
| MindMap Diagram rendering.                   |              |                                        |
| Floorplan Diagram rendering.                 |              |                                        |
| Network Topology Diagram rendering.          |              |                                        |
| Charts and line graphs.                      |              |                                        |
| ASCII art rendering.                         |              |                                        |
| Whiteboard rendering for free form drawings. |              |                                        |
| Google Maps rendering.                       |              |                                        |
| Timeline rendering.                          |              |                                        |
| Gantt Chart rendering.                       |              |                                        |
| Theme-able rendering.                        | Yes.         | Yes.                                   |
| Theme-able UI/IDE.                           | Yes.         | Yes.                                   |

### Copy and Paste

| Feature                                                      | Typora-based                                                 | VSCode-based                                                 |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| Copy plain text from Markdown selection.                     |                                                              |                                                              |
| Copy Markdown from selection.                                |                                                              |                                                              |
| Copy HTML from selection.                                    |                                                              | Sorta? with `Markdown Extended`.<br />Yes, with `Copy Markdown as HTML`. |
| Paste CSV as Markdown table.                                 |                                                              |                                                              |
| Paste Excel as Markdown table.                               |                                                              | Yes, with `Excel to Markdown table`.                         |
| Paste HTML as Markdown.                                      |                                                              | Yes, with `Markdown Paste`.                                  |

### Links

| Feature                                                      | Typora-based                       | VSCode-based                                                 |
| ------------------------------------------------------------ | ---------------------------------- | ------------------------------------------------------------ |
| Follow links.                                                | Yes.                               |                                                              |
| Follow anchors.                                              | Yes.                               |                                                              |
| Auto linking to other local wiki pages.                      | ...                                | Yes, see below.                                              |
| Suggest links to other local wiki pages.                     |                                    | Yes, with:<br />`MarkDown Link Suggestions`<br />`wikilink4md` |
| Auto create non-existing pages on link click.                | No. Typora simply throws an error. |                                                              |
| Auto linking to favorite Internet sites like Google, Wikipedia, etc. |                                    |                                                              |
| Auto linking of email addresses, click opens default email client. |                                    | Yes, with `MarkDown Email Links`.                            |
| Auto link namespaces.<br />This allows different Sections of the Wiki to use different auto link rules. Useful, for example, if your work Section has different rules than the rest of your Wiki. | No.                                |                                                              |
| Links can open files on local file system.                   |                                    |                                                              |
| Links can launch scripts/apps on local file system.          |                                    |                                                              |

### Image Pasting and Special Paste Support

| Feature                                                      | Typora-based | VSCode-based                                                 |
| ------------------------------------------------------------ | ------------ | ------------------------------------------------------------ |
| Paste images with auto save and linking to local file system.<br />If the clipboard contains image data, it should be saved to the file system, and a markdown image link inserted. | Yes.         | Yes, with `Markdown Paste`.                                  |
| Paste special text and auto-link as appropriate.             |              | Yes, with `Markdown Paste`.                                  |
| Raw HTML support.                                            |              | Maybe, with `Extensible Markdown Converter`.                 |
| Raw CSS support. (style="css" attribute works)               | Yes.         | Sorta, with `Markdown Extended`.<br />Maybe, with `Extensible Markdown Converter`. |
| Re-flow auto formatting.<br />Auto formats selection, or whole page, to meet certain formatting specifications like being 80 characters long. |              | Yes, with:<br />``Reflow Markdown`<br />`Remark`             |
| Drag-and-drop image auto save and linking.                   |              |                                                              |
| Automatic conversion of imagery files to reduced resolution, compressed formats.<br />An optional feature that automatically converts pasted imagery files to a reduced resolution and good compression format when saving to the `assets` location. |              |                                                              |
| Control image size in rendered markdown.                     | Yes???       | Yes, with:<br />`Markdown Image Size`                        |

### Tables

| Feature                                                      | Typora-based                                                 | VSCode-based                                                 |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| Table formatting.                                            |                                                              | Yes, with `Markdown Extended`.<br />Yes, with `Markdown Table Formatter`. |
| Modify table structure visually.                             | Yes.                                                         | Maybe, , with `Markdown Extended`.                           |
| Auto-numbered index column for tables.                       |                                                              | Maybe, with `Extensible Markdown Converter`.                 |

### Visual Extras

| Feature                                                      | Typora-based                                                 | VSCode-based                                                 |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| Admonitions, call-out boxes.<br />Like INFO, WARNING, etc. boxes that are styled to call them out off the page. | No, but might be possible with HTML/CSS/snippets.            | Yes, with `Markdown Extended`                                |
| Navigation helpers (back to top, down to bottom, etc.)       |                                                              |                                                              |
| Footnotes.                                                   |                                                              | Yes, with `Markdown Extended`.<br />Yes, with `Markdown Footnotes`. |
| Nice button rendering (using kbd HTML tags?)                 | Yes.                                                         | Yes, , with `Markdown Extended`                              |
| Checkbox rendering.                                          | Yes.                                                         | Yes, with `Markdown Extended`.<br />Yes, with `Markdown Checkboxes`. |
| Renders super and sub scripts.                               | Yes.                                                         | Yes, with `Markdown Extended`                                |
| Abbreviation helper.                                         |                                                              | Yes, with `Markdown Extended`.                               |
| List hierarchy folding.                                      |                                                              |                                                              |
| Page section folding.                                        |                                                              |                                                              |
| Details tag, or collapsible text areas.                      | Sorta. The <details> HTML tag can be used in some cases, but it's a bit buggy. |                                                              |

### Search and Navigation

| Feature                                                      | Typora-based                                                 | VSCode-based |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------ |
| Go to page by name.                                          | Yes.                                                         |              |
| Search for page by name.                                     | Sorta, the `Go to page by name` feature lets you type and see suggestions. |              |
| Search for content in pages.                                 | No.                                                          |              |
| Search favorite search engines with selected text.<br />Google, Wikipedia, ... | Yes, with Google configured by default. Other search engines can be added to the configuration. |              |
| Search image using Google Image Search.                      |                                                              |              |

### Task Management

| Feature                                   | Typora-based                 | VSCode-based                |
| ----------------------------------------- | ---------------------------- | --------------------------- |
| To-Do List view that displays all To-Dos. | No, but had some impl ideas. | Yes, with `MarkDown To-Do`. |
| Task Priorities.                          |                              |                             |
| Due Dates.                                |                              |                             |
| Start Dates.                              |                              |                             |
| Dependency Links.                         |                              |                             |
| Assign using people/mentions.             |                              |                             |
| View in different calendar views.         |                              |                             |

### Content Rendering

| Feature                                                      | Typora-based               | VSCode-based                                              |
| ------------------------------------------------------------ | -------------------------- | --------------------------------------------------------- |
| Include specific content from one page on another.           |                            |                                                           |
| Include the entire content of one page on another.           |                            | Yes, with:<br />`Markdown Include`                        |
| Renders images.                                              |                            |                                                           |
| Renders videos with playback controls.                       |                            |                                                           |
| Renders audio playback controls.                             |                            |                                                           |
| Renders HTML Previews.                                       |                            |                                                           |
| Page templates.                                              |                            | Yes, with:<br />`docs-markdown`<br />`Template Generator` |
| Section templates, which define one or more pages that use Page templates. |                            | Yes, with:<br />`Template Generator`                      |
| Code fence language support.                                 | Yes, pretty good coverage. | ...                                                       |
| Web badge rendering.<br />These are the badges often seen on places like GitHub, etc. Usually used to inform the user what features/services are used, along with status. (Badge services like: https://www.shield.io and https://) |                            | Yes, with:<br />`Badgen`<br />`Badges`                    |
| Renders Presentation Display (e.g. reveal.js)                |                            | Yes, with:<br />`vscode-reveal`                           |

### Security and Privacy

| Feature                                                      | Typora-based | VSCode-based |
| ------------------------------------------------------------ | ------------ | ------------ |
| Obscure protected text visually. (e.g. passwords `********`) |              |              |
| Set individual words, sentenses, paragraphs, page sections, or whole pages to use obscured visual text. |              |              |
| Encrypt protected text.                                      |              |              |
| Encrypt protected page.                                      |              |              |
| Encrypt protected section.                                   |              |              |

### Sync and Import

| Feature                                | Typora-based       | VSCode-based                                                |
| -------------------------------------- | ------------------ | ----------------------------------------------------------- |
| Import from OneNote.                   |                    |                                                             |
| Sync with OneNote.                     |                    |                                                             |
| Import from Google Keep.               |                    |                                                             |
| Sync with Google Keep.                 |                    |                                                             |
| Import from TickTick.                  |                    |                                                             |
| Sync with TickTick.                    |                    |                                                             |
| Easily extended with new features.     | No, closed source. | Yes! VS Code is open source, but also has an extension API. |
| Import from Google Calendar.           |                    |                                                             |
| Sync with Google Calendar.             |                    |                                                             |
| Chrome Extension for "Send to ARFWIKI" |                    |                                                             |

### Git Data Repo

| Feature                                | Typora-based | VSCode-based |
| -------------------------------------- | ------------ | ------------ |
| Control over Git directly from editor. |              | Yes.         |
| One-push "git sync" operation.         |              |              |
| Auto/background "git sync" operation.  |              |              |
| Integrated view of Page history.       |              |              |
| Restore previous version of Page.      |              |              |
| Integrated view of Section history.    |              |              |
| Restore previous version of Section.   |              |              |

### Asset Storage

| Feature                                 | Typora-based | VSCode-based |
| --------------------------------------- | ------------ | ------------ |
| Integrated with Git-LFS.                |              |              |
| Storage for imagery, video, audio, etc. |              |              |

### Stream Log

| Feature                                                      | Typora-based | VSCode-based                      |
| ------------------------------------------------------------ | ------------ | --------------------------------- |
| Create new/next daily log file.                              |              |                                   |
| Insert short text into daily log file from arbitrary location. |              |                                   |
| Go to today's log file. (Create if does not exist.)          |              | Yes, with:<br />`open-daily-memo` |
| Automatically inserts date and time where needed.            |              |                                   |

### Wiki Integrity

| Feature                                  | Typora-based | VSCode-based                                         |
| ---------------------------------------- | ------------ | ---------------------------------------------------- |
| Scan all Internet links for broken ones. |              | Maybe, with:<br />`HTTP/s and relative link checker` |

### Other Features

| Feature                                                      | Typora-based | VSCode-based                 |
| ------------------------------------------------------------ | ------------ | ---------------------------- |
| Reminders                                                    |              |                              |
| External live content (from Internet)                        |              |                              |
| Beancount integration                                        |              |                              |
| People/mention tags                                          |              |                              |
| Define selected words using online dictionary                |              |                              |
| Gravitar integration for people/mentions                     |              |                              |
| Facebook integration for people/mentions                     |              |                              |
| Headers and footers on every page                            |              |                              |
| Insert clipboard watchdog<br />This feature allows the user to select a spot on the page and being a "watchdog" session where anything that shows up on the clipboard will be pasted there. |              |                              |
| Define enum list of pre-determined values.                   |              | Yes, with:<br />`ActionLock` |
| Cycle/switch between options in enum list.                   |              | Yes, with:<br />`ActionLock` |

## Leveraging Existing Extensions

There are tons of extensions already out there for VS Code, so we can likely leverage lots of them:

### Markdown Related

#### badgen

> Snippets to quickly insert Badgen badges into Markdown documents.

#### Graphviz Markdown Preview

> Adds GraphViz support to VS Code's built-in markdown preview.

#### Markdown Extension Pack

> Includes Markdown All in One, markdownlint, Markdown PDF, Markdown+Math, Markdown Preview Enhanced, Markdown TOC, VS Live Share, Markdown Table Prettifier, Markdown Emoji

- Markdown All in One

  - > All you need for Markdown (keyboard shortcuts, table of contents, auto preview, and more).

- Markdown Emoji

  - > Adds :emoji: syntax support to VS Code's built-in Markdown preview.

- markdownlint

  - > Markdown linting and style checker.

- Markdown Preview Enhanced

  - > Open source project, port of Markdown Preview Plus and RStudio Markdown.
    >
    > Includes automatic scroll sync, math typesetting, mermaid diagrams, PlantUML diagrams, pandoc, PDF export, code chunk, presentation writer, and more.
    >
    > Ability to run "code chunks".

- Markdown Table Prettifier

  - > Transforms markdown tables to be more readable.

- Markdown TOC

  - > Generate TOC (table of contents) of headlines from parsed markdown file.

- Markdown+Math

  - > mdmath allows to use Visual Studio Code as a markdown editor capable of typesetting and rendering TeX math.

#### MarkDown Jira Links

(Optional)

Highlights JIRA links automatically in Markdown files for configured projects. Configure projects in Settings with the `markDownJiraLinks.codesToUrls` key.

Example

```json
"markDownJiraLinks.codesToUrls": [
  {
    "code": "RHAEO",
    "url": "https://jira.rhaeo.net/browse/"
  },
  {
    …
  }
]
```

#### MarkDown Link Suggestions

> Suggests local files and local MarkDown file headers when typing MarkDown links URLs.

#### Markdown Navigation

> Auto generate markdown navigation panel to the activity bar.

#### MarkDown To-Do

> Collects to-do items in MarkDown files to an Explorer tree view with contextual menus for toggling and removing to-do items and keeps the tree view up-to-date as to-do items are inserted, updated, and deleted in any MarkDown file in the workspace.

#### markdown-dir

> Markdown directory navigation header generator.

#### open-daily-memo

> Provides `Open daily memo` command. It opens `YYYYMMDD.md`

Daily memo directory can be customized, but not sure if it can be done in a way which fully organizes memos into `./YYYY/MM/YYYYMMDD.md` structure.

Open Source, repo can be forked on GitHub [here](https://github.com/hitode909/vscode-open-daily-memo).

#### wikilink4md

> Supports WikiStyle `[[]]` grammar for Files in the same folder. After typing `[[` a list with all available files in folder will pop up and you can choose the correct file. <kbd>Alt</kbd>+<kbd>Enter</kbd> if marked file does not exist, it will be created.

#### Markdown Shortcuts

> Handy shortcuts for editing Markdown files. Now with title and context menu integration!

#### Markdown helper

> Apply Markdown formatting to text using keyboard shortcuts or context menu.

#### Copy Markdown as HTML

> This extension will copy the currently selected markdown text to the clipboard as HTML.

#### Markdown Paste

> A smartly paste for markdown. Supports several "smart paste" features.

#### MarkDown Link Suggestions

> Suggests local files and local MarkDown file headers when typing MarkDown links URLs.

### Git Related

#### Git History

#### Git Lens

### General Snippets

These extensions support snippets, in general... not necessarily for Markdown, but Markdown is included!

#### snippet-creator

> This extension helps to automate snippet creation. Select text you want to create snippet from and use command `Create snippet` from command palette.

## Markdown-it

This is a very popular, pervasive, open source Markdown parser and rendering engine. It is extensible, and many of the VS Code extensions either use it under the hood, or are based on it.

- [GitHub Repository](https://github.com/markdown-it/markdown-it)
- [GitHub Project Group](https://github.com/markdown-it)
- [NPM List of Markdown-it Plugins](https://www.npmjs.com/search?q=keywords:markdown-it-plugin)
- [Markdown-it API Documentation](https://markdown-it.github.io/markdown-it/)
- [Markdown-it Live In-browser Demo](https://markdown-it.github.io/)

Markdown-it can likely be used to help power new VS Code Extensions, as it seems to already be doing that for several existing ones.

Here are some of the Markdown-it Plugins that are of interest:

### Plugins of Interest

#### markdown-it-plantuml

https://www.npmjs.com/package/markdown-it-plantuml

#### markdown-it-smartarrows

https://www.npmjs.com/package/markdown-it-smartarrows

#### markdown-utils

https://www.npmjs.com/package/markdown-utils

#### markdown-it-diaspora-mention

https://www.npmjs.com/package/markdown-it-diaspora-mention

#### markdown-it-include

https://www.npmjs.com/package/markdown-it-include

## Deployment and Packaging

This could be done quite easily using Docker! The Docker image would include VS Code, a ton of the extensions mentioned above, and other needed software. The whole app would then simply launch as the default command of the Docker image.

Would probably want to customize VS Code's theme just to make it look different from other instances of VS Code.

Will need to X11 forward the graphics, so Windows users need to have an X11 server running.