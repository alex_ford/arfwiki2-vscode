# Live Content

[TOC]

## Overview

Live content is a capability that allows the user to include live content data from external sources, such as the Internet.

Use case: you're using the ticket tracking system JIRA and you want to include the ticket name of a specific ticket on one of your wiki pages. If that name changes, you'd like it to update in your wiki.

==TBD==