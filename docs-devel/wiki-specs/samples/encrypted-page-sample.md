# Encrypted Page Sample

**Navigation**

[ ARF Wiki ] > [ Wiki Specs ] > [ Samples ] > [ Encrypted Page Sample ]

**Related**

[ [Decrypted Page Sample](./decrypted-page-sample.md) ] | [ [Hidden Encrypted Page Sample](./.encrypted-page-sample.md) ]

*The content on this page is protected by encryption. Please enter the password to decrypt the contents.*