# Decrypted Page Sample

**Navigation**

[ ARF Wiki ] > [ Wiki Specs ] > [ Samples ] > [ Decrypted Page Sample ]

**Related**

[ [Encrypted Page Sample](./encrypted-page-sample.md) ] | [ [Hidden Encrypted Page Sample](./.encrypted-page-sample.md) ]

*This page is setup with encryption to help protect it's content. It will auto-encrypt after 60 seconds of inactivity, or by clicking this [ENCRYPT]() link.*

[TOC]

## Overview

This is a sample of what a Decrypted Page might look like. It is similar in outer structure to the [Encrypted Page Sample](./encrypted-page-sample.md) but with all of the content present.

In the Encrypted Page, the content is removed. The content is stored in a hidden file (e.g. `.encrypted-page-sample.md`) and this is the only data that is stored in Git. An extremely lightweight page takes it's place which informs the user that the page is encrypted, and provides the mechanism for how to decrypt it.

Decrypting a page simply involves entering the password on the page! ==TBD==

## Hidden Encrypted Page Sample

The sample page that is hidden by using the `.` filename prefix can be viewed [here](./.encrypted-page-sample.md). A simple online AES encryption form was used to generate the sample cipher-text. https://aesencryption.net/

If you decode the page with AES-128, you should get the following:

> This is some ARF Wiki content that was encrypted online using https://aesencryption.net!

## Section 2

...