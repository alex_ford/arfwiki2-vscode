# Day View Sample

< Yesterday												Tomorrow >

```properties
# Respects the following options:

# "30 minutes", "15 minutes", "2 hours", "4 hours", etc.
generated.views.dayview.granularity = "hour" 
# true or false
generated.views.dayview.twentyfourhourclock = false
```

|          | Wednesday 2018-08-22 |
| -------- | :------------------: |
| 12:00 AM |                      |
| 1:00 AM  |                      |
| 2:00 AM  |                      |
| 3:00 AM  |                      |
| 4:00 AM  |                      |
| 5:00 AM  |                      |
| 6:00 AM  |                      |
| 7:00 AM  |                      |
| 8:00 AM  |                      |
| 9:00 AM  |                      |
| 10:00 AM |                      |
| 11:00 AM |                      |
| 12:00 PM |                      |
| 1:00 PM  |                      |
| 2:00 PM  |                      |
| 3:00 PM  |                      |
| 4:00 PM  |                      |
| 5:00 PM  |                      |
| 6:00 PM  |                      |
| 7:00 PM  |                      |
| 8:00 PM  |                      |
| 9:00 PM  |                      |
| 10:00 PM |                      |
| 11:00 PM |                      |
| 12:00 AM |                      |

