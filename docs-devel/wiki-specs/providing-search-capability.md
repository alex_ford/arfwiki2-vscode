# Providing Search Capability

[TOC]

## Solr

Apache Solr, backed by Apache Lucene, is a popular and powerful open source full-text search engine that can be downloaded and deployed almost anywhere. It would take some tinkering to get it to work, but it is more than possible.

## OS File Explorer

The File Explorer of both Windows and Linux provide basic search capabilities. Could this be sufficient?

==TBC==

## True Universal Private Search

TUPS!

The reality is, I have "search" problems daily, and I've gone without any real solution. Here are just a few of the "locations" that could be searched:

* VIRGON (ASUS ZenBook 3 Deluxe UX490UAR laptop)
* Synology DISKSTATION network attached storage
* Samsung Galaxy S8+
* DropBox
* OneDrive
* Google Drive

==TBC==