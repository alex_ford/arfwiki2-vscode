# Table of Contents

[TOC]

## Overview

Every wiki page features a Table of Contents, which is mostly provided by Typora.

ARF Wiki does include some customization ability:

## Floating TOC

The TOC can be turned into a "floating" version of itself, and positioned in the upper left, upper right, lower left, or lower right.

==TBD==

Can be done using CSS?