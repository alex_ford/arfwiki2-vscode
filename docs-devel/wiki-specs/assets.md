# Assets

[TOC]

## Overview

Assets are generally non-text files that are referenced by pages in the wiki. They are stored separately from the Markdown files to avoid problems with having those kinds of files checked into a version control system like Git. Git supports a feature called Git LFS and that is one of the options for Asset Storage.

## The Problem

For those who aren't familiar with the guts of version control and why it's not a great idea to commit binary blobs, here's the short version: version control achieves a great deal of efficiency by being able to compute "deltas" or "differences" between versions of tracked files. For example, if you have a 5000 word .txt file, but you only change 100 words, the only thing the version control system needs to store is the 100 words that changed, along with some metadata that helps pinpoint where/when the change occurred.

Introduce binary blobs: these fun creatures are immune to the powers of version control differencing algorithms. Due to the overwhelming possible meanings of the binary data (i.e. you can't assume it's text) the best version control can do when a binary blob changes is... store the entire binary blob, anew. That doesn't sound terrible? I forgot to mention that version control systems, at least by default, **never** delete anything.

So now maybe you can see why, if you check in a 10 MB video today, edit it, check it back in tomorrow, and do so 50 more times, how you would now have a Git repo that is 10 * 52 = 520 MB in size, and the only thing it contains is a single video file... along with all 52 versions that ever existed.

https://www.git-tower.com/learn/git/faq/handling-large-files-with-lfs

## Option 1: Git LFS

Git Large File Store (LFS) is an extension to the Git Specification intended on addressing exactly this issue of how to store larger, binary files away from the source code, but not adversely affect the Git Workflow more than necessary.

It seems like one big problem with Git LFS is a lack of maturity and available implementations for a server-side.

## Option 2: Pseudo-LFS Referencing

Because it seems that Git LFS is a bit of a niche technology/capability, it might be better to try and "mock" what it is doing by simply storing non-Markdown files somewhere else, using a programmatic API that is mature and common place. (e.g. DropBox, Google Drive, a NAS or FTP site.)

This could be possible simply by having VS Code intercept the inclusion of a file, copy the actual file to the destination "LFS" using whatever supported LFS Driver the user has selected, and then putting a standard URL in the Markdown file. (In a very naive sense, this is what Git LFS is actually doing for us.)

Now, it will be up to the user to ensure they have the right accessibility to the LFS location itself... but then again, it's also up to the user to ensure they have access to the Git location where the Markdown files are kept. So there are just two locations that need to be accessible for ARF Wiki 2 to be fully functional in this case.

### Benefits

- Opportunity to support many popular and widely used services and technologies like: DropBox, Google Drive, etc.
- Possibility of making a plugin/extensible mechanism so that other folks can contribute new "asset storage drivers".

### Consequences

- Would mean more upfront work for ARF Wiki 2 development.
- ...

## Option 3: Use a Separate Git Repository

You could use Git, but use a separate repository explicitly for assets and keep your Markdown wiki pages in the other repository. Once again, this is sort of what Git LFS is already doing, in a naive sense. This could also be considered a more specific implementation of Option 2... without the generalness of supporting random "asset storage drivers".

### Benefits

- Same technology for all your files; reduces the added complexity of adding yet another technology to the software stack.
- Would be faster than Option 2 to implement.

### Consequences

- Wouldn't be a good choice if you save lots of versions of the binary files, with the default settings.
    - You could address this by setting the assets repo to only save a certain number of versions. For example, you can configure the assets repo to save only the most recent 3 versions. This might be a good compromise between a large repo size, and actually having your binary files versioned. (You can go back to earlier versions, up to the number you indicate!)
- Wouldn't be good for initial download (repository cloning) if left to the default behavior, because this would download the entire 'assets repository' to your computer.
    - This could be mitigated, partially by controlling the overall repo size by doing something like the above.
    - Or, you can do a shallow clone of depth 1, which means the 'assets repo' will only download the latest version of each file.
    - Or, you can let ARF Wiki 2's Referential Linking mechanism, which means you don't actually download the 'assets repo' to your local computer at all. The tradeoff here is that you must have network connectivity to wherever the 'asset repo' is stored.

## Option 4: Boar

Check out [Boar](https://bitbucket.org/mats_ekberg/boar), a version control system specifically for large binary files.

... TBD ...
