# Page Metadata

[TOC]

## Overview

ARF Wiki stores a number of additional bits of information about your pages.

* Page Creation Date: <date>
* Page Last Modified Date: <date>
* Page Created By: <user>
* Page Created On: <hostname>
* Page Last Viewed Date: <date>
* Page Last Viewed By: <user>
* Page Revisions: <number>
* ...

## Show Page Metadata

You can show/hide page metadata on each page by changing the options in [page-metadata-options.md]()