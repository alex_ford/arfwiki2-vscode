# Widgets

[TOC]

## Overview

Widgets are dynamic portions of a page which are replaced by the background wiki processing engine in order to reflect changes or updates periodically.

See [View Pages]() which are generally predefined or purpose-specific pages containing one or more widgets.

See [Live Content](./live-content.md) which is the abstract idea that encompasses Widgets.

## Mechanics

The background daemon service will scan the entire wiki for widget markup and when found will replace/update it with the appropriate content.

## Examples of Some Widgets

### Countdown Timer

Shows a countdown timer with a specified resolution being one of: "day", "week", "month", "year".

Week, month, and year resolutions can also show a single sigfig of additional precision.

#### Example

	5 days remaining
	
	2 weeks remaining
	
	6 months remaining
	
	1.3 years remaining

### Recently Updated Pages

Shows a list of the top N most recently updated wiki pages, along with links to the page.

### Table of Child Pages

Similar to Typora's Table of Contents markup, ARF Wiki's `TOCP` is used to dynamically list the children pages beneath a given section.

### Navigation Trail

This widget is updated to show the navigation trail, or heirarchy, of the page in the wiki. It looks like this:

[ [ARF Wiki]() ] > [ [Hello World]() ] > [ [My Page]() ]

Each block in the trail will be linked to the corresponding page. Home Pages are a special case, where the section name is displayed in the trail, and it is linked to the `home.md` file within that section.

### Related Pages

This widget is updated to show pages that are related to the current page.

### See Also

This widget is updated to show pages that ... ==how is this different from Related Pages==?

### Page, Section, Wiki Score Cards

These widgets show the metrics collected and computed about your wiki at the specified scope level.

### Previous and Next Page auto links

Certain sections contain pages which can be sorted into a "natural order", like time. This widget takes some configuration parameters and then automatically creates a "Previous" and a "Next" link, automatically!

### Header Content

...

### Footer Content

...

### Toolbar Widget

Not sure if this is needed, but we shall see!

See [Toolbar]() for more information.