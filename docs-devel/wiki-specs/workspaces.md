# Workspaces

This feature is a way to "open" and "close" parts of the wiki. This is useful for a number of reasons:

- When you want to focus on a smaller set of wiki sections/pages.
- When you want to hide certain sections due to the environment you are in.
- ...

The page [Section Inclusion](./section-inclusion.md) might be a duplicate of this.

## How user opens a workspace

==???==

## How user defines a workspace

==???==