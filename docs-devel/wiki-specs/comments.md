# Comments

[TOC]

## Overview

Comments are annotated text blocks that are inserted onto a page with a particular look-and-feel, along with some common metadata.

> [@alex]() wrote on 2018-08-22 19:21 EST
>
> > There is this thing that I want to try, let's see how it goes, shall we?
>
> [Reply]() [Delete]()

The first line is predetermined to be made up of:

- The user/person, with link to the corresponding page under the [People Section](../people/home.md).
- The static string literal "wrote on".
- The timestamp of when the comment was made.

The subsequent lines are the comment text, supplied by the user.

## Collaboration

Comments can be an effective way to use a wiki to collaborate with other people. While the wiki doesn't support concepts like logins, you can capture comments from other people simply by tagging them.

See [Collaboration](./collaboration.md) for related details.

## Avatar Photo

If the named user (e.g. @alex) has a photo in the People section, it will be shown as part of the comment!

==TBD==

## Workflow Actions

Comments can start workflows if desired. Workflows enable structured ...

### Reply

This is always available, making the new comment a reply to an existing one.

==Show how this might look==

### Request Review

If a comment author "requests review", then the comment will track it's review status and enable users to perform additional actions to interact with the comment.

#### Approved

...

#### Rejected

...