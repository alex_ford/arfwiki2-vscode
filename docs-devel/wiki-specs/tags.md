# Tags

[TOC]

## Overview

A Tag is the general term for any user-supplied text that is treated as special, usually automatically applying a link, inserting additional information, and being searchable. There are a number of different tags, which are defined on this page.

## Rules

- Tag text cannot contain spaces, nor start with a number, or use any symbol except for hyphens (`-`).
- Tag text may contain alphabetical characters, both upper-case and lower-case, along with numbers (after the first character).
- Hyphens (`-`) are ignored during matching, so `foobar` is equivalent to `foo-bar`.
- Capitalization sensitivity is configurable with the `tags.caseInsensitive` option. Setting this to `true` (the default) makes the case irrelevant (so `#foobar` is the same as `#FooBar`). Setting this to `false` makes the case relevant, so `#hello` is NOT the same as `#HeLLo`).
- Generally, a tag has some additional features which are accessed using the colon (`:`) symbol. For example `@alexford:age` will insert Alex Ford's `age` where the markup appears.

## Aliases

Many tags support Aliases, which are simply alternate ways to reference the tag. This is useful when you might have a tag that you type often, and want to create shorter, or easier to remember, aliases. Or perhaps it just has multiple ways to refer to it, and you'd like to be able to use any of them to mean the same thing.

One use case could be for dealing with a person. Let's say I have a page `./people/friends/alex-ford.md` and on it, I've defined the following aliases `@af`, `@arf`, `@alex`, `@aford`, `@forda`, `@fordar`. I can now use any of these tags and the wiki will do the same thing for all of them.

### Conflicting Aliases

Conflicting aliases occur when you've defined the same alias twice for different tags. The result will be that the first match will be used to process the tag, and a warning will be displayed to indicate the problem.

==OR== a selection box will be presented at the time of tagging which will allow you to select the correct match to use.

## Explicit Tags

Explicit Tags are declared by the user entering a prefix before some text. This is in contrast with [Implicit Tags](#Implicit-Tags) which do not require a prefix.

### General Tags (`#`)

General Tags are declared using the `#` symbol, followed by the text of your choice. The wiki engine applies the following:

- The tag is highlighted for easier user recognition.
- The tag becomes a link to a [View Page](./view-pages.md) which lists the pages and lines where the general tag is used.
- ...

#### Include Attributes

- `#generalTag:count`
- `#generalTag:...`

### People/Mention Tags (`@`)

- The tag is highlighted for easier user recognition.
- The tag becomes a link to a corresponding page in the `people` section of the wiki.
- The tag receives hover-text which displays the contact information defined on the `people` page for the person.
- Additional insertion suggestions can be accessed by typing `@name:email` which would include a dynamic link to the person's email as defined on their page in the `people` section.
- People/Mention tags work according to an exact match to a page in the `people` section, or a match to one of the aliases defined within the pages of the `people` section.
  - So, for example, if I have a page under `./people/friends/alex-ford.md`, I could reference `@alexford` or `@alex-ford`

#### Include Attributes

- `@mention:name`
- `@mention:first-name`
- `@mention:last-name`
- `@mention:middle-name`
- `@mention:aliases`
- `@mention:age`
- `@mention:date-of-birth` or `@mention:dob`
- `@mention:phone-number`
- `@mention:email`
- `@mention:home-address`
- `@mention:<custom-attribute-name>`

### Location Tags (`&`)

- The tag is highlighted for easier user recognition.
- The tag becomes a link to a corresponding page in the `location` section of the wiki, if one exists.
- A "Google Maps" icon will be included after the tag which links externally to `https://maps.google.com` with the address pre-loaded.Email Addresses
- Hovering over the location tag will show information from the local wiki page, if one exists, as well as information from Google Maps, if any exists. It will also show a cached version of a small Google Maps rendering. (If no cached version exists, then Internet will be required in order to download the image.)
- Matched if `&name` matches any page, or the aliases defined on one of the pages, in the `location` section of the wiki.
- Matched if `&address-regex` matches a recognized regular expression for an address.
- Matched if `&lat,lon` matches a recognized regular expression for a latitude/longitude pair.

#### Include Attributes

- `&location:...`

### Phone Number Tags (`^`)

- The tag is highlighted for easier user recognition.
- The tag becomes a link to a corresponding page in `people` or `locations` that contains the phone number.
  - If multiple pages are found, a drop down list will be presented instead, with a listing of the pages for the user to select from.
- A :phone: icon will be included after the tag which will request your operating system to place a call using the number.
  - Support for this is operating system/software dependent.

#### Include Attributes

- `^phone-number:...`

### Question Tags (?)

`?can I do this?`

This tag works a little differently than the others. A question tag doesn't need to be a single word. Instead, you simply type `?` then the first word, *without* a space between the question mark and the first word. Keep typing your question, until the end, then finish it with the ending `?` question mark.

Result:

:grey_question: can I do this?

> Some answer goes here!

### Task Tags ([])

` - [ ] tag`

A Task Tag can be denoted using proper Markdown (e.g. `- [ ] text goes here`) or the ARFWiki shorthand `[] text goes here`.

Result:

- [ ] text goes here

### Idea Tags (!)

`!some awesome idea`

Result:

:bulb: some awesome idea

## Implicit Tags

Implicit Tags do not require a prefix to be entered first. The wiki engine applies various regexs looking for positive matches. An Implicit Tag will be processed as soon as the user presses <kbd>Space</kbd> or <kbd>Enter</kbd>. If the tagging is not desired, a single <kbd>Backspace</kbd> or <kbd>Ctrl</kbd>+<kbd>Z</kbd> (Undo) will remove the tag.

### Email Address Tags

- Matches any text that has a first part, separated by an `@` symbol, followed by a second part, separated by a `.`, followed by a third part.
  - Regex: `.*[@].*[.].*` ==TODO== this is far too general! Need to be a bit more specific to avoid false matches.

### Location Tags

- Matches any text that looks close enough to a recognized address, for example: 
  - Examples: `156 Foobar Lane, City, State, Country`
  - Regex: `regex`
- See [Location Tags](#explicit-tags#location-tags) under the explicit tags section for more details.

- [ ] ==TODO== how do you specify an anchor link to a section that appears multiple times on a page? (Like the link directly above!)

### Phone Number Tags

- Matches reasonable text that looks like a phone number.
  - e.g. `617-123-4567`, `617 123 4567`, `(617) 123-4567`, `617.123.4567`
- See [Phone Number Tags](#phone-number-tags) for more details.