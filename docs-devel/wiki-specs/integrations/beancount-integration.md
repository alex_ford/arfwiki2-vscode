# Beancount Integration

[TOC]

## Overview

Beancount is a command line toolkit and language for declaring financial data in plain text files. It's written largely in Python and has a similar "vibe" to it that ARF Wiki does.

Integration between ARF Wiki and Beancount could involve:

- Generated view pages in ARF Wiki from Beancount data.
- Live Content references to data in Beancount.
- ...