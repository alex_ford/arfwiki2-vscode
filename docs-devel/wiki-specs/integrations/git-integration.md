# Git Integration

## Overview

Git is the primary backend for storage of ARF Wiki pages.

## History and Restoration

Git provides the mechanism for Section and Page history, along with Restoration!