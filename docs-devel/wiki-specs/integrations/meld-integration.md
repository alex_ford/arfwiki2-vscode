# Meld Integration

## Overview

Meld is used as a cross-platform diff viewer. When desired, or needed, meld is called upon to show different versions of a page side-by-side so the user can make a reasonable choice.