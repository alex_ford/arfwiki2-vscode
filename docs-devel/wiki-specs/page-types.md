# Page Types

[TOC]

## Overview

Page types are a loosely defined purpose for a page and it helps the wiki maintain organization. There is a fine line between [Template Pages]() and page type.



See also [Wiki Conventions](./wiki-conventions.md)

See also [Template Pages](./template-pages.md)



- [View Pages](./view-pages.md)
  - Generated pages that contain content from dynamic sources.
  - See also [View Pages](./view-pages.md)
- Option Pages
  - This is the primary way which ARF Wiki exposes configuration options to end-users. Here, you'll find various pages that contain a [Properties Box]() full of interesting things to tweak and try!
- Reference Information Pages
  - These are pages that contain reference information, much like the entirety of [Wikipedia.org](https://www.wikipedia.org).
- Life Log Pages
- Work Log Pages
- Meeting Pages
  - For when you have a meeting with other people and you need to capture the notes.
  - ...
- Tracking Pages
  - For when you want to track something. This will require periodic data entry from the user, most likely indexed by time, and perhaps other secondary metrics.
  - Tracking Pages will benefit from automatic analysis and reporting.
  - e.g. you want to track your car services.
- [ ] ==TODO== this page needs more details!