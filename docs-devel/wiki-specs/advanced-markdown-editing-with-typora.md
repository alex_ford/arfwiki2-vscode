# Advanced Markdown Editing with Typora

[TOC]

## Overview

See https://support.typora.io/HTML/

See https://support.typora.io/Add-Custom-CSS/

## Diagrams

* https://support.typora.io/Draw-Diagrams-With-Markdown/

## Tables

### Move Row/Column

In Typora, you can simple hover over the left/top border of a row/column, click and drag to it's new home.

| b    | a    | c    |
| ---- | ---- | ---- |
|      |      |      |
| 2    | 1    |      |
|      |      |      |

## Math and LaTeX

Subsets of the MathML and LaTeX markup languages for mathematics and scientific notation are supported by Typora.

See https://support.typora.io/Math/ for more information.

### Inline Math Statement

Math statements can be added inline to a page using single dollar signs wrapping the statement: `$some-math-statement-here$`

Addition Sample: $1+1$ 

Addition Sample with Computed Result: ${1+1=}{T}$ 

### Math Statement Block

Math blocks can be added by typing two dollar signs (`$$`) sequentially, then pressing Enter.
$$
\begin{align*}
y = y
\end{align*}
$$

## Coloring Text

This can be accomplished by using HTML tags, which Typora has partial support for.

<span style='color:red'>Red</span> <span style='color: blue'>Blue</span> <span style="color: green">Green</span>

<span style="background:grey">Grey Background</span>

## Useful Text Styles

Render a keyboard box: <kbd>WIN</kbd>+<kbd>F</kbd>

`<kbd></kbd>`

<details>
    <summary>
        Click here to see more...
    </summary>
    And then this was revealed! You can edit this by holding Ctrl when you click on the element.
</details>

## Buttons and Form Controls

<button>Click Me!</button>

<form><label>Password: </label><input type='password' placeholder="Enter Password Here" required/></form>

<label><input type="checkbox" checked="checked"> Check me!</label>



