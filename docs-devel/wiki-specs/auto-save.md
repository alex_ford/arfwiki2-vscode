# Auto Saving

Wiki pages are auto saved using a feature of Typora.

It is set to happen at least once every 12 seconds (0.2 minutes).

If starting with a fresh install of Typora, you must do the following:

1. Open Preferences and enable Auto Saving.
2. Open Advanced Preferences and change the value of "autoSaveTimer" to 0.2

