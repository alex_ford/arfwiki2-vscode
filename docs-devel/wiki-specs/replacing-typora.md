# Replacing Typora

[TOC]

## Overview

While Typora is an excellent Markdown editor, it isn't quite as streamlined as it could be for the concepts and features enabled by the ARF Wiki project.

## Bugs and Known Issues

- Typora can occasionally, but rarely, spaz out and entire chunks of data can be erased.
- Working with lists isn't always perfect, and actually, about 1 out of every 10 times, I manage to get Typora into a weird state where it just can't figure out what to do.
- Auto Save feature is a nice idea, but when Typora performs the save, it moves the cursor up to the top of the page. Very annoying!
- Opening links always opens them in a new Typora Window. Haven't figure out if there's a way to change this.
- [home](./home.md)

Also see: [Known Issues](./known-issues.md)

## Lost Typora Features

Typora gives us quite a number of useful features which we will lose if we move away from it. List them out here:

1. ...

## Option 1: replace with another Markdown editor

...

## Option 2: replace with VS Code

...

## Option 3: roll a custom Markdown editor specifically for ARF Wiki

...