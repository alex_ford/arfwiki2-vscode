# Page Tags

[TOC]

## Overview

Page tags are simple single term text strings that start with `!#`. We could not simply use `#` because that is in-use by Markdown syntax.

```markdown
!#tag1 !#tag2
Here is some text. !#tag3
```

