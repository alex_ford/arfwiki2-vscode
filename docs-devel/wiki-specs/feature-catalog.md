# Other Feature Ideas

[TOC]

## Overview

This page is for other feature ideas that I haven't made dedicated pages for yet.

- [ ] ==TODO== turn each of these bullets into a H3 section

- Reminders on any page, anywhere

  - Set reminders on entire pages, or portions of pages, including Tasks, and ARF Wiki will alert you when the reminder triggers.

- Stream of Consciousness (SOC) Editor (DUPLICATE, SEE BELOW)
  - Could this be implemented using some Markdown/Python trickery? Essentially, provide a page designated as thee SOC Editor and then have a background script organize and perform tasks on it in near real-time.
  - Ultimately, the point of the SOC Editor is for the user to be able to just "type and go" into a chronological log. They don't worry about anything other than inputting content in the moment.

- General Concept: Use Hidden HTML for replacements

  - Any script that performs a replacement of some markup on the page should place the user-supplied markup into a hidden HTML element. This allows the script to find it again if updates are needed, and allows the user to edit the original markup that they wrote.

- Progressive/Response App for List and Tasks
  - This app would focus entirely on the list/tasks aspect of ARF Wiki.
  - ...

- Active Tasking View Page

  - A generated view page for all active tasks (uncompleted, and eligible to start) assigned to anyone, or a specific user.

- Task Planner

  - ...

- Task Comments, Page Comments
  - Ability to add timestamped comments to tasks, elements, pages, etc.

  - Comments include the author, the hostname of the computer, and the timestamp, along with the comment text itself.

  - Comments can basically use any Markdown syntax available.

  - They are displayed using the Quote element, like so:

  - > @alex | virgon | 2018-08-21 20:53 EST
    >
    > Hello world, this is a comment block.

- URL shortener
  - Certain URLs can be shortened automatically into a more friendly display link. For example: http://company.jira.com/home.asp?project=MTHN&view=home can be auto resolved to it's web page name of "MTHN JIRA Home"
  - Patterns can be defined which help guide ARF Wiki in shortening other types of links as well.

- Google Maps Location Auto Linking
  - When an address is detected, ARF Wiki can auto link the address text to Google Maps.
  - A small overview map could also be inserted as a screen shot.
  - Example page: [Westhaver NH House](../unfiled/westhaver-nh-house.md)

- Gantt Chart View Pages

  - Create View Pages using Mermaid's Gantt Charting capabilities from filtered sets of Tasks.

- Quick screen capture - selection, window, and full

  - This is just a nice-to-have to speed up the user's workflow.
  - Windows workaround: use the built-in "Snipping Tool" which places the screen capture on the clipboard. It can then be pasted directly into Typora.
  - Linux workaround: ==???==

- Navigate backwards through page view history

- Navigate forwards through page view history

- Favorite/pin certain pages so they standout and appear at the top

- Context menu mod to include "Link Assist" submenu

  - Link Assist can create links for you, like automatically linking the selected text to the Wikipedia page, or providing a link to search Google, Amazon, or Google Maps.
  - "Feeling Lucky" option to hyperlink selected text to the first result of a Google search.
  - "Peek" option to view the results of a Google search in order to select one of the results to be the link.

- Inline math/arithmetic calculations.

  - Simply type out basic arithmetic expressions and they will be evaluated automatically!
  - Impl Details, Option 1
    - The background daemon can look for modified files and when detected can look for math expressions. Any that are found are evaluated and the result is put in place.
  - Workarounds:
    - Windows: using AutoHotKey, you can have scripts which accomplish this.
    - Linux: ==???==

- Visual merge/conflict resolver

  - For when the `git-sync-daemon` is unable to perform an automatic merge.
  - Workarounds:
    - All Platforms: use your favorite Git GUI, like Atlassian SourceTree

- Renaming a page or section updates all links throughout the wiki

  - ...

- View/compare differences in pages over time

  - Leverages Git's version control ability to pull up older versions of page content.
  - ...

- Stream of Consciousness Log Entry (DUPLICATE, SEE ABOVE)

  - This is a notetaking style that emphasizes data entry over all other things. You should not be concerned about organization, styling, etc. The only thing you need to do is insert what you're thinking!
  - ARF Wiki automatically inserts SOC Log Entries into the `logs` section of the wiki, filed by date.
  - Small Python/QT-based Desktop GUI for inputting short twitter-like entries???
  - Mobile entry???

- Copy Link To Here feature

  - Allows the user to create links to any part of their wiki. The option can be accessed through the right-click menu, or via hotkey (TBD).
  - Under the hood, the wiki simply inserts an anchor tag at the spot the user is at, or fetches the name of the anchor tag if one is already there. This is then placed on the clipboard so the user may paste it elsewhere.

- Wiki Analysis and Statistics view page

  - A generated view page that shows various analytics and statistics about the user's wiki. This includes the size of the data repo, the asset repo, number of orphaned pages, etc. A "wiki quality score" is also given, which is an aggregate measure of the overall quality of your wiki. If you are following good wiki practices, your score will be closer to 100%!

- [Wiki Quality Score](./wiki-quality-score.md)

  - ... ==TBD==

- Instant Redaction of text as it is typed/hovered over

  - This feature covers up the text as you type it, and only reveals small parts of it as you hover your mouse over the text. This is potentially useful if you're in a public space, or otherwise have prying eyes looking over your shoulder.

## Extended Emoticons/Emojis

Beyond the ones [included in Typora](./emoticons.md), ARF Wiki will install several more emoji sets. You access them the same way as the other ones.

### Nerd Set

- [ ] ==TODO== 
- Windows Logo
- Mac Logo
- Android Logo
- iPhone Logo
- Linux Logo
- Ubuntu Linux Logo
- Debian Linux Logo
- Fedora Linux Logo
- RedHat Linux Logo
- ...

### Badge Set

- [ ] ==TODO== 
- Status badges, like from GitHub, etc.
  - Success
  - Failed
  - Warning
  - Disabled
- ...

### Websites and Logos

- [ ] ==TODO== 
- GitHub Logo
- GitLab Logo
- BitBucket Logo
- JIRA Logo
- Confluence Logo
- WordPress Logo

## Auto Emoji Insertion and Replacement

### Insertion

Insertions are when the wiki engine adds the emoji as a prefix or postfix to the word or phrase that triggered it. For example, typing Windows could result in Windows []. Insertion options and mappings can be controlled on the [Emoji Options](../options/emoji-options.md) page.

Here are some default insertions:

| Trigger Text                              | Emoji   |
| ----------------------------------------- | ------- |
| `email`, `e-mail`, `someone@domain.com`   | :email: |
| Windows, Mac, Android, iOS, Linux, Ubuntu |         |
|                                           |         |

### Replacement

Replacements are when the wiki engine removes what was typed and puts the emoji in it's place. This is more suitable for when you type a shorthand or alternative text sequence that maps to an emoji. For example, typing `:)` could be replaced with :smile:. Replacements can be controlled on the [Emoji Options](../options/emoji-options.md) page.

Here are some default replacements:

| Replaced Text                                                | Emoji                                                        |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| `:)`, `:(`, `:O`, `:'(`, `8)` (also all these with a hyphen for a "nose" is included as well, e.g. `:-)`) | :smile:, :frowning_face:, :open_mouth:, :cry:, :sunglasses:  |
| `(!)`, `(?)`, `(/)`, `(x)`, `($)`, `(!?)`                    | :exclamation:, :grey_question:, :heavy_check_mark:, :x:, :heavy_dollar_sign:, :interrobang: |
| (ok), (nope), (yes), (no)                                    | :ok:, :no_entry_sign:, :+1:, :thumbsdown:,:hand:,            |
| (music), (drinks), (coffee)                                  | :notes:🍸, ☕️                                                  |

Navigation: :arrow_left: Previous      Next :arrow_right:    :leftwards_arrow_with_hook: :arrow_up_small: :arrow_down: Go to Bottom    :arrow_up: Go to Top    :arrow_right_hook:  :m

Computing: :cd: :dvd: :computer: :computer_mouse: :desktop_computer: :battery: 

Holidays and Festivals: :jack_o_lantern:

Currency: :moneybag: :dollar: :yen: :euro: :currency_exchange:

​Food and Drink: :icecream::tomato: :eggplant: :corn: :grapes: :watermelon: :banana: :orange: :pineapple: :cherries: :peanuts: :pizza: ==add drinks==

Travel: :airplane: :car: :bus: :busstop: :train: :ship: :rocket: :bike: :walking: :m:

Time and Sequence: :alarm_clock: :timer_clock: :stopwatch: :stop_sign: :stop_button: :play_or_pause_button: :fast_forward: :rewind: :pause_button: :repeat: :repeat_one: :twisted_rightwards_arrows: :record_button:  ⏬ ⏫ 🔽 🔼

:movie_camera: :cinema: :tv: :film_projector: :film_strip: :video_camera: :video_game: :vhs: 

Trees and Plants: :evergreen_tree: :christmas_tree: :deciduous_tree: :palm_tree: :seedling: :cactus: :tulip: :wilted_flower: :rose:

:shower:

:ledger:

:zap:

:new:

:artificial_satellite: 

:earth_americas: :world_map: :globe_with_meridians: :earth_africa: :earth_asia: :us: :thailand: :de: :fr: :cn:

​Awards and Accomplishments: :1st_place_medal: :2nd_place_medal: :3rd_place_medal: :trophy: :medal_military: :medal_sports: 

Numbers: :zero: :one: ... :nine:

:radio_button:

:lock: :closed_lock_with_key: :key: 

Moon Phases: :new_moon: :waning_crescent_moon: :last_quarter_moon: 🌗 🌖 :full_moon: :moon: :waxing_gibbous_moon: :first_quarter_moon: ​ :waxing_crescent_moon: :low_brightness: :high_brightness:

## Live Suggestions and Recommendations

As you type new content, the wiki can provide live suggestions and recommendations based on what you're writing. This could be useful to help you find content you previously created, but may not remember clearly.

- [ ] ==TODO== sketch out how Live Suggestions and Recommendations would look on the user interface of Typora
- [ ] ==TODO== think about how to actually implement this.

## Page Diffs with Meld

...

## Quick Copy Code Lines

Support for copying lines of code displayed in a code fence with a minimal number of clicks! Makes it real easy to use reference snippets!

## Additional Paste As Formatters

When pasting various kinds of text, the wiki can help format that text into more appropriate Markdown text. For example, CSV text can be turned into a Markdown table.