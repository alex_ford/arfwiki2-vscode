# Options

**Navigation**

**Related**

[ [Options Home](../options/home.md) ]

[TOC]

## Overview

The `options` is a special section of the wiki that contains `option pages` which are used by the python daemons and utility scripts to control their operation.

## Design Rationale

There needed to be a way to control the behavior of the python daemons and utilities. I also wanted it to be accessible to the end-user, from Typora. It occurs to me that there is nothing stopping a Markdown page from essentially being a glorified properties file!

[markdown-options-validation.py] -> [markdown-options-parser.py] -> [Dict]

Error messages will be inserted back into the options page.

Example:

==markdown-options-validation: There was a problem validating the options on this page.==

==markdown-options-parser: There was a problem parsing the options on this page.==

==markdown-options-parser: Illegal value for option 'foobar' on this options page.==

Option pages with errors will be temporarily renamed to `_optionpage_.md` to help the user identify which page in the tree has the problem.

## Option Pages

An Option Page is simply a Markdown file that contains a code fence set to `properties` language. All other content on the Option Page is ignored. In the code fence, comments (denoted with `#`) are ignored.

The following Option Pages are defined:

| Page                                         | Description                                                  |
| -------------------------------------------- | ------------------------------------------------------------ |
| [Asset Options](../options/asset-options.md) | Options for how assets (images, audio, video, etc.) are treated by ARF Wiki. |
| Encryption Options                           |                                                              |
| Git Options                                  |                                                              |
| Linker Options                               |                                                              |
| Log Options                                  |                                                              |
| Logging Options                              |                                                              |
| Page Metadata Options                        |                                                              |
| Section Options                              |                                                              |
| Task Options                                 |                                                              |
| Wiki Options                                 |                                                              |

## Options Reference

The actual options are detailed on their respective Option Page, along with some comments on legal values and what they do.

The general form is that options will appear in the first code fence of an option page (additional code fences are ignored). Options themselves will take the form:

```properties
<namespace>.<opt1>.<opt2>.<optN> = <value>
```

Where `<namespace>` is roughly the options page name.

Where `<opt1>`, `<opt2>`, `<optN>` are names that define the specific option.

Where `<value>` is the value to give to the option.