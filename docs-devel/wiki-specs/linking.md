# Linking

[TOC]

## Overview

One of the strongest features of a wiki is linking. Both linking externally to other websites on the Internet, but also internally to other wiki pages and headings.

Typora supports quick linking using < and > brackets.

Typora will also automatically link most commonly recognized URLs.

To create an anchor link:

```markdown
[Link Text](#heading-name)
```

Follow the link by holding CTRL when clicking.

The standard markdown link syntax:

```markdown
[Link Text](http://blog.alexrichardford.com)
```

Spaces in a heading/anchor should be replaced with a hyphen: `-`

Links shall be all lowercase and punctuation removed.

Examples:

	let's go			lets-go.md
	
	Hello, World!		hello-world.md

## Reference Links

Reference links use pointers at the text to be linked, which refer to the link data stored elsewhere (typically at the bottom) of the page.

## Autolinks

Typora will attempt to automatically create links out of text surrounded by `< >` symbols.

<https://www.google.com>

Protocol is required for autolinks.

## Local Links

Local links refer to files/resources on the local filesystem. These can be relative or absolute.

## Link Suggestions/Intellisense

- [ ] ==TODO==

## Open Directory in File Explorer

With Typora, it is possible to create a link which opens a File Explorer window to a particular directory on the local computer.

[Typora Resource Docs directory](file:///C:\Program Files\Typora\resources\app\Docs) - clicking this should open a File Explorer at the target directory.

Works on Windows :heavy_check_mark:

Works on Linux :grey_question:

Works on Mac :grey_question:

```markdown
[Typora Resource Docs](file:///C:\Program%20Files\Typora\resources\app\Docs)
[Typora Resource Docs](file:///C:\Program Files\Typora\resources\app\Docs)
```

Click [here](file:///C:\Users\arf41\Workspace\arfwiki-data) to open this wiki's data directory.

==How do use relative links to open directories?==

## Absolute Links to Other Markdown Files

 [Markdown Reference](file:///C:\Program Files\Typora\resources\app\Docs\Markdown Reference.md) - clicking this should open in Typora.

```markdown
[Markdown Reference](file:///C:\Program Files\Typora\resources\app\Docs\Markdown Reference.md)
```

## Platform Difference Handling

Differences include path syntax, legal/illegal characters, case sensitivity, encoding, application names, directory/filesystem structure, etc.

The wiki engine will automatically convert things...

## Links to Shell Scripts

Works :heavy_check_mark:        Doesn't Work :x:        Unsure :grey_question:

### Windows PowerShell

**Relative Link:** [Manual Git Sync PowerShell Script](..\..\arfwiki-src\manual-git-sync.ps1) Works ✔️ ==PREFERRED==

**Relative File Link:** [Manual Git Sync PowerShell Script](file:///..\..\arfwiki-src\manual-git-sync.ps1) Doesn't Work ❌

**Absolute Link:** [Manual Git Sync PowerShell Script](C:\Users\arf41\Workspace\arfwiki-src\manual-git-sync.ps1) Works :heavy_check_mark:

**Absolute File Link:** [Manual Git Sync PowerShell Script](file:///C:\Users\arf41\Workspace\arfwiki-src\manual-git-sync.ps1) Works ✔️

### Linux Shell

**Relative Link:** [Manual Git Sync Shell Script](../../arfwiki-src/manual-git-sync.sh)

- Doesn't work :x: this opens the script in a text editor.

**Relative File Link:** [Manual Git Sync Shell Script](file:///../../arfwiki-src/manual-git-sync.sh)

- Nothing :x::x: not even a Typora error notification.

**Absolute Link:** [Manual Git Sync Shell Script](/home/alex/Workspaces-Personal/arfwiki-src/manual-git-sync.sh)

- Doesn't work :x: just opens system text editor to edit the script.

**Absolute File Link:** [Manual Git Sync Shell Script](file:///home/alex/Workspaces-Personal/arfwiki-src/manual-git-sync.sh)

- Doesn't work :x: just opens system text editor to edit the script.

[test](apt://Typora)

## Links to Python Scripts

If the .py extension is not registered... well... let's go ahead and register it!!

[Create New Page](file:///C:\Users\arf41\Workspace\arfwiki-src\create-new-page.py)

[cmdfile test](cmdfile://../../arfwiki-src/hello-world.bat) :x:

## Links with File Handler Protocols

File Handler Protocols are used by operating systems to help create URIs which result in the correct action being taken when executed. The `file://` protocol is a prime example of this, used in the tests above.

The URLs may need to be encoded to properly execute. Something like https://www.urlencoder.org/ can help with this.

`file:///`

`code:///` [code://test](code://linking.md)

`search:`

Click [here](search://) to open the Search View of the File Explorer.

Click [here](search:query=hello&displayname=arfwiki) to open the Search View of the File Explorer, having the query "hello" executed.

Click [here](search:query=hello&crumb=location:C%3A%5CUsers%5Carf41) to open the Search View of the File Explorer, having the query "hello" executed in the C:\Users\arf41\ directory.

Click [here](search:query=hello&crumb=location:C%3%5CUsers%5Carf41%5CWorkspace%5Carfwiki\-data) to open the Search View of File Explorer, having the query "hello" executed in the C:\Users\arf41\Workspace\arfwiki-data\ directory.

https://docs.microsoft.com/en-us/windows/desktop/shell/search-protocol

[edit](print://)

[passing arguments](..\..\arfwiki-src\hello-world-args.bat)

[passing arguments 2](..\..\arfwiki-src\hello-world-args.bat "world")

[shell startup folder](shell:startup)

So, it seems like whatever I can enter in the Windows File Explorer is also what I can use here in Typora.

[Open Action Center](ms-actioncenter://)

[Open Alarms & Clock App](ms-clock://)

[Open Calculator](calculator://) Win10 :heavy_check_mark:

[Open Cmd](/windows/system32/cmd.exe)

[Open Cmd, Launch Batch Script with Args](/windows/system32/cmd.exe "/k" "echo hi")

[Open Mail](mail)

Open system email client, and compose email: [mailto](mailto:arf4188@gmail.com?subject=Hello&body=World)

[Open Windows 10 Photos App](ms-photos://)

[Open Windows 10 Photos App](ms-photos:viewer) (this syntax force closes for some reason)

[Open Windows 10 Photos App at Image File](ms-photos:viewer?fileName="c:\users\arf41\workspace\arfwiki-data-lfs\1534939786852.png") (not working)

[Open Windows 10 Video Editor App](ms-photos://videoedit?Action=View) (not working)

[test](ms-resource://hi)

[nfs](script://hi)

[javascript](javascript:alert) :x:

[Open System Information/About Window](ms-settings:about) Win10 :heavy_check_mark:

[test](ms-settings:emailandaccounts)

[Open Windows 10 AppData directory](shell:AppData) Win10 :heavy_check_mark:

[Open Application directory](shell:AppsFolder) Win10 :heavy_check_mark:

[Open Desktop directory](shell:desktop) Win10 ✔️

[Open Documents Library](shell:documentsLibrary) Win10 ✔️

[Open Downloads directory](shell:downloads) Win10 ✔️

[Open Libraries](shell:Libraries) Win10 ✔️

[Open This PC](shell:MyComputerFolder) Win10 ✔️

[Open Music Library](shell:MusicLibrary) Win10 ✔️

[Open Pictures Library](shell:PicturesLibrary) Win10 ✔️

[Open Profile/Home directory](shell:Profile) Win10 ✔️

[Open Videos Library](shell:VideosLibrary) Win10 ✔️

[wmi](wmi://)

## Opening the Character Map

### Windows 10

...

### Linux (Ubuntu 18.04)

...

## Registering Windows Protocol Handlers

On Windows, we can modify the registry to add additional "protocol handles" that can enable us to do lots of clever things!

See the `edit-windows-registry.py` script for Python methods to help with this.

In essence, the Windows Registry can have new protocol handlers inserted into it.

![1535028064359](C:\Users\arf41\AppData\Local\Temp\1535028064359.png)

In the above screen clipping, you can see the `powershell` and `code` entries, which are ones that I've added. They enable the `powershell://` and `code://` protocols.

### Registry Entry Spec

Key  names: `name\`

Key Default Value: `(Default) = value`

Variables: `varname = value`

Replace the items surrounded by `{ }`

- `{name}\`
  - `(Default) = "URL:{name} protocol"`
  - `"URL Protocol" = `
  - `DefaultIcon\`
  - `shell\`
    - `open\`
      - `command\`
        - `(Default) = "executable.exe"`

## Registering Ubuntu Linux Protocol Handlers

For most of the link tricks for Markdown and Typora to work, you need to have the appropriate default Protocol Handlers defined at the operating system level.

### Common Problem: Links Open Text Editor Instead of Launching Script

This means your system doesn't default to executing script files.

To fix this, open the File Explorer, find a script file `.sh` to use as the guinea pig, right-click on it, select "Open With" tab, then ... ==TBD==