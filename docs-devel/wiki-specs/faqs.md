# FAQs

[TOC]

## Wiki data repo is too large, how can I reduce the size?

==TBD==

## How can I restore a previous version of a page?

==TBD==

## How can I link to a specific wiki page?

==TODO: this is not ideal as it doesn't open the wiki root directory==

```shell
$> typora ~/path/to/wiki-data/page.md
```

