# Snippets

[TOC]

## Overview

Snippets are portions of Markup/ARF Wiki code that have been saved for reuse.

They are similar to [Template Pages](template-pages.md), except those provide whole pages for reuse.

## Accessing Snippets

==Hotkey?==



