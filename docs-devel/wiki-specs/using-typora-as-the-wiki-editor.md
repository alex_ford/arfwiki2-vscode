# Using Typora as the Wiki Editor

[TOC]

Typora is a pretty amazing GUI application for dealing with Markdown documents. It includes just enough functionality to make this idea seem practical! But, with that said, the devil is always in the details... and there will always be room for improvement. This page documents those!

## Configuration and Tweaks

Go through the settings and change them to your liking! I made a lot of changes from the default installation...

The rest of these involve opening Advanced Settings, then the conf.user.json file and apply the following:

### Speed Up Auto Save

```json
  "autoSaveTimer" : 0.1,
```

### More Search Engines

```json
"searchService": [
    ["Search with Google", "https://google.com/search?q=%s"],
    ["Search with Wikipedia", "https://en.wikipedia.org/wiki/Special:Search/%s"],
    ["Search with Amazon", "https://www.amazon.com/s?url=search-alias%3Daps&field-keywords=%s"]
  ],
```

## Limitations and Workarounds

### No auto creation of links to non-existent pages

You can create a link using typical Markdown syntax, but if you link to a non-existent page, Typora simply complains that it does not exist. What other wikis will do is they will create the page at the moment the link is clicked.

The only thing I've got to work around this for now is to simply create the page manually by right-clicking in the sidebar, or pressing CTRL + N.

### ~~No Table of Contents for inclusion on pages~~

This isn't the end of the world, as I just realized Typora has a pretty clean and straightforward "sidebar" that shows the entire wiki's directory structure and files! It also includes an Outline tab which shows a hierarchy of the headings on the current page.

And, just realized that this is wrong entirely, as Typora does support a special markup which inserts a live Table of Contents on the page! Just surround the letters TOC with square brackets and Typora will do the rest!

### No Search Capability

...

### No Statistical Introspection Capability

This is a helpful and somewhat just "fun" capability that tells you how many pages there are, how many intralinkages exist, how many orphaned pages, which pages are orphaned, etc.

### Typora limited to desktop environments

While Typora is wonderful on Windows, Linux, and presumably Mac, there isn't an Android version.

### No Revision Capability in the GUI

While we can use Git under the hood to provide revision history and distributed access, none of these features are exposed to Typora. Therefore, the nice menu items or buttons you might have in other wikis to view things like revision history, are not available.

Revision history must be done outside of Typora, using Git and perhaps another piece of software to help make working with Git easier.

