# Running Local Scripts

[TOC]

## Overview

Linking to either a .sh or .bat script, then pressing CTRL + Click simply opens the script up in the system's default text editor.

[sh script](../../arfwiki-src/hello-world.sh)

[bat script](../../arfwiki-src/hello-world.bat)

Linking to a .py file does the same thing.

[py script](../../arfwiki-src/hello-world.py)