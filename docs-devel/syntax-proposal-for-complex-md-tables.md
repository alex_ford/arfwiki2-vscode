syntax-proposal-for-complex-md-tables
====

# Option 1

- Can introduce a new syntax which allows breaking long text across newlines

| COL1 | COL2 | COL3 |
| ==== | ==== | ==== |
|


# Option 2

- Can introduce a new syntax which allows for referential content to be rendered in the table, but defined elsewhere as a paragraph in the markup.

|   |   |   |
| - | - | - |
|   |   |   |
