# Markdown Page Templating

## Options

- Cheetah3
    - http://cheetahtemplate.org/
    - A Python-powered template engine. It is agnostic to what the template is for (i.e. Markdown)
    - ...
- VS Code Snippets
    - ...
- [Genshi](https://genshi.edgewall.org/)
- [Jinja2](https://github.com/pallets/jinja)
- [Mako](http://www.makotemplates.org/)
