# searching markdown

## Options

- ElasticSearch
- [Whoosh](http://whoosh.readthedocs.io/en/latest/)
- Solr
    - https://github.com/edsu/solrpy
    - https://github.com/django-haystack/pysolr
- VS Code quick-open by filename
- VS Code search files
- [BernhardWenzel/markdown-search: Search engine for markdown files with tagging](https://github.com/BernhardWenzel/markdown-search)
