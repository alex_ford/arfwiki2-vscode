# python markdown post processor

Looking for a tool developed in Python that runs flexible post-processing steps over a collection of Markdown files. Specifically, post processing means parsing the Markdown, and adding/removing to it, in order to improve the page.

## Specific Use Cases

- Option to convert all inline links to referential links, as well as the reverse
- Evaluate simple math expressions which do not already have a result
- Ensures that Table of Contents and Table of Pages remain up-to-date
- Replacement of "Wiki Vars" that a user types into a page
- Analytics which are cached into the Properties section of a page for faster analysis later on
- Synchronizing data to/from Markdown pages with external resources
- Live-file-previews for various file types which can be easily converted to images (e.g. Excel -> JPG)
- Live-file-references for various file types... updates to either one are synchronized to the other!
    - Excel <> Table
    - ...
- Reminder execution
- Smart pages, which are entirely generated based on other page content in the wiki
    - ...
- Collecting tags
    - Per the ARF Wiki 2 Tag Spec:
        - @ for people
        - # for general tags (hashtags)
        - ...
- Auto-linking and other tag enrichment
    - User simply types @MK and it will eventually be converted to [@MK](../../link-to-mk.md)
- Download certain files from links inserted into page
    - User simply inserts the link, and configures the postprocessor to enable it for that file type.
    - Postprocessor will download the link, file it into `assets`, and replace the text in the Markdown page appropriately. (The original URL can be retained as the 'alt text' or as a 'caption/citation'.)
    - Most commonly used for image files, audio files, video files, etc.
    - Ability to whitelist/blacklist and filter on Internet domains.
- Auto-update Feed Widgets
    - A Feed Widget can be placed on any page and it will be updated by the postprocessor.
    - By default, a Feed Widget could be placed on the main home.md page.
- Execute and display code output
    - User inserts python code into a code fence. The postprocessor will execute the code and show the results as a matching code fence using the 'output' language type.
