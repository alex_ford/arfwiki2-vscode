# other interesting python libs

- [schedule — schedule 0.4.0 documentation](https://schedule.readthedocs.io/en/stable/)
- [r0x0r/pywebview: A lightweight cross-platform native wrapper around a webview component that allows to display HTML content in its own dedicated window](https://github.com/r0x0r/pywebview/)
- [Pygal — pygal 2.0.0 documentation](http://www.pygal.org/en/latest/)
- [Welcome to Bokeh — Bokeh 0.13.0 documentation](http://bokeh.pydata.org/en/latest/)
- [Dash by Plotly - Plotly](https://plot.ly/products/dash/)
- [Python Data Analysis Library — pandas: Python Data Analysis Library](http://pandas.pydata.org/)
