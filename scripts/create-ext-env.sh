#!/bin/bash

# Should this really be here? Most likely this should be part of the vscode-extmgr project but since arfwiki2 is my "guineapig" for this, it's here for now.

# Manual
# ls | grep -i <keyword> | xargs cp -rt ../extensions-arfwiki2/

# Default vscode path is different on each platform

# Right now, this is just a really "dumb" way to capture all of the extension dependencies ARFWIKI2 has.
pushd ${HOME}/.vscode/extensions
mkdir -p ../extensions-arfwiki2/
cp -r alanwalk.markdown-navigation-1.0.3 ../extensions-arfwiki2/
cp -r amoosbr.markdown-nomnoml-0.0.2 ../extensions-arfwiki2/
cp -r archety.material-icons-0.2.0 ../extensions-arfwiki2/
cp -r axetroy.vscode-markdown-script-0.0.1 ../extensions-arfwiki2/
cp -r bierner.markdown-emoji-0.0.7 ../extensions-arfwiki2/
cp -r bierner.markdown-footnotes-0.0.4 ../extensions-arfwiki2/
cp -r bierner.markdown-mermaid-1.0.0 ../extensions-arfwiki2/
cp -r chintans98.markdown-jira-1.1.0 ../extensions-arfwiki2/
cp -r csholmq.excel-to-markdown-table-1.1.0 ../extensions-arfwiki2/
cp -r davidanson.vscode-markdownlint-0.20.0 ../extensions-arfwiki2/
cp -r equinusocio.vsc-material-theme-2.4.2 ../extensions-arfwiki2/
cp -r fabiospampinato.vscode-highlight-1.2.9 ../extensions-arfwiki2/
cp -r fcrespo82.secretlens-1.5.0 ../extensions-arfwiki2/
cp -r fcrespo82.secretlens-2.0.0 ../extensions-arfwiki2/
cp -r geeklearningio.graphviz-markdown-preview-0.0.7 ../extensions-arfwiki2/
cp -r jebbs.markdown-extended-0.9.5 ../extensions-arfwiki2/
cp -r jock.snippets-project-0.0.2 ../extensions-arfwiki2/
cp -r joshbax.mdhelper-0.0.11 ../extensions-arfwiki2/
cp -r jrieken.md-navigate-0.0.1 ../extensions-arfwiki2/
cp -r lextudio.restructuredtext-78.0.0 ../extensions-arfwiki2/
cp -r markdown-bundle ../extensions-arfwiki2/
cp -r marvhen.reflow-markdown-2.0.0 ../extensions-arfwiki2/
cp -r mdickin.markdown-shortcuts-0.8.1 ../extensions-arfwiki2/
cp -r pkief.material-icon-theme-3.6.0 ../extensions-arfwiki2/
cp -r qcz.text-power-tools-1.2.1 ../extensions-arfwiki2/
cp -r romanpeshkov.vscode-text-tables-0.1.5 ../extensions-arfwiki2/
cp -r ryu1kn.text-marker-1.9.0 ../extensions-arfwiki2/
cp -r seanmcbreen.mdtools-1.0.1 ../extensions-arfwiki2/
cp -r shd101wyy.markdown-preview-enhanced-0.3.8 ../extensions-arfwiki2/
cp -r silvenon.mdx-0.1.0 ../extensions-arfwiki2/
cp -r statiolake.vscode-markdown-run-snippet-0.2.0 ../extensions-arfwiki2/
cp -r telesoho.vscode-markdown-paste-image-0.9.1 ../extensions-arfwiki2/
cp -r tomashubelbauer.markdown-jira-links-1.0.0 ../extensions-arfwiki2/
cp -r tomashubelbauer.vscode-markdown-email-links-2.0.0 ../extensions-arfwiki2/
cp -r tomashubelbauer.vscode-markdown-link-suggestions-12.0.3 ../extensions-arfwiki2/
cp -r tomashubelbauer.vscode-markdown-todo-11.0.0 ../extensions-arfwiki2/
cp -r tyriar.theme-sapphire-0.7.0 ../extensions-arfwiki2/
cp -r wangeleile.link4md-0.2.0 ../extensions-arfwiki2/
cp -r wangeleile.wikilink4md-0.2.0 ../extensions-arfwiki2/
cp -r yzhang.markdown-all-in-one-1.6.0 ../extensions-arfwiki2/
popd
