#!/bin/bash

# Shell script for starting ARFWIKI backed by Visual Studio Code.
#
# Author: Alex Richard Ford

# TODO: change this to expect that ARFLIB_BASH is defined in environment
    # and fail if it is not!

# Reference shared bash functions from ARFLIB
ARFLIB_HOME="$(realpath "${HOME}/Workspaces-Personal/arflib/")"
ARFLIB_BASH="$(realpath "${ARFLIB_HOME}/submodules/arflib-bash/")"
source "${ARFLIB_BASH}/src/logging.sh"
log_info "Starting ARF Wiki 2 - Visual Studio Code"

# Resolve this script's absolute path
SCRIPT_NAME="$(realpath ${0})"
if [ "${SCRIPT_NAME}" == "bash" ]; then
    # This handles the case if/when this script is sourced instead of executed
    SCRIPT_NAME="${BASH_SOURCE}"
fi
log_debug "SCRIPT_NAME is: ${SCRIPT_NAME}"
SCRIPT_DIRNAME="$(dirname ${SCRIPT_NAME})"
log_debug "SCRIPT_DIRNAME is: ${SCRIPT_DIRNAME}"
SCRIPT_REALPATH="$(realpath ${SCRIPT_DIRNAME})"
log_debug "SCRIPT_REALPATH is: ${SCRIPT_REALPATH}"

# Set terminal window title
xtitle "ARF WIKI - VISUAL STUDIO CODE"

# Launch Python start script to kick-off isolated vscode environment
python3 "${SCRIPT_REALPATH}/launch-arfwiki2.py"
