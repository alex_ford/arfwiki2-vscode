#!/bin/bash

# Author: Alex Ford (arf4188@gmail.com)
# Website: http://www.alexrichardford.com
# License: MIT License

# Attempt to lookup `docker-machine` and capture the return value as proof
# the system is using Docker Machine or not
# 0 = yes, system is using Docker Machine
# Else = no, system is not using Docker Machine
which docker-machine
USING_DOCKER_MACHINE=$?
if [[ ${USING_DOCKER_MACHINE} -eq 0 ]]; then
    echo "[ INFO ] Docker Machine detected!"
    docker-machine active
    if [[ $? == "False" ]]; then
        echo "[ ERRO ] No Docker Machine detected in this shell's configuration. Did you run `docker-machine env` first?"
        exit 1
    fi
else
    echo "[ INFO ] Docker Machine not detected, will continue with native Docker."
fi

# Now we can build the Docker image
docker build -t arfwiki2 .
