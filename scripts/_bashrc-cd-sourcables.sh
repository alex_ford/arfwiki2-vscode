#!/bin/bash
# source me!

# An entry in the user's .bashrc file should look somethign like this:
#   source /path/to/this/file.sh
# 
# Author: Alex Richard Ford (arf4188@gmail.com)

function print_debug_var() {
    if [ "${DBUG}" != 0 ]; then
        varName="${1}"
        cmd="\${${varName}}"
        echo "[ DBUG ] Literal command: '${cmd}'"
        echo "[ DBUG ] '${varName}' is: '$(eval echo $(eval 'echo ${cmd}'))'"
    fi
}

export ARFWIKI2_SCRIPTS_DIR="$(dirname $(realpath ${BASH_SOURCE}))"
print_debug_var "ARFWIKI2_SCRIPTS_DIR"
export ARFWIKI2_VSCODE_DIR="$(dirname ${ARFWIKI2_SCRIPTS_DIR})"
print_debug_var "ARFWIKI2_VSCODE_DIR"
export ARFWIKI_ROOT="$(dirname ${ARFWIKI2_VSCODE_DIR})"
print_debug_var "ARFWIKI_ROOT"

function cd-arfwiki() {
    cd "${ARFWIKI_ROOT}"
}

function cd-arfwiki-app() {
    cd "${ARFWIKI2_HOME}"
}

function cd-arfwiki-data() {
    cd "${ARFWIKI_ROOT}/arfwiki-data"
}

function cd-arfwiki-assets() {
    cd "${ARFWIKI_ROOT}/arfwiki-assets"
}

