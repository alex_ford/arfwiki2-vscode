#!/bin/bash

###
# Install script for CLI and Python based tooling. This script leverages
# functions/code from ARFLIB-BASH, and is intended to support the
# following environments:
#    * Ubuntu 18.04 Desktop/Server
#    * Termux (Android) *nix-like
#    * WSL/Ubuntu 18.04 on Windows 10
#
# Usage:
#   * Copy this script to a location suitable for the project.
#   * Review this script and follow instructions for each TODO comment found.
#
# Discussion of noteworthy differences follows:
#   * On Termux, the system operates in "single-user" mode, and does not have a
#     "sudo" command, nor any need for it. This is handled by this script by
#     simply looking for the 'sudo' command, and if it's not found, it is not
#     used.
#   * Python is referenced differently across the environments... ultimately
#     we want to be using Python 3, so code is included which attempts to
#     resolve this.
#   * The "user local bin" directory is not the same across systems. The most
#     common is "/usr/local/bin". If that doesn't exist, then we just resolve
#     to the same location as 'bash' which should always exist.
#
# Author: Alex Richard Ford (arf4188@gmail.com)
# Website: http://www.alexrichardford.com
# License: MIT License
###

# Initial "whereami" bash code, helps resolve paths across different hosts
# Expects the following project structure:
#    ./project                    (PROJECT_HOME_DIR)
#    ./project/scripts/           (PROJECT_SCRIPTS_DIR)
#    ./project/scripts/install.sh (THIS_SCRIPT)
THIS_SCRIPT="$(realpath $0)"
PROJECT_SCRIPTS_DIR="$(dirname ${THIS_SCRIPT})"
# Assumes the project home/root is the next directory above the ./scripts/ dir
PROJECT_HOME_DIR="$(dirname ${THIS_SCRIPT})"

DATA_DIR="$(dirname ${PROJECT_HOME_DIR})/arfwiki-data)"
ASSETS_DIR="$(dirname ${PROJECT_HOME_DIR})/arfwiki-assets)"

# Simple error printout function
function error() {
    EXIT_CODE="$1"
    MSG="$2"
    echo -e "[ \e[31mERRO\e[0m ] ${MSG}"
    exit ${EXIT_CODE}
}
function debug() {
    MSG="$1"
    echo -e "[ \e[32mDBUG\e[0m ] ${MSG}"
}
function info() {
    MSG="$1"
    echo -e "[ \e[30mINFO\e[0m ] ${MSG}"
}

# Resolve sudo, if we should use it or not
which sudo >/dev/null 2>&1
if [ $? == 0 ]; then
    USE_SUDO="sudo"
else
    USE_SUDO=""
fi

# Resolve Python 3 command(s)
# TODO: evolve this to handle other cases
PYTHON3="python3"
apt list ${PYTHON3} | grep ${PYTHON3}
if [ $? != 0 ]; then
    error 1 "could not resolve Python 3 commands for this system!"
fi

which ${PYTHON3} >/dev/null 2>&1
if [ $? != 0 ]; then
    ${USE_SUDO} apt install ${PYTHON3}
    ${USE_SUDO} apt install python3-pip
    ${USE_SUDO} ${PYTHON3} -m pip install pipenv
else
    info "Python 3 already installed!"
fi

# Save current directory so we can go back to it later
# This is preferred over pushd/popd
ORIGIN_DIR="$(pwd)"
cd "${PROJECT_HOME_DIR}"
if [ $? != 0 ]; then
    error 1 "PROJECT_HOME_DIR (${PROJECT_NAME_DIR}) could not be accessed!"
fi
${PYTHON3} -m pipenv --venv >/dev/null 2>&1
if [ $? != 0 ]; then
    ${PYTHON3} -m pipenv install
else
    info "existing virtual environment $(pipenv --venv) detected!"
fi
cd "${ORIGIN_DIR}"

# Attempt to resolve the "user local" bin directory
BIN_DIR="/usr/local/bin"
if [ ! -e "${BIN_DIR}" ]; then
    info "${BIN_DIR} doesn't exist on this system, trying next option..."
    BIN_DIR="$(dirname $(which bash))"
    #error 1 "user's local bin directory could not be determined!"
    #exit 1
fi
info "installing bin links to: ${BIN_DIR}"

# Usage: make_bin_link name
# This expects a ./project/scripts/run-name.sh to exist.
function make_bin_link() {
    NAME="${1}"
    RUN_SCRIPT_PATH="${PROJECT_SCRIPTS_DIR}/run-${NAME}.sh"
    BIN_LINK_PATH="${BIN_DIR}/${NAME}"
    if [ -e "${BIN_LINK_PATH}" ]; then
        ${USE_SUDO} unlink ${BIN_LINK_PATH}
    fi
    ${USE_SUDO} ln -sv "${RUN_SCRIPT_PATH}" "${BIN_LINK_PATH}"
    if [ $? != 0 ]; then
        error 1 "encountered a problem while creating bin links!"
    fi
}

# Create bin links
NAMES=( \
    arfwiki-vscode \
)
for NAME in ${NAMES[*]}; do
    info "creating bin link for: ${name}"
    make_bin_link ${NAME}
done

