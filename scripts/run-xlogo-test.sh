#!/bin/bash

# Use this to run a quick and fast "xlogo" test if you're having issues
# with the main `run.sh` or just want to have some confidence about what is
# and isn't working.
#
# "xlogo" is a very simple X utility that opens a window with a graphical
# "X" drawn on the canvas. That's it. Really!

docker exec -ti --rm \
       -e DISPLAY=$DISPLAY \
       -v /tmp/.X11-unix:/tmp/.X11-unix \
       arfwiki2 xlogo
