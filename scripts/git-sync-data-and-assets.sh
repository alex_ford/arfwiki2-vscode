#!/bin/bash

scriptDir=$(dirname $(realpath $0))
echo "[ INFO ] scriptDir is: ${scriptDir}"

pushd ../arfwiki-data
${scriptDir}/submodules/git-commit-sync/git-sync.sh
popd

pushd ../arfwiki-assets
${scriptDir}/submodules/git-commit-sync/git-sync.sh
popd
