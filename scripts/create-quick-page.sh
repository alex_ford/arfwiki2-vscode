#!/bin/bash

# Create Quick Page provides a fast way for the user to create a new page in the
# unfiled section of his/her wiki.
#
# Author: Alex Richard Ford (arf4188@gmail.com)

THIS_SCRIPT="$(realpath $0)"
SCRIPTS_DIR="$(dirname ${THIS_SCRIPT})"
PROJECT_DIR="$(dirname ${SCRIPTS_DIR})"
WORKSPACE_DIR="$(dirname ${PROJECT_DIR})"
WIKI_DATA_DIR="${WORKSPACE_DIR}/arfwiki-data/"
UNFILED_PAGES_DIR="${WIKI_DATA_DIR}/unfiled/"

QUICK_PAGE_NAME="$(date +%Y%m%d-%H%M%S).md" # e.g. 20190215-173721.md

ORIGIN_DIR="$(pwd)"

# TODO: add some starting content, e.g. page title

cd "${SCRIPTS_DIR}" &&
  code "${UNFILED_PAGES_DIR}/${QUICK_PAGE_NAME}"
  #"${SCRIPTS_DIR}/open.sh" "${UNFILED_PAGES_DIR}/${QUICK_PAGE_NAME}"
cd "${ORIGIN_DIR}"
