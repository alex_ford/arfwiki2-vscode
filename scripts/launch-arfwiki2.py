#! python3

# Launcher script for ARF Wiki 2 (VS Code) via Microsoft Visual Studio Code!
#
# Author: Alex Ford (arf4188@gmail.com)
# Website: http://www.alexrichardford.com
# License: MIT (See LICENSE.md)

import logging
from os import makedirs
from os.path import dirname, realpath, exists
import sys
import subprocess
from argparse import ArgumentParser
import shutil

# Setup a basic logger
# TODO: Support logging to a file, as defined in ./config/app.config
# TODO: add support for the --verbose CLI arg
logging.basicConfig(level=logging.INFO)
_log = logging.getLogger(__name__)

def _logVar(vName: str, vObj: object):
    _log.info(" {} is: {}".format(vName, vObj))

# Resolve path to VS Code executable
VSCODE_EXE = shutil.which("code")
_logVar("VSCODE_EXE", VSCODE_EXE)

# Grab various directories
SCRIPT_ROOT_DIR = dirname(realpath(sys.argv[0]))
_logVar("SCRIPT_ROOT_DIR", SCRIPT_ROOT_DIR)
assert exists(SCRIPT_ROOT_DIR)

ARFWIKI2_HOME_DIR = dirname(SCRIPT_ROOT_DIR)
_logVar("ARFWIKI2_HOME_DIR", ARFWIKI2_HOME_DIR)
assert exists(ARFWIKI2_HOME_DIR)

VSCODE_WORKSPACE_FILE = realpath("{}/arfwiki2.code-workspace".format(ARFWIKI2_HOME_DIR))
_log.info("VSCODE_WORKSPACE_FILE is: {}".format(VSCODE_WORKSPACE_FILE))
assert exists(VSCODE_WORKSPACE_FILE)

VSCODE_USER_DATA_DIR = realpath("{}/.vscode/user-data/".format(ARFWIKI2_HOME_DIR))
_log.info("VSCODE_USER_DATA_DIR is: {}".format(VSCODE_USER_DATA_DIR))
if not exists(VSCODE_USER_DATA_DIR):
    _log.info("VSCODE_USER_DATA_DIR not found, creating one now!")
    makedirs(VSCODE_USER_DATA_DIR)
assert exists(VSCODE_USER_DATA_DIR)

VSCODE_EXTENSIONS_DIR = realpath("{}/.vscode/extensions/".format(ARFWIKI2_HOME_DIR))
_log.info("VSCODE_EXTENSIONS_DIR is: {}".format(VSCODE_EXTENSIONS_DIR))
if not exists(VSCODE_EXTENSIONS_DIR):
    _log.info("VSCODE_EXTENSIONS_DIR not found, creating one now!")
    makedirs(VSCODE_EXTENSIONS_DIR)
assert exists(VSCODE_EXTENSIONS_DIR)

ARFWIKI_DATA_DIR = realpath("{}/../arfwiki-data/".format(ARFWIKI2_HOME_DIR))
_log.info("ARFWIKI_DATA_DIR is: {}".format(ARFWIKI_DATA_DIR))
assert exists(ARFWIKI_DATA_DIR)

ARFWIKI_ASSETS_DIR = realpath("{}/../arfwiki-assets/".format(ARFWIKI2_HOME_DIR))
_log.info("ARFWIKI_ASSETS_DIR is: {}".format(ARFWIKI_ASSETS_DIR))
assert exists(ARFWIKI_ASSETS_DIR)

# Pages to open immediately on start
WIKI_README_PAGE = realpath("{}/README.md".format(ARFWIKI2_HOME_DIR))
_logVar("WIKI_README_PAGE", WIKI_README_PAGE)
assert exists(WIKI_README_PAGE)
WIKI_HOME_PAGE = realpath("{}/home.md".format(ARFWIKI_DATA_DIR))
_logVar("WIKI_HOME_PAGE", WIKI_HOME_PAGE)
assert exists(WIKI_HOME_PAGE)

def main():
    # [ ] TODO: consider installing autopep8/pylint/etc.
    # [ ] TODO: consider scanning extensions and performing the install right here, instead of having the user manually do it
    _log.info("Launching VS Code!")
    # [ ] TODO: opening CURRENT_MONTH_TX_FILE isn't working for some reason
    # [o] TODO: how can I gain access to vscode's output?
    completedProcess: subprocess.CompletedProcess = subprocess.run([
            VSCODE_EXE,
            "--verbose",
            "--user-data-dir", VSCODE_USER_DATA_DIR,
            "--extensions-dir", VSCODE_EXTENSIONS_DIR,
            VSCODE_WORKSPACE_FILE,
            WIKI_README_PAGE,
            WIKI_HOME_PAGE
        ],
        check=True)
    _log.info("{}".format(completedProcess))

if __name__ == '__main__':
    argParser = ArgumentParser(
        description="Launch script for ARF Wiki 2!"
    )
    argParser.add_argument("--test",
        action="store_true",
        help="Print out environment and diagnostic information then exit before actually launching the app.")
    args = argParser.parse_args()
    if args.test:
        _log.info("Test environment and diagnostic output has finished. Launch without '--test' option to launch the app.")
        exit(0)
    main()
