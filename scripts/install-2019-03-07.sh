#!/bin/bash

# Installs various symlinks and Bash aliases to make working with arfwiki easier!
# Author: Alex Richard Ford (arf4188@gmail.com)

ARFWIKI_VSCODE_SCRIPTS_DIR="$(dirname $(realpath $0))"
ARFWIKI_VSCODE_ROOT_DIR="$(dirname ${ARFWIKI_VSCODE_SCRIPTS_DIR})"
ARFWIKI_ROOT_DIR="$(dirname ${ARFWIKI_VSCODE_ROOT_DIR})"
ARFWIKI_DATA="${ARFWIKI_ROOT_DIR}/arfwiki-data/"
ARFWIKI_ASSETS="${ARFWIKI_ROOT_DIR}/arfwiki-assets/"

USE_SUDO="sudo"
#USE_SUDO=""

BIN_DIR="${HOME}/.local/bin/"

echo "[ INFO ] $0 is starting..."

echo "[ INFO ] Adding 'source' line to user's .bashrc file..."
cat "${HOME}/.bashrc" | grep -i "arfwiki" >/dev/null 2>&1
if [ $? != 0 ]; then
    echo "source ${ARFWIKI_VSCODE_SCRIPTS_DIR}/_start-bashrc-lines.sh" >> ${HOME}/.bashrc
else
    echo "[ WARN ] existing ARFWIKI bashrc lines detected - refusing to add new ones while old ones are present."
fi

echo "[ INFO ] Installing bin links..."
# TODO: convert the bash function/alias for starting ARFWIKI into a bin link
${USE_SUDO} ln -sv "$(realpath ${ARFWIKI_VSCODE_SCRIPTS_DIR}/create-quick-page.sh)" "${BIN_DIR}/arfwiki-create-quick-page"

echo "[ INFO ] $0 is finished!"

