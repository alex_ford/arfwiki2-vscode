#! python3

# Python script that helps create a separate "external environment" for ARF Wiki 2 to run,
# so it doesn't end up clobbering (or getting clobbered by) other instances of vscode.
#
# All the good docs:
# - https://code.visualstudio.com/docs/editor/extension-gallery
# - https://code.visualstudio.com/docs/editor/command-line

import os

import logging
import platform
import shutil
import subprocess
from pathlib import Path


#logging.basicConfig(level=logging.INFO)
logging.basicConfig(level=logging.DEBUG)
logging.info("Starting up!")

def main():
    # Default extensions directory is different on each platform
    defaultExtDir = None
    if platform.system() == "Linux":
        logging.info("We're on Linux!")
        defaultExtDir = "{}/.vscode/extensions".format(Path.home())
    elif platform.system() == "Windows":
        logging.info("We're on Windows!")
        defaultExtDir = "{}\.vscode\extensions".format(Path.home())
    else:
        logging.error("Unsupported operating system '{}'!".format(platform.system()))
        exit(1)
    logging.debug("defaultExtDir is: {}".format(defaultExtDir))

    # Verify that the directory already exists (as it should)
    defaultExtDirPath = Path(defaultExtDir)
    if not defaultExtDirPath.exists():
        logging.error("The default vscode extension directory doesn't exist!")
        exit(1)

    # TODO: provide interactive option to select extensions 1-by-1
    # TODO: provide option to use a workspace's recommended extensions as the list
    # TODO: provide option to use a simple newline delimited file as the list

    # for now, we just do the right thing for arfwiki2 exclusively
    # create external environment extension directory
    #os.mkdirs(".vscode/extensions/")
    externalEnvExtPath: Path = defaultExtDirPath.parent.joinpath("extensions-arfwiki2")
    logging.info("Creating/updating {}".format(externalEnvExtPath))
    os.makedirs(externalEnvExtPath, exist_ok=True)

    # copy over all named extensions from the default ext dir into the new dir
    extList = [
        "alanwalk.markdown-navigation-1.0.3",
        "amoosbr.markdown-nomnoml-0.0.2",
        "archety.material-icons-0.2.0",
        "axetroy.vscode-markdown-script-0.0.1",
        "bierner.markdown-emoji-0.0.7",
        "bierner.markdown-footnotes-0.0.4",
        "bierner.markdown-mermaid-1.0.0",
        "chintans98.markdown-jira-1.1.0",
        "csholmq.excel-to-markdown-table-1.1.0",
        "davidanson.vscode-markdownlint-0.20.0",
        "equinusocio.vsc-material-theme-2.4.2",
        "fabiospampinato.vscode-highlight-1.2.9",
        "fcrespo82.secretlens-1.5.0",
        "fcrespo82.secretlens-2.0.0",
        "geeklearningio.graphviz-markdown-preview-0.0.7",
        "jebbs.markdown-extended-0.9.5",
        "jock.snippets-project-0.0.2",
        "joshbax.mdhelper-0.0.11",
        "jrieken.md-navigate-0.0.1",
        "lextudio.restructuredtext-78.0.0",
        "markdown-bundle",
        "marvhen.reflow-markdown-2.0.0",
        "mdickin.markdown-shortcuts-0.8.1",
        "pkief.material-icon-theme-3.6.0",
        "qcz.text-power-tools-1.2.1",
        "romanpeshkov.vscode-text-tables-0.1.5",
        "ryu1kn.text-marker-1.9.0",
        "seanmcbreen.mdtools-1.0.1",
        "shd101wyy.markdown-preview-enhanced-0.3.8",
        "silvenon.mdx-0.1.0",
        "statiolake.vscode-markdown-run-snippet-0.2.0",
        "telesoho.vscode-markdown-paste-image-0.9.1",
        "tomashubelbauer.markdown-jira-links-1.0.0",
        "tomashubelbauer.vscode-markdown-email-links-2.0.0",
        "tomashubelbauer.vscode-markdown-link-suggestions-12.0.3",
        "tomashubelbauer.vscode-markdown-todo-11.0.0",
        "tyriar.theme-sapphire-0.7.0",
        "wangeleile.link4md-0.2.0",
        "wangeleile.wikilink4md-0.2.0",
        "yzhang.markdown-all-in-one-1.6.0",
    ]
 
    for extension in extList:
        print("... {}".format(extension))
        # Windows: for some reason, all the extensions are set to read-only... not sure why... so we'll just re-download instead
        #shutil.copy(defaultExtDirPath.joinpath(extension), externalEnvExtPath)
        # can use 'code --extension-dir <dir> --install-extension <extension-id>
        subprocess.run(["code",
                        "--extension-dir",
                        externalEnvExtPath,
                        "--install-extension",
                        extension],
                    shell=True,
                    check=True)

    # TODO: add the .vscode/extensions/ directory to .gitignore

    # TODO figure out if there's a nice way to deal with versions of the extensions (maybe provide mechanism for updating them?)

if __name__ == '__main__':
    main()