ARF Wiki 2 - VS Code
====================

This is the 2nd iteration of the ARF Wiki concept, which focuses on an integrated software stack using: Microsoft Visual Studio Code, Tons of VS Code Extensions, Python 3, Git, MongoDB, and Docker.

```mermaid
graph TD;
    A(Visual Studio Code IDE)
    A --> B
    A --> E
    A --> I
    B(Python Extension)
    C(Git)
    C --> D
    C --> A
    D(Docker)
    D --> A
    E(Markdown All in One)
    F(MongoDB)
    D --> F
    G(Docker Compose)
    G --> D
    H(Python3)
    I(ARF Wiki 2 Scripts)
    H --> I
    I --> F
```

**Table of Contents**

- [Attribution](#attribution)
- [Project Purpose](#project-purpose)
- [Windows Requires Ubuntu Linux Subsystem](#windows-requires-ubuntu-linux-subsystem)
- [Getting Started](#getting-started)
    - [Native Installation](#native-installation)
    - [[WIP] Docker-based Installation](#wip-docker-based-installation)
        - [Docker on Windows](#docker-on-windows)
        - [Docker on Other Operating Systems](#docker-on-other-operating-systems)
        - [Creating Your Docker Machine](#creating-your-docker-machine)
        - [Working With Docker Machine](#working-with-docker-machine)
        - [Building the Docker Image](#building-the-docker-image)
            - [Windows](#windows)
            - [Linux](#linux)
        - [Setting Up an X Window Server](#setting-up-an-x-window-server)
            - [Windows](#windows-1)
            - [Linux](#linux-1)
        - [Run the Docker Container (and thus VS Code for ARF Wiki 2)](#run-the-docker-container-and-thus-vs-code-for-arf-wiki-2)
- [Starting Your New Wiki](#starting-your-new-wiki)
    - [Structure](#structure)
    - [Notes On Submodules](#notes-on-submodules)
- [MongoDB Usage](#mongodb-usage)
- [Additional Documentation is Embedded in your Wiki](#additional-documentation-is-embedded-in-your-wiki)

# Attribution

- **Author**: Alex Ford (arf4188@gmail.com)
- **Website**: http://www.alexrichardford.com
- **License**: MIT License
- **Code Repository**: https://www.gitlab.com/alex_ford/arfwiki2-vscode

# Project Purpose

Expanding on the overview given at the top of this page, the purpose of this project is to create a unified and feature rich environment for creating **personal wikis**. Most of the sections and pages within your wiki are completely freeform and up to you to decide what content is put on them. There are a handful of "sepcial sections" and special "page widgets" that can be used to make your pages even more powerful.

Once you've gotten all the software installed and running, and you have your wiki repositories created, check out the [Guide to Effective Pesonal Wikis](./guide-to-effective-personal-wikis.md), and [ARF Wiki 2 Quick Reference](arfwiki-2-quick-reference.md). That is the "cheat sheet" if you will, to help you get started and keep being powerful.

Finally, if you used the `create-new-wiki.sh` script to start your wiki, it will have copied over lots of documentation that you can read through and better understand some of the features, what software component provides them, etc.

# Windows Requires Ubuntu Linux Subsystem

**DEPRECATED CONTENT** *Here for informational purposes only. This will eventually be removed.*

This project will totally run on Windows, at least Windows 10... with the Ubuntu Linux Subsystem installed. Please make sure to install that before attempting to run the software!

# Getting Started

There are a few ways you can install the software. You can install it natively, which is more traditional and probably more comfortable for non-advanced users, or you can install it via Docker.

## Native Installation

1. Install Microsoft Visual Studio Code.
2. Ensure you have the `arfwiki2-vscode` software on your system. (i.e. this repository!)
3. Launch a special instance of VS Code by using the provided scripts:
    ```shell
    python ./launch-arfwiki.py
    ```
4. This will copy the `./config/app.defaults.config` to `./config/app.config` if this is your first time using the software.
    - You are free to edit the configuration in this file if you desire. Restart ARF Wiki 2 to apply changes.
4. Open Extensions in the side bar, and using the Extension Menu, select "Show Recommended Extensions".
5. Install all of the recommended extensions (if desired). NOTE: there is a button at the top of the side bar which allows you to do this with one click!
6. After all extensions are installed, close VS Code and relaunch it using the script as done before.

## [WIP] Docker-based Installation

ARF Wiki 2 can use Docker in order to bundle up a highly customized and configured instance of VS Code without you needing to know how to do it yourself!

### Docker on Windows

On Windows, Docker runs within a Virtual Machine either supported by the
VirtualBox or Hyper-V virtualization software. Make sure you have an adequate
`default` VM to build and run the ARF Wiki 2 container.

### Docker on Other Operating Systems

TBD

### Creating Your Docker Machine

*Applies to both Windows and Mac systems. Does **NOT** apply to Linux!*

If you haven't already created a Docker Machine VM that you are
happy with, you can use the following command to ensure you create
one that will have adequate resources for the build and runtime of
ARF Wiki 2.

```bash
docker-machine create --driver "virtualbox" --virtualbox-cpu-count "2" --virtualbox-memory "2048" --virtualbox-disk-size "40000" --virtualbox-ui-type "headless" default
```

### Working With Docker Machine

*Applies to both Windows and Mac systems. Does **NOT** apply to Linux!*

Check on your Docker Machine and/or start it if needed.

```powershell
docker-machine status
```

> Stopped

```powershell
docker-machine start
```

> Starting "default"...<br/>
> (default) Waiting for an IP...<br/>
> ...

You may also have to run the following in your PowerShell console before any
other Docker command:

```powershell
& docker-machine env default | Invoke-Expression
```

This ensures that your environment has the correct settings so that your shell
can communicate with the Docker software running in your Docker Machine. NOTE: if you're not using `default` as your Docker Machine for this, then you need to replace that with the name of your machine.

More help can be found on [Troubleshooting Docker on Windows](toubleshooting-docker-on-windows.md)

### Building the Docker Image

Use the provided `build.sh` script to build the Docker Image from the
Dockerfile. (A Docker Image is used in order to start a Docker Container!) Only Bash shell scripts are provided, but they should actually run on Windows without too much trouble. From a PowerShell prompt, just prefix each Bash script with `sh` before executing.

#### Windows

```powershell
sh .\build.sh
```

#### Linux

```bash
./build.sh
```

### Setting Up an X Window Server

#### Windows

Download and install your preferred X Window Server... here is a short (incomplete) list of some options:

- [Xming X Server for Windows](https://sourceforge.net/projects/xming/)
- [VcXsrv Windows X Server](https://sourceforge.net/projects/vcxsrv/)

#### Linux

There's nothing to do here! Congrats, as a Linux user, you already have an X Window Server running... (it's actually responsible for many parts of the Desktop UI you're using right now!)

### Run the Docker Container (and thus VS Code for ARF Wiki 2)

```bash
./run.sh
```

# Starting Your New Wiki

The location of your wiki data is a little different in ARF Wiki 2; instead of being in a folder just outside of the software files, in ARF Wiki 2, by default we utilize a `data` sub-directory to store all of
the wiki data. The folders within are technically all submodules managed by the arfwiki2-vscode's local Git configuration.

If you have an existing wiki, perform the following commands:

```shell
cd ./arfwiki2/
mkdir <wiki-name>
git clone <md-repository-url> <wiki-name>/md-files/
git clone <assets-repo-url> <wiki-name>/asset-files/
```

==The following is for FUTURE reference.==

You can get started fast and easy by using the ARF Wiki 2 Command Line Interface, a.k.a. CLI.

```shell
python ./cli/cli.py
```

```shell
python ./cli/cli.py create "My First Wiki" "This is where all my awesome content will go."
```

## Structure

`./data/my-wiki/md-files/`

In the above directory, you will find an independent Git repository that contains all of your Markdown files.

`./data/my-wiki/asset-files/`

In this directory, you will find an independent LFS repository that contains all of your non-Markdown files (e.g. images, audio clips, etc.).

## Notes On Submodules

It was determined that using Git Submodules would not be a good
fit for the need here because this implies that the actual source
project (`arfwiki2-vscode`) would always need to know about each
and every wiki... and this just isn't the case. A "hidden" directory of embedded wikis, and their git repos, just makes more
sense.

# MongoDB Usage

**NOT IMPLEMENTED YET**

It's on the roadmap to attempt to use something like MongoDB (though maybe the selection will change by the time it actually gets used). MongoDB would provide for a nice "engine" that gives us query, search, sort, and filtering capabilities without doing a lot of heavy lifting myself in Python code.

At the moment, I'm thinking that each section of a wiki is mapped to a MongoDB Collection, and each page in a section is mapped to a Document in the Collection.

# Additional Documentation is Embedded in your Wiki

As a sort of "eat your own dogfood" thing, the vast majority of the
documentation for this project is actually stored within Wiki pages, which will
have been copied into your new Wiki if you used the scaffolding script to
get your Wiki started. **OR ATLEAST IT WILL BE, AT SOME POINT IN THE FUTURE!**
