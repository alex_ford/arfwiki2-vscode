#! python3

# Encryption/decryption sandbox script! This leverages Simple Crypt.
# https://github.com/andrewcooke/simple-crypt
#
# `python3 -m pip install simple-crypt `
#
# Most of this file comes directly from the example given on the github page/README.md
#
# Ideas for ARF WIKI:
# - Could use this to encrypt page content, page filenames, section directory names, etc.
#       - This would produce a folder structure like this:
#           - (d) 736300025de0206888d84d82b3287cf09d04c8235bbaca5c3233de76ca91598ef36b6fee39c616311be20002c1db3710f9e19a9e35a8d6b95a6cb9181a7b0298630c72df90e085d62431bf05cd6aa3589ad4
#               - (f) 73630002c53688d28ed3bd2102d27dd0cfd14fc0e261281cc6a61711ca1b509dca6dfd903b37d85a01ee3a565cdae547a8e350864dbc865732ddb5b5cbd82d933af27baacb2ed268f8ea67e79eae0906.md
#               - (f) 7363000275a3b6ffcc40b7c222c6ce1e801190b69398e977e7808743a8b7008d99971f56896ad641426e919a7d3d35d9632fa8e9c886aef20f8723208c8830dcb5fc8ea500bd655c94c3fab630aeaa0f.md
#       - Pretty nasty, but it would work! These files can then be checked into Git just like regular text.
#       - Encryption can happen at different stages, depending on user desire:
#           - "On-demand", data remains encrypted until user accesses it directly.
#           - "On-checkout", data is decrypted as it is checked out from Git (it is then encrypted before being put back into Git)
#        - Encryption enforcement can be done by `git-commit-sync`, along with page metadata stored at the top of the file in cleartext (even in an encrypted file).
#            - Section metadata is stored in the 'section page' which is defined as the `home.md` page within a given section.
# - Is Simple-Crypt compatible with VS Code SecretLens extension?
#        - A quick live test didn't work... I couldn't use Secret Lens to decrypt a cipher created by Simple-Crypt.

<sl:700dcc47a0343d6fee9efc52d0003f4794a9ea0636d7a53eefcafe0fc85189585fe602fecf400dd1a6e1f16c8c672799:sl>

from binascii import hexlify
from getpass import getpass
from sys import stdin

from simplecrypt import encrypt, decrypt

# read the password from the user (without displaying it)
password = getpass("password: ")

# read the (single line) plaintext we will encrypt
print("message: ")
message = stdin.readline()

# encrypt the plaintext.  we explicitly convert to bytes first (optional)
ciphertext = encrypt(password, message.encode('utf8'))

# the ciphertext plaintext is bytes, so we display it as a hex string
print("ciphertext: %s" % hexlify(ciphertext))

# now decrypt the plaintext (using the same salt and password)
plaintext = decrypt(password, ciphertext)

# the decrypted plaintext is bytes, but we can convert it back to a string
print("plaintext: %s" % plaintext)
print("plaintext as string: %s" % plaintext.decode('utf8'))
