#!/bin/bash

# Creates a new wiki markdown page given just the page slug as input.
#
# Author: Alex Richard Ford (arf4188@gmail.com)

function _to_sentence_case() {
    str="$1"
    # TODO:
}

function newpage() {
    name="$1"
    echo "[ INFO ] creating page '${name}'"
    echo "$name" >> "${name}.md"
    echo "======" >> "${name}.md"
    echo "" >> "${name}.md"
    echo "[ INFO ] '${name}.md' has been created!"
}

function makepage() {
    name="$1"
    url="$2"
    content="$3"

    newpage "${name}"

    echo "${url}" >> "${name}.md"
    echo "" >> "${name}.md"
    echo "${content}" >> "${name}.md"
    echo "[ INFO ] content has been written to '${name}.md'."

    echo " --------------------------------------------------------- "
    cat "${name}.md"
    echo " --------------------------------------------------------- "
}
