# Takes a section, with one or more wiki pages, and consolidates them into a
# single page. Then moves the consolidated page up to the parent directory, and
# removes the section directory. An undo can be performed using Git.

# Demote section into a page
# This takes all the pages in a section and
# combines them into a single page, names the page to a user supplied name or an
# auto generated one, moves it into the parent directory, and then deletes the
# section.
