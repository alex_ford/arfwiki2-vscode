#!/usr/bin/bash

# Allows for various types of queries to be performed over a collection of
# markdown pages.
#
# Author: Alex Ford (arf4188@gmail.com)
# Website: http://www.alexrichardford.com
# License: MIT License

# Standard Python logger with simple basic configuration
logging.basicConfig()
_logger = logging.getLogger(__name__)
_logger.setLevel(level=logging.INFO)

def main():
    pass

if __name__ == '__main__':
    main()
