#! python

from PIL import Image
import sys

# Usage:
# python .\scripts\optimize-image.py ../arfwiki-assets/FF55D301-E098-9888-421F-7DA4A5E4EC80.jpg

filename = sys.argv[1]
print("Working with: {}".format(filename))
basewidth = 300
img = Image.open(filename)
wpercent = (basewidth/float(img.size[0]))
hsize = int((float(img.size[1])*float(wpercent)))
img = img.resize((basewidth,hsize), Image.ANTIALIAS)
outputFilename = "{}.sm.png".format(filename)
print("Saving to: {}".format(outputFilename))
img.save(outputFilename)
