#!/usr/bin/python3

# Creates a new quick page in the "unfiled" section of the wiki.
#
# TODO: optionally takes the name
# of the page. If the name already exists, the page will be opened.
#
# Author: Alex Ford (arf4188@gmail.com)
# Website: http://www.alexrichardford.com
# License: MIT License

import logging
import os
import shutil
from pathlib import Path
from datetime import datetime


# Standard Python logger with simple basic configuration
logging.basicConfig()
_logger = logging.getLogger(__name__)
_logger.setLevel(level=logging.INFO)


def main():
    _logger.info("Starting!")
    userhome = Path.home()
    current_datetime = datetime.now()
    current_dtString = current_datetime.strftime("%Y%m%dT%H%M%S")
    quickPage = "{}/Workspaces-Personal/arfwiki-data/unfiled/{}.md".format(userhome, current_dtString)
    _logger.info("Creating page: {}".format(quickPage))
    # TODO: insert heading into page
    #shutil.copy(src, dst)
    Path(quickPage).touch()
    os.execl("/usr/bin/code", "--reuse-window", quickPage)
    _logger.info("Finished!")


if __name__ == '__main__':
    main()
