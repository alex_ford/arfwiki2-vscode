#!/bin/bash

# @DEPRECATED ??? See the `start-*` scripts located in the parent directory.

# Startup script for a typical user launch of ARF Wiki 2.

code --user-data-dir ~/.arfwiki2/
# might want "--extensions dir" to separate the installed extensions from this instnace of vscode and others?

# TODO
# There are a bunch of tweaks I'd like to apply to the VS Code interface to
# hide some of the more "programmer" oriented features. How can I apply those
# automagically and have `arfwiki2` just start up with those settings?
