#!/bin/bash

ARFWIKI_USER_DATA="${HOME}/Workspaces-Personal/arfwiki2/arfwiki2-vscode/.vscode/user-data"

ARFWIKI_EXTENSIONS="${HOME}/Workspaces-Personal/arfwiki2/arfwiki2-vscode/.vscode/extensions"

ARFWIKI_DATA_DIR="${HOME}/Workspaces-Personal/arfwiki2/arfwiki-data"

# Produce the YEAR, MONTH, and DAY in 0-padded format
YEAR="$(date +%Y)"
MONTH="$(date +%m)"
DAY="$(date +%d)"

FILE="${ARFWIKI_DATA_DIR}/log-stream/${YEAR}/${MONTH}/${DAY}.md"

if [ ! -e "${FILE}" ]; then
    echo "[ INFO ] Log stream file for today did not exist, so it will be created now!"
    #touch "${FILE}"

fi

code --user-data-dir "${ARFWIKI_USER_DATA}" \
    --extensions-dir "${ARFWIKI_EXTENSIONS}" \
    --goto "${ARFWIKI_DATA_DIR}/log-stream/${YEAR}/${MONTH}/${DAY}.md"
