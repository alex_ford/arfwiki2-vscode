# Generates various styles of calendar output formatted in Markdown!
# Here are the available styles:
#   - Annual (12 months)
#   - Bi-annual (6 months)
#   - Quarter-annual (3 months)
#   - Month (1 month)
#   - Bi-weekly (2 weeks, a.k.a. 14 days)
#   - Week (7 days)
# Simply specify the calendar type you want, along with the appropriate
# starting date. Generally speaking, if you do something like request a month
# type and give the date "2018-09-21", you will get the entire month of
# September.

# Author: Alex Ford (arf4188@gmail.com)
# Website: http://www.alexrichardford.com
# License: MIT License

# TODO: implement me!
