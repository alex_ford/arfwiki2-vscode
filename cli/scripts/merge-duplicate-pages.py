#! python3

# Given two Markdown wiki pages which are deemed to be "duplicates" of one
# another by the user, provide facilities to merge them together.
#
# Idea 1: use Meld merge? Or VS Code merge?
#
# Idea: provide "auto" switch that handles common scenarios quickly, such as
# duplicate pages created in the Unfiled section.
#
# Author: Alex Ford (arf4188@gmail.com)
# Website: http://www.alexrichardford.com
# License: MIT License

import logging

# Standard Python logger with simple basic configuration
logging.basicConfig()
_logger = logging.getLogger(__name__)
_logger.setLevel(level=logging.INFO)

def main():
    _logger.info("Someday I will do something!")

if __name__ == '__main__':
    main()
