#! python

# This script implements the ability to execute embedded python code stored
# within Markdown pages. It expects them to be within code fences, along with
# using the 'python' language type.
#
# Example:
# ```python
# ```
#
# The above would attempt to use the first 'python' found in the environments
# path. If explicit is needed, it can be done like this:
#
# ```python {#!/usr/bin/python3}
# ```
#
# Author: Alex Ford (arf4188@gmail.com)
# Website: http://www.alexrichardford.com
# License: MIT License
