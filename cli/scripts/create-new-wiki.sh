#!/bin/bash

# Helps create a new wiki for use with ARF Wiki 2!
#
# Author: Alex Richard Ford (arf4188@gmail.com)
# Website: http://www.alexrichardford.com
# License: MIT License (see LICENSE.md)

# TODO need to redesign this because my assumption that Git would be fine having
# an "embedded" Git repo inside a parent Git repo was wrong!
# Submodules still don't seem like a great idea...
# Maybe we can get Docker to play nice with a directory outside of the parent
# directory of the Dockerfile... or... am I confusing this with Vagrant???

echo "This script will help you start a new wiki using the scaffolding (template) files."
echo "What would you like to call your new wiki? "
read wikiname

echo "Sure! '${wikiname}' sounds great."
echo "Just a second while I copy things over..."
sleep 1
mkdir -vp data/${wikiname}/md-files
mkdir -vp data/${wikiname}/asset-files
sleep 1
cp -vr scaffolding/* data/${wikiname}/md-files/
pushd data/${wikiname}/md-files/
git init
popd

echo "All done! Your new wiki '${wikiname}' should be ready to go!"

# TODO automatically open the new wiki in VS Code

# TODO install shortcuts for Windows and Linux so user can simply type:
# [WIN] + arfwiki2, "wikiname" (or something similar)