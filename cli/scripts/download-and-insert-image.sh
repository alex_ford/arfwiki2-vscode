#!/bin/bash

# STATUS: ideation/not ready for use
echo "[ ERRO ] Not Ready, or Not Yet Implemented!"
exit 1

# Download and insert an image into a wiki page.
#
# The image will be downloaded to the ${wiki_root}/arfwiki-assets/ directory
# and given a UUID for the filename. [The image will then be compressed and
# resized to a maximum of 400x400.] Finally, a Markdown image link will be
# generated and inserted into the specified wiki page.
#
# Author: Alex Richard Ford (arf4188@gmail.com)
# Website: http://www.alexrichardford.com
# License: MIT License (see LICENSE.md)

# Usage example:
# download-and-insert-image.sh https://pbs.twimg.com/profile_images/972154872261853184/RnOg6UyU.jpg ../arfwiki-data/home.md 4 10
IMAGE_URL=${1}
WIKI_PAGE=${2}
WP_LINE=${3}
WP_POSITION=${4}

# Constants/Options
WIKI_ROOT=./
ASSETS_DIR=${WIKI_ROOT}/arfwiki-assets
