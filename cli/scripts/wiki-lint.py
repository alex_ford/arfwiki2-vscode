#! python

# The essential "linter", a series of algorithms that perform tests and checks
# on the wiki content in order to ensure it meets certain levels of quality and
# validity.
#
# Linting Codex
# See [wiki-linter-codex.md](../docs/wiki-linter-codex.md)
#
# TODO: check out https://github.com/MihaZupan/MarkdownValidator
#
#

# Author: Alex Ford (arf4188@gmail.com)
# Website: http://www.alexrichardford.com
# License: MIT License

# TODO: implement start of wiki-lint.py!


