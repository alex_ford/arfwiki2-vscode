#! python

# Computes statistics for a single page in an ARF Wiki instance.
#
# Careful not to conflate linting responsibilities here! The wiki-lint.py script
# can make use of the stats computed by this script, but this script should not
# attempt to make sense or derive meaning from any of the computed stats, simply
# compute and return them.

# Author: Alex Ford (arf4188@gmail.com)
# Website: http://www.alexrichardford.com
# License: MIT License

from os import stat
from argparse import ArgumentParser


def main(args):
    page_stats = {
        'line_count': 0,
        'character_count': 0,
        'page_file_size': 0,
        'page_file_atime': 0,
        'page_file_mtime': 0,
        'page_file_ctime': 0,
        'total_outbound_links': 0,
        'broken_outbound_links': 0,
        'asset_count': 0,
    }
    statinfo = stat(args.page)
    page_stats.update({'page_file_size': statinfo.st_size})
    page_stats.update({'page_file_atime': statinfo.st_atime})
    page_stats.update({'page_file_mtime': statinfo.st_mtime})
    page_stats.update({'page_file_ctime': statinfo.st_ctime})
    with open(args.page) as pageFile:
        for line in pageFile:
            print(line)
            page_stats.update({'line_count': page_stats.get('line_count') + 1})
            page_stats.update({'character_count': page_stats.get('character_count') + len(line)})
    print("{}".format(page_stats))


if __name__ == '__main__':
    argParser = ArgumentParser(
        description="Computes statistics for a single page in an ARF Wiki instance.")
    argParser.add_argument("page", help="The path to a wiki page to compute stats for.")
    args = argParser.parse_args()
    main(args)
