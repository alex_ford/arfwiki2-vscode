#!/bin/bash

# Requires:
#   - Python 3
#   - Pillow (PIL Fork) for Python 3
#   - TODO: Optimize Images VS Code extension

scriptsDir=$(dirname $(realpath $0))
echo ${scriptsDir}

pipenv run python ${scriptsDir}/optimize-image.py ${1}
