#! python3

# Creates a section index, which is really just a listing of the Markdown pages
# and any sub-sections (directories) of a folder, excluding the special
# 'home.md' page which is generally where this index will be used. This does not
# recurse into directories, it only produces a 1 level deep view of them. Icons
# are used, along with a slight naming convention to indicate the difference
# between pages (files) and sections (directories).
#
# Author: Alex Ford (arf4188@gmail.com)
# Website: http://www.alexrichardford.com
# License: MIT License

import logging
import os
from os import DirEntry


# Standard Python logger with simple basic configuration
logging.basicConfig()
_logger = logging.getLogger(__name__)
_logger.setLevel(level=logging.INFO)

def main():
    items = []
    for item in os.scandir():
        item: DirEntry
        print("{}".format(item))
        # get the directories, which are Wiki Sections
        if item.is_dir():
            items.append(item)
        # get the Markdown files, which are Wiki Pages
        if item.is_file():
            pass  # TODO: need to only accept .md files (ignore everything else)
    print("{}".format(items))
    # TODO: # output Markdown formatted section index

if __name__ == '__main__':
    main()
