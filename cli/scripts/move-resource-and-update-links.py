# This script aids in moving (or renaming) any resource in the wiki, be it a
# section, a page, an image, etc. Beyond performing the simple move/rename
# operation, it also scans the entire wiki for link references to the old
# resource and updates them to reflect the new resource.
