#! python3

# Downloads the specified resource to the ${WIKI_ASSETS_DIR} and provides an
# absolute link, or relative link if a relative source is specified, so the link
# can be referenced in a wiki page.
