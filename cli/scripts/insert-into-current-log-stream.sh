#!/bin/bash

# This script attempts to open the Log Stream page for the
# current date within the Log Stream folder structure.
# If the folder/page cannot be found, then it is created.
#
# It should also be configured to run as a task from inside
# VS Code.
#
# Author: Alex Richard Ford (arf4188@gmail.com)
# Website: http://www.alexrichardford.com
# License: MIT License (see LICENSE.md)

# TODO add option to specify different log-streams, with default remaining as the general log-stream

echo "[ INFO ] Current working directory: $(pwd)"
exit

# Determine today's date
year=$(date +%Y)
month=$(date +%m)
day=$(date +%d)
echo "Today is: ${year}-${month}-${day}"

logDir=$(realpath ../arfwiki-data/log-stream/${year}/${month})
echo "[ INFO ] Navigating to: ${logDir}"
pushd ${logDir}
echo "[ INFO ] Current working directory: $(pwd)"
exit

# Determine the wiki root directory (should end with the directory separator character)
# TODO we'll just hardcode this for now
# wikiDataDir=$(realpath ../arfwiki-data/)
# echo "[ INFO ] Wiki data directory is: ${wikiDataDir}"

# # Determine the Log Stream directory (should end with the directory separator character)
# # TODO hardcoding this for now, but should change later
# logStreamDir=${wikiDataDir}/log-stream/
# echo "[ INFO ] Log Stream directory is: ${logStreamDir}"
# echo "[ INFO ] Combined path to Log Stream directory is: ${logStreamDir}"

# # Build up the year/month folder structure
# echo "Creating Log Stream directory structure if it doesn't already exist..."
#  # the logStreamDir should already exist
# pushd ${logStreamDir}
# # we'll create the year and month directory if needed
# mkdir -p ${year}/${month}/
# logStreamPageDir=${logStreamDir}/${year}/${month}/
# echo "[ INFO ] Log Stream Page Dir is: ${logStreamPageDir}"

# Create page for today, if it doesn't already exist
#lineNumber=-1
# pushd ${logStreamPageDir}
# if [ $? -ne 0 ]; then
#     echo "[ ERRO ] An error attempting to navigate into the wiki data directory has occurred!"
#     exit 1
# fi

if [ ! -e ${day}.md ]; then
    echo "[ WARN ] A page for today wasn't found... will create it now!"
    # TODO maybe make this a template or something instead of writing it out line by line like this
    touch ${day}.md
    echo "# ${year}-${month}-${day}" >> ${day}.md
    echo "" >> ${day}.md
    echo "## Log Stream" >> ${day}.md
    echo "" >> ${day}.md
    # TODO insert current time here, to get things started faster!
    echo "- " >> ${day}.md
    echo "[ INFO ] New page created: ${year}/${month}/${day}.md"
else
    echo "[ INFO ] Page for today already exists. Moving on..."
    # TODO insert next time entry into the list, to get things going faster!
fi

# Open the page in VS Code, and position on the new entry about to be written
echo "[ DBUG ] Current working directory: $(pwd)"
echo "[ DBUG ] Contents of working directory: $(ls)"
echo "[ INFO ] Opening in VS Code: ${wikiDataDir}${logStreamPageDir}${day}.md"
code ${day}.md
#code --goto "${day}.md" #:${lineNumber}

# We used 2 pushd commands, so we need 2 popd ones!
popd
popd
