#!/bin/bash

code --verbose \
     --user-data-dir ./.vscode/user-data/ \
     --extensions-dir ./.vscode/extensions/ \
     ./arfwiki2.code-workspace
