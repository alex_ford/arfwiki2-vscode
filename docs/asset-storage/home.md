# Asset Storage

## Overview

## Using a separate Git repo

- You can `rebuild` your repo if you're unhappy with the history of the assets stored within. This basically means creating a new repo, excluding/including whatever it is you want, and then using that new repo from that point forward.

### Checking out with specific number of historical versions downloaded

TBD

### "Lazy" checkout, files only download when they are accessed

Is this even possible with Git?

TBD

## Tips

- Use small file sizes.
    - Resize and compress images to the smallest/lowest amount you can handle.
