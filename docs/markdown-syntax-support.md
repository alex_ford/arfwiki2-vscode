# Markdown Syntax Support

## Markdown Core

## GitHub Flavored Markdown (GFM)

## DocFX

https://dotnet.github.io/docfx/spec/docfx_flavored_markdown.html?tabs=tabid-1%2Ctabid-a

https://marketplace.visualstudio.com/items?itemName=ms-docfx.DocFX

## MDX

https://mdxjs.com/

https://github.com/mdx-js/mdx
