# ARF Wiki 2 Quick Reference

**Table of Contents**
- [ARF Wiki 2 Quick Reference](#arf-wiki-2-quick-reference)
    - [Overview](#overview)
    - [Glossary (Terms and Definitions)](#glossary-terms-and-definitions)
    - [Markdown Syntax and Extended Syntax](#markdown-syntax-and-extended-syntax)
        - [Markdown Syntax Reference](#markdown-syntax-reference)
        - [Extended Syntax Reference](#extended-syntax-reference)
        - [Tagging](#tagging)
    - [Commands and Actions Reference](#commands-and-actions-reference)
        - [Editing](#editing)
        - [Linking](#linking)
    - [Power User Tips](#power-user-tips)

## Overview

This page serves as a quick reference for using ARF Wiki 2.

## Glossary (Terms and Definitions)

ARF Wiki 2 uses a number of terms in very specific ways. Here is the reference for what they mean.

| Term    | Definition |
| ------- | ---------- |
| Section |            |
| Page    |            |

## Markdown Syntax and Extended Syntax

We separate the syntax (the way in which you express Markdown content) into two different categories: Markdown, and Extended. Markdown is the base syntax, and conforms to most of the common implementations of the spec. Your content which uses Markdown syntax will be rendered by virtually any Markdown viewer. Extended Syntax is all of the "added" markup that ARF Wiki 2 introduces either by way of Visual Studio Code Extensions, Widgets, or raw HTML. While some of this may be rendered by other Markdown viewers, it's not gauranteed (actually, it's not likely).

### Markdown Syntax Reference

TBD

### Extended Syntax Reference

TBD

### Tagging

Tags are intended to be a simple way for you to mark your content with specific semantics that the software can also understand well enough to help you with organization, searching, etc.

| Tag | Description |
| --- | --- |
| `@` | Use to tag people, pets, and other subjects. |
| `#` | |
| `%` | |
| `^` | |
| `&` | |
| `*` | |

## Commands and Actions Reference

Commands and Actions are the various functions/operations that you can perform using either a sequence of keystrokes, menus, or buttons.

| Description                                                                                              | Command or Action Sequence                                    | Provider <alttext="Which software component is responsible for providing this feature.">(?)</alttext> |
| -------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------- |
| Save current page.                                                                                       | <kbd>Ctrl</kbd>+<kbd>S</kbd>                                  | VS Code                                                                                               |
| Format page content.                                                                                     | <kbd>Shift</kbd>+<kbd>Alt</kbd>+<kbd>F</kbd>                  | VS Code + Markdown All in One                                                                         |
| Insert an emoji.                                                                                         | ...                                                           | ...                                                                                                   |
| Insert an icon.                                                                                          | ...                                                           | ...                                                                                                   |
| Search for Page by name.                                                                                 | ...                                                           | ...                                                                                                   |
| Search for text on current Page.                                                                         | ...                                                           | ...                                                                                                   |
| Search for text on all Pages.                                                                            | ...                                                           | ...                                                                                                   |
| Open/create today's General Log Stream Page.                                                             | Ctrl+P, task `Insert entry into current Log Stream`                                                       | ARF Wiki 2                                                                                                   |
| Perform manual **Git commit-sync**.                                                                      | <kbd>Ctrl</kbd>+<kbd>P</kbd>, `"task git-commit-sync for current project"` | ARF Wiki 2                                                                                                   |
| Start automatic **Git commit-sync daemon**.                                                              | <kbd>Ctrl</kbd>+<kbd>P</kbd>, `"task auto git commit-sync"`   | ARF Wiki 2                                                                                            |
| Take a quick note, filed under "Unfiled" section.                                                        |                                                               |                                                                                                       |
| Insert quick note into the current Log Stream page.                                                      |                                                               |                                                                                                       |
| Insert first link from Google search for the selected terms.                                             |                                                               |
| Insert link to a Google Search for the selected terms.                                                   |                                                               |                                                                                                       |
| Open browser to Google and search for the selected terms.                                                |                                                               |                                                                                                       |
| Insert link to a Wikipedia Search for the selected terms.                                                |                                                               |                                                                                                       |
| Open browser to Wikipedia and search for the selected term(s).                                           |                                                               |                                                                                                       |
| Insert link to a YouTube search for the selected terms.                                                  |                                                               |                                                                                                       |
| Insert first result from YouTube search for the selected terms.                                          |                                                               |                                                                                                       |
| Open browser to YouTube and search for the selected term(s).                                             |                                                               |                                                                                                       |
| Insert first link from Google Maps search for the selected terms.                                        |                                                               |                                                                                                       |
| Insert directions for the first link from Google Maps.                                                   |                                                               |                                                                                                       |
| Insert information from Google Maps of the first result for the selected terms.                          |                                                               |                                                                                                       |
| Insert link to a Google Image search for the selected term, or image.                                    |                                                               |                                                                                                       |
| Insert the first image from a Google Image search for the selected term.                                 |                                                               |                                                                                                       |
| Insert an image chosen from a pop-up dialog of results from a Google Image search for the selected term. |                                                               |                                                                                                       |
| Evaluate selected text as a mathematical expression and insert the result at the extended. | Type up some math, then Ctrl+Shift+P, "`Calculate`" | [Calculate](docs\vscode-extension-reviews\calculator-feature.md#calculate-(`acarreirocalculate`)) |
| Evaluate selected text as a mathematical expression and replace the text with the result. | Type up some math, then Ctrl+Shift+P, "`Calculate and Replace`" | [Calculate](docs\vscode-extension-reviews\calculator-feature.md#calculate-(`acarreirocalculate`)) |

### Editing

 | Action/Function          | Command/Action Sequence | Provider |
 | ------------------------ | ----------------------- | -------- |
 | Make text bold.          | Ctrl+B                  | VS Code  |
 | Make text italicized.    | Ctrl+I                  | VS Code  |
 | Make text underlined.    | Ctrl+U                  | VS Code  |
 | Make text strikethrough. | Ctrl+-                  |

### Linking

| Action/Function                               | Command/Action Sequence                                                                                                                      | Provider |
| --------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------- | -------- |
| Link, with suggestions, to another wiki page. | Type <kbd>[</kbd><kbd>[</kbd> then wait a second for a suggestion list to show up. Type part of the page name and select what page you want. | ...      |

## Power User Tips

Become a wiki mad-man! (Or mad-woman!) The following are techniques which leverage most of the above in particular ways in order to maximize ARF Wiki 2's usefulness.

TBD
