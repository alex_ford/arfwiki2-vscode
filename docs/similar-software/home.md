# Similar Software

[TOC]

## Overview

This page documents several similar software implementations to `arfwiki`.

## Cherrytree

https://www.giuspen.com/cherrytree/

## Microsoft OneNote

## Google Keep

## Lessers

### QOwnNotes

- Available in the Ubuntu Software Center.

## [[Docuwiki]]

See the [docuwiki.md](docuwiki.md) page for more details.
