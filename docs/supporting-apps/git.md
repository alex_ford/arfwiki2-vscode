# Git

<details><summary>Wiki Page Properties</summary>

```json
{
    date-created: '2018-10-04 18:30 EST',
    created-by: 'alex',
    created-host: 'bg-messier87',
}
```

</details><br/>

- [Git](#git)
  - [Overview](#overview)
  - [How ARF Wiki uses Git](#how-arf-wiki-uses-git)
  - [Why not use DropBox, Google Drive, etc.?](#why-not-use-dropbox-google-drive-etc)

## Overview

>> Git is a version-control system for tracking changes in computer files and coordinating work on those files among multiple people. It is primarily used for source-code management in software development, but it can be used to keep track of changes in any set of files.[^1]

## How ARF Wiki uses Git

...

## Why not use DropBox, Google Drive, etc.?

...

[^1]:https://en.wikipedia.org/wiki/Git <img src="../../../arfwiki-assets/fontawesome/svgs/brands/wikipedia-w.svg" width="16px" height="16px">
