# Troubleshooting Docker on Windows

- [Troubleshooting Docker on Windows](#troubleshooting-docker-on-windows)
    - [Overview](#overview)
    - [Docker Machine fails to response to `docker-machine` commands](#docker-machine-fails-to-response-to-docker-machine-commands)
    - [Docker Machine storage runs out](#docker-machine-storage-runs-out)
    - [Docker Machine is slow, not performing well](#docker-machine-is-slow-not-performing-well)
    - [Docker Machine fails to connect with TLS error](#docker-machine-fails-to-connect-with-tls-error)

## Overview

This page documents several problems you might run into while attempting to run
Docker on Windows, and how you can potentially solve the problems.

Make sure you've read through the [README.md](./README.md) as well.

## Docker Machine fails to response to `docker-machine` commands

...

## Docker Machine storage runs out

...

## Docker Machine is slow, not performing well

...

## Docker Machine fails to connect with TLS error

If you encounter this error, or something similar to it:

<blockquote style="font-family: monospace;"><pre>
Error checking TLS connection: ssh command error:
command : ip addr show
err     : exit status 255
output  :
</pre></blockquote>

You can try regenerating the TLS certificates to solve the problem:

```shell
docker-machine regenerate-certs
```

<blockquote style="font-family: monospace;"><pre>
Regenerate TLS machine certs? Warning: this is irreversible. (y/n): y
Regenerating TLS certificates
Waiting for SSH to be available...
</pre></blockquote>