Hosting a Private Git Server
============================

Sure, you could use GitHub, GitLab, or any of the other numerous public providers of Git server services... but then again, the one core aspect of this project is to put the data back in your hands! That means limiting who and where your data is exposed to.

# Install Git Server

It's not hard, check out the official docs! You'll just need a computer that is always online/available and configured so that you can reach it even if you're behind a firewall, router, or other networking devices.

I highly recommend obscuring your SSH port instead of leaving it as "22".

```shell
 # create your SSH keypair if you don't already have one
ssh-keygen
 # create your SSH config if you obscured the port, as recommended above.
 # (replace 22 with whatever you picked, and hostname.tld with the way you can access your server)
echo "Host hostname.tld\n    Port 22\n\n" >> ~/.ssh/config
 # one-liner to upload your SSH key to your server
cat ~/.ssh/id_rsa.pub | ssh user@hostname.tld 'cat .ssh/authorized_keys'
 # now test the SSH connection, it should be password-free!
ssh user@hostname.tdl "echo Hello, from \$(hostname). I\'m password free!"
 # lastly, clone your data directory
git clone user@hostname.tdl:~/path/to/repo
```
