# External Integrations

- [External Integrations](#external-integrations)
    - [Overview](#overview)

## Overview

These are other apps and services which can, or have, been integrated with ARF Wiki 2!

- Contact Managers
    - Windows 10 People App
    - Gmail Contacts
    - Android Contacts
    - Office 365 Outlook Calendar
- Calendar Managers
    - Windows 10 Calendar App
    - Office 365 Outlook Calendar
    - Android Calendar
    - Google Calendar
- Photos and Images
    - Google Photos
- Video
    - Google Photos
    - YouTube
- Music
    - YouTube
- Time Tracking
    - Toggl
    - ...
- Todo and Task Management
    - TickTick
    - Atlassian JIRA
    - ...
- Personal Information Management
    - KeePass
    - ...
- Email
    - Gmail
        - New mail can be recorded as an entry in your Log Stream.
- Finance
    - Beancount
        - "Text-based personal finance" meets "text-based personal wiki".
        - Could include dynamic widgets that use the Beancount system in order to display graphs, values, tables, etc.
