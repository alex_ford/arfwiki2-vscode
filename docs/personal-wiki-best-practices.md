# Personal Wiki Best Practices

## Overview

...

## The List

1. Separate page intent into "Reference Pages", "Note Pages", ...
2. DO adopt the Log Stream concept for notes.
3. Keep page and section names short, but meaningful.
4. DO use unique page and section names, even if they are located in different section heirarcies, as this will reduce the pain in duplicate resolution during searches.
5. DO use links to your other wiki pages as much as possible.
6. DO use links to external resources as much as possible.
7. DO save often, commit often, and push/pull often. (Use `git-commit-sync` if/when available.)