# History of ARF Wiki

- [History of ARF Wiki](#history-of-arf-wiki)
    - [Overview](#overview)

## Overview

ARF Wiki is an old concept for me. I don't remember when I first started essentially writing down my thoughts, ideas, plans, etc. into digital form like this. The earliest I can recall is back in college, when I had stood up what I'll call "ARF Wiki 0", which was just an instance of the Wikipedia software (MediaWiki) installed on my own server.
