:bulb: IDEA

A general extension for inserting some user-defined text into a user-defined file.

Would be useful for `arfwiki2` as the generalized backend for inserting into the log stream.
