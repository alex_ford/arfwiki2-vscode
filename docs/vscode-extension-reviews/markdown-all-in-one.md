# Markdown All in One

## The Bad

- Auto-open Markdown Preview feature doesn't work very well. While it does automatically open a preview of the current Markdown page to the side, it gets pretty messed up when you open additional pages, click links in the preview pane, etc. You can easily end up with a 3+ vertical splits of the editor pane, which you then have to cleanup.
