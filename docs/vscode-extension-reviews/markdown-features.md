# Markdown Features

- [Markdown Features](#markdown-features)
    - [Overview](#overview)
    - [Markdown All in One](#markdown-all-in-one)
    - [markdownlint](#markdownlint)
        - [The Good](#the-good)
        - [The Bad](#the-bad)
    - [Graphviz Markdown Preview](#graphviz-markdown-preview)
        - [The Good](#the-good)
        - [The Bad](#the-bad)
    - [Markdown Emoji](#markdown-emoji)
        - [The Good](#the-good)
        - [The Bad](#the-bad)
    - [MarkDown Link Suggestions](#markdown-link-suggestions)
        - [The Good](#the-good)
        - [The Bad](#the-bad)
    - [Markdown Navigation](#markdown-navigation)
        - [The Good](#the-good)
        - [The Bad](#the-bad)
    - [Markdown Preview Mermaid](#markdown-preview-mermaid)
        - [The Good](#the-good)
        - [The Bad](#the-bad)
    - [Markdown Script](#markdown-script)
        - [The Good](#the-good)
        - [The Bad](#the-bad)
    - [Markdown Shortcuts](#markdown-shortcuts)
        - [The Good](#the-good)
        - [The Bad](#the-bad)
    - [Markdown Table Generator](#markdown-table-generator)
        - [The Good](#the-good)
        - [The Bad](#the-bad)
    - [MarkDown To-Do](#markdown-to-do)
        - [The Good](#the-good)
        - [The Bad](#the-bad)

## Overview

...

## Markdown All in One

## markdownlint

### The Good

- Helps detect Markdown errors which can cause pages not to render properly.

### The Bad

- It ends up flagging a lot of stuff, by default, and conflicts with other extensions like the formatting one.
    - Can the rules be modified enough to make this worth keeping?
    - ...

## Graphviz Markdown Preview

### The Good

- ...

### The Bad

- ...

## Markdown Emoji

### The Good

- ...

### The Bad

- ...

## MarkDown Link Suggestions

### The Good

- ...

### The Bad

- ...

## Markdown Navigation

### The Good

- ...

### The Bad

- ...

## Markdown Preview Mermaid

### The Good

- ...

### The Bad

- ...

## Markdown Script

### The Good

- ...

### The Bad

- ...

## Markdown Shortcuts

### The Good

- ...

### The Bad

- ...

## Markdown Table Generator

### The Good

- ...

### The Bad

- ...

## MarkDown To-Do

### The Good

- ...

### The Bad

- ...
