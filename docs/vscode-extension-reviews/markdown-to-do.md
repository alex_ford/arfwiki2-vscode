# MarkDown To-Do

`tomashubelbauer.vscode-markdown-todo`

- :heavy_check_mark: provides an idependent side-bar panel
- :heavy_check_mark: provides a contribution to the Explorer side-bar panel
- :x: no way to filter todo list
- :x: no way to sort todo list
- :x: not a lot of configuration/settings to tweak to change behavior (only 1, actually)
- :x: doesn't prefix it's namespace to commands, so they are somewhat hard to find in the command pallette

## Conclusion

Not good enough for arfwiki!
