# Markdown Preview Enhanced

https://shd101wyy.github.io/markdown-preview-enhanced/

- :+1: for including a floating action toolbar!
    - Gives you table of contents, refresh preview, and back-to-top buttons that scroll with you down the page.
- :+1: gives you a right-click context menu to access extra features
    - Image Helper
        - Allows entering of URL, file browser for file (copied to assets folder), file browser for file (uploaded to web service)
        - ❓ Can we customize the assets folder location?
            - There is the `markdown-preview-enhanced.imageFolderPath` setting which I've just set to `../arfwiki-assets/`. Let's see if that works!
            - Well, it definitely did it! But not sure if the behavior is exactly what I want. Seems like the "starting directory" might be where the page is located.
            - Specifying link as so: `~/Workspaces-Personal/arfwiki-assets/`
            - ![8C0CBF23-20E8-3782-4C53-17B9658FAFCA.png](../../../arfwiki-assets/8C0CBF23-20E8-3782-4C53-17B9658FAFCA.png)
            - It moved the image to the right place, but it doesn't seem to want to render/display it.
            - ...
- :+1: emojis....
- :+1: does script execution from code blocks
- :+1: extended table markdown syntax for merging cells, etc.
- :+1: wiki links `[[]]`
    - Implements a slightly different protocol than I was using, but it might be OK to just adopt theirs.
    - Spaces are converted to underscores ` ` --> `_`
    - Periods remain untouched.
    - Capitalization remains untouched, and links/pages are case sensitive for their naming.
- :+1: math
- :+1: mermaid markup rendering
- :+1: option to force single preview (`true` by default)
    - I imagine this resolves the problem with the default preview engine in vscode, where it can open a ton of new windows!
- :+1: actually displays images! The default preview doesn't seem to do that...
