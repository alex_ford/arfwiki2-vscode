# VS Code Extension Reviews

## Overview

## Table

| Extension Name (package-id)                                                                                                      | Status                 | Remarks                                                                                             |
| -------------------------------------------------------------------------------------------------------------------------------- | ---------------------- | --------------------------------------------------------------------------------------------------- |
| :emojisense:                                                                                                                     |                        |                                                                                                     |
| Badges                                                                                                                           |                        |                                                                                                     |
| Calculator (lixquid)                                                                                                             |                        |                                                                                                     |
| Code Runner                                                                                                                      |                        |                                                                                                     |
| Create Unique Ids                                                                                                                |                        |                                                                                                     |
| Google Search                                                                                                                    |                        |                                                                                                     |
| Markdown All in One                                                                                                              |                        |                                                                                                     |
| Markdown Extended                                                                                                                |                        |                                                                                                     |
| [Markdown Include](https://marketplace.visualstudio.com/items?itemName=Kevenin.markdowninclude)                                  |                        |                                                                                                     |
| Markdown Navigation                                                                                                              |                        |                                                                                                     |
| Markdown Paste                                                                                                                   | Trial 2018-09-10       |                                                                                                     |
| Markdown Preview Enhanced                                                                                                        |                        |                                                                                                     |
| MarkDown To-Do                                                                                                                   |                        |                                                                                                     |
| markdown-dir                                                                                                                     |                        |                                                                                                     |
| Paste URL                                                                                                                        |                        |                                                                                                     |
| Path Intellisense                                                                                                                |                        |                                                                                                     |
| Planner                                                                                                                          |                        |                                                                                                     |
| snippet-creator                                                                                                                  |                        |                                                                                                     |
| Tasks Panel                                                                                                                      |                        |                                                                                                     |
| [[vscode-journal]]                                                                                                               | Trial as of 2018-09-10 |                                                                                                     |
| vscode-json                                                                                                                      |                        |                                                                                                     |
| vscode-util                                                                                                                      |                        |                                                                                                     |
| wikilink4md                                                                                                                      |                        |                                                                                                     |
| Rainbow CSV                                                                                                                      |                        | This is useful when working with CSV exports provided by financial institutions.                    |
| [Markdown helper](https://marketplace.visualstudio.com/items?itemName=joshbax.mdhelper)                                          | Trial 2018-09-12       | Giving this one a shot because it offers a couple different Markdown shortcuts like new table rows. |
| [Markdown Navigate - Visual Studio Marketplace](https://marketplace.visualstudio.com/items?itemName=jrieken.md-navigate)         |                        |                                                                                                     |
| [Markdown Footnotes - Visual Studio Marketplace](https://marketplace.visualstudio.com/items?itemName=bierner.markdown-footnotes) |                        |                                                                                                     |
| https://marketplace.visualstudio.com/items?itemName=axetroy.vscode-markdown-script                                               |
| [Marginalia - Visual Studio Marketplace](https://marketplace.visualstudio.com/items?itemName=jamesnorton.marginalia)             |
| [vscode-journal - Visual Studio Marketplace](https://marketplace.visualstudio.com/items?itemName=pajoma.vscode-journal)          |


### Extending Display Capabilities

These allow VS Code to open additional files beyond the ones it is capable of opening by default.

|            |     |     |
| ---------- | --- | --- |
| vscode-pdf |     |     |
| hex-ext    |     |     |

### Task Runners/Executors

| Extension Name  | Package ID                 | Status | Remarks |
| --------------- | -------------------------- | ------ | ------- |
| [[Task Runner]] [<img src="../../../arfwiki-assets/icons/visual-studio.svg" width="20px" height="20px">][0] | `SanaAjani.taskrunnercode` |        |         |

<img src="../../../arfwiki-assets/icons/visual-studio.svg" width="20px" height="20px">


[0]: https://marketplace.visualstudio.com/items?itemName=SanaAjani.taskrunnercode
[1]: ../../../arfwiki-assets/icons/visual-studio.svg
