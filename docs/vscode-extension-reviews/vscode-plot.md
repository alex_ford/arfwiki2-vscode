# vscode plot

<details><summary>Wiki Page Properties</summary>

```json
{
    date-created: '2018-10-05 10:03 EST',
    created-by: 'alex',
    created-host: 'bg-messier87',
}
```

</details>

- [vscode plot](#vscode-plot)
  - [Overview](#overview)
  - [Commands](#commands)
  - [Samples](#samples)

## Overview

> Simple visualizer for numerical sequence.

`henoc.vscode-plot`

## Commands

`Open VSCode Plot`: Open the plot tab. The easy way is to select some numerical sequences and click the context menu.

## Samples

From the sequences of numbers below, select as many lines as you wish, then open the singular command provided by this extension. If the editor opens on top of this one, you won't see anything. Arrange the editors so you can see **both** the editor with the raw data and the editor with the plot, then make the selection to draw it.

1 2 3 4 3
5 1 2 9 1
1 1 2 2 3
3 3 2 2 1

![pasted-image-from-clipboard](../../../arfwiki-assets/2018-10-05-10-18-27.png)

![pasted-image-from-clipboard](../../../arfwiki-assets/2018-10-05-10-17-42.png)
