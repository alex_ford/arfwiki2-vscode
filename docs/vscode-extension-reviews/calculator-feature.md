# Calculator Feature

- [Calculator Feature](#calculator-feature)
    - [Overview](#overview)
    - [Wiki Needs](#wiki-needs)
    - [calc (`borisa.calc`)](#calc-borisacalc)
        - [Review](#review)
    - [Calculate (`acarreiro.calculate`)](#calculate-acarreirocalculate)
        - [Review](#review)
    - [Calculator (`lixquid.calculator`)](#calculator-lixquidcalculator)
        - [Review](#review)

## Overview

This page covers multiple VS Code Extensions that relate to providing calculator features.

## Wiki Needs

- Ability to calculate at least simple arthithmetic expressions right on the Markdown page.

## calc (`borisa.calc`)

**Status**: No Go

- Type up some math, then press `=` to automatically have the result calculated. (This is very much like OneNote!)

### Review

- Couldn't get it to work! Bummer!

## Calculate (`acarreiro.calculate`)

**Status**: In-use

- Select math, then call on the `calculate` command to evaluate.
- Option to replace selected math expression, or append to it.

### Review

- Worked flawlessly! It's just unfortunate the only way to activate the feature is via the Command Palette.

## Calculator (`lixquid.calculator`)

https://marketplace.visualstudio.com/items?itemName=lixquid.calculator

### Review

- Based on the description, it sounds like this could be better than `acarreiro.calculate` extension!
- Doesn't auto detect expressions, and therefore doesn't auto evaluate them either.
- But otherwise, it's nice! Works as advertised. Just press Ctrl+Shift+P, type `calc` and select one of the options.
    - `Calculator: Evaluate`
        - Takes the current selection and evaluates the math! The result is inserted after the expression.
        - Example: Type then select `5+5`, run the action, `5+5 = 10` is now the text on the page.
        - :-1: can't handle dollar signs, e.g. `$1 + $1` doesn't work
    - `Calculator: Show Input Panel`
        - Opens a small command palette prompt that will evaluate math expressions right there.
        - This is basically a very easy and quick to use calculator widget!
        - (Wish) doesn't seem to be a way to insert the result... which this was possible!
    - `Calculator: Count`
        - ...
    - `Calculator: Replace`
        - Similar to the Evaluate operation, but replaces the expression instead of keeping it around.
        - Example: Type then select `5+5`, run the action, `10` replaces the selected text.
