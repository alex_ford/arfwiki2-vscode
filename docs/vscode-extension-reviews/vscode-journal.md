# vscode-journal

```json
{
    date-created: '2018-09-10 10:51 EST',
    created-by: 'alex',
    created-on: 'bg-messier87',
}
```

[TOC]

## Overview

This extension offers functions to quickly open new markdown pages and insert text into various existing pages! Sounds a lot like what I want for ARF Wiki!

- Settings key/namespace: `journal`
- After install, you must open settings and set the base path where the ext will read/save files. Workspace must be reloaded for this change to take effect!
- 2018-09-10 11:04 EST
    - It's pretty good, very similar to what I want... but need a few tweaks. Some stuff might be tweakable from settings... and if the project is open source, I could simply fork it and make the changes I need since it's so close!
