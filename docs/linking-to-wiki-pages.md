Linking to Wiki Pages
=====================

- [VS Code's Built-in Command Line](#vs-codes-built-in-command-line)
    - [Example: drop into current position in the log stream](#example-drop-into-current-position-in-the-log-stream)
- [Protocol Handler](#protocol-handler)
- [URI Structure](#uri-structure)
    - [Page Position Parameters](#page-position-parameters)

# VS Code's Built-in Command Line

```shell
code --user-data ${ARFWIKI_USER_DATA} --extensions ${ARFWIKI_EXTENSIONS} --goto ${FILE}[${LINENUM}[${CHARNUM}]]
```

## Example: drop into current position in the log stream

```shell
ARFWIKI_USER_DATA=${HOME}/Workspaces-Personal/arfwiki2/arfwiki2-vscode/.vscode/user-data

ARFWIKI_EXTENSIONS=${HOME}/Workspaces-Personal/arfwiki2/arfwiki2-vscode/.vscode/extensions

ARFWIKI_DATA_DIR=${HOME}/Workspaces-Personal/arfwiki2/arfwiki-data

# Produce the YEAR, MONTH, and DAY in 0-padded format
YEAR="$(date +%Y)"
MONTH="$(date +%M)"
DAY="$(date +%d)"

FILE="${ARFWIKI_DATA_DIR}/log-stream/${YEAR}/${MONTH}/${DAY}.md"

if [ ! -e "${FILE}" ]; then
    echo "[ INFO ] Log stream file for today did not exist, so it will be created now!"
    touch ${FILE}
fi

code --user-data-dir "${ARFWIKI_USER_DATA}" --extensions-dir "${ARFWIKI_EXTENSIONS}" --goto "${ARFWIKI_DATA_DIR}/log-stream/${YEAR}/${MONTH}/${DAY}.md"
```

# Protocol Handler

ARF Wiki 2 will eventually provide a Protocol Handler that will trigger for `arfwiki2://` URIs.

# URI Structure

The protocol must be `arfwiki2://`.

The next part is the name of the wiki.

The final part is the path to the page.

Full example: `arfwiki2://mywiki:log-stream/2018/09/02.md`

## Page Position Parameters

We may also support specifying a specific header on the page. `arfwiki2://mywiki:log-stream/2018/09/02.md#header-name`

We may also support specifying a specific line number on the page. `arfwiki2://mywiki:log-stream/2018/09/02.md:31`

We may also support specifying a matched text expression, which could be used for finding a date on the page. The following example provides a regex that would match something like "2018-09-02": `arfwiki2://mywiki:log-stream/2018/09/02.md?"[\d]{4}-[\d]{2}-[\d]{2}"`
