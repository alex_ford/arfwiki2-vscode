# Use Svg Icons

- Leveraging the SVG Icons extension `idleberg.svg-icons`
- Provides snippets for popular SVG icons, including Octicons, Evil Icons, Open Iconic, SmartIcons Glyphs, and Bytesize.
- Limited to: html, blade, jsx, vue scopes.
    - Could probably hack this to open it up...
