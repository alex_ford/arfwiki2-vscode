# Use Octicon Icons

https://octicons.github.com/

The entire collection has been downloaded into ./arfwiki-assets/octicons/

They can be referenced like this:

![](../../../arfwiki-assets/octicons/bell.svg)

Or like this:

![][0]

[0]: ../../../arfwiki-assets/octicons/bell.svg

Color can be changed like this:

<img src="../../../arfwiki-assets/octicons/bell.svg" style="width: 20px; height: 20px; foreground="#FF0000">.</i>.

## Embedded GlyphSearch for Octicon icons

<div style="display: block; border-width: 2px; border-style: outset; border-color: lightgrey; box-shadow: 5px 5px 5px; align: center; text-align: center;">https://glyphsearch.com/?library=octicons
    <iframe style="width: 99%; height: 300px; display: block; outline: none;" src="https://glyphsearch.com/?library=octicons">https://glyphsearch.com/?library=octicons</iframe>
</div>
