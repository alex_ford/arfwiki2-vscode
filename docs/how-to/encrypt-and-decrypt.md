# Encrypt and Decrypt

Updated 2018-09-25

- Currently makes use of the VS Code `secretlens` extension.
- Open the page you want to encrypt, select some or all of the content, then issue the SecretLens: Encrypt command.
- To decrypt, simply do the reverse!

## Known Issues

- 2018-10-15: there is no way to currently "lock" SecretLens, so your content remains decryptable with SecretLens until some unknown point in time.
