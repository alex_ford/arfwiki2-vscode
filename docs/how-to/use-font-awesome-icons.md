# Use Font Awesome Icons

- [GlyphSearch for FontAwesome](https://glyphsearch.com/?library=font-awesome)
    - Search for icons from Font Awesome, Foundation, Glyphicons, IcoMoon, Ionicons, Material Design, and Octicons

https://fontawesome.com/how-to-use/on-the-web/setup/getting-started?using=web-fonts-with-css

|                                                                               |                                                                        |                                   |                               |
| ----------------------------------------------------------------------------- | ---------------------------------------------------------------------- | --------------------------------- | ----------------------------- |
| <div align="center"><i class="fa fa-address-book"></i><br/>Address Book</div> | <div align="center"><i class="fa fa-amazon"></i><br/>Amazon Logo</div> | <i class="fa fa-btc"> Bitcoin</i> | <i class="fa fa-usd"> USD</i> |

- [ ] TODO: write a simple iterator script to fill in the above table with the fontawesome files in `${ASSETS}/fontawesome`

## Embedded Search for Font Awesome Icons

<div style="display: block; border-width: 2px; border-style: outset; border-color: lightgrey; box-shadow: 5px 5px 5px; align: center; text-align: center;">https://glyphsearch.com/?library=font-awesome
    <iframe style="width: 99%; height: 600px; display: block; outline: none;" src="https://glyphsearch.com/?library=font-awesome">https://glyphsearch.com/?library=font-awesome</iframe>
</div>
