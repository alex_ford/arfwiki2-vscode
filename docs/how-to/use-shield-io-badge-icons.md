# Use Shield.io Badge Icons

The [official site](https://shields.io/#/) is the gold standard reference!

[TOC]

## Featured Badges

### Cryptocurrency badges

COMING SOON

### License badges

[![Apache License](https://img.shields.io/badge/license-Apache%202.0-orange.svg?style=flat-square)](http://www.apache.org/licenses/LICENSE-2.0)
[![The MIT License](https://img.shields.io/badge/license-MIT-orange.svg?style=flat-square)](http://opensource.org/licenses/MIT)
Proprietary

### Operating System / Platform badges

![windows 10](https://img.shields.io/badge/windows-10-lightgrey.svg?logo=windows) ![windows 8](https://img.shields.io/badge/windows-8-lightgrey.svg?logo=windows)

![ubuntu 18.04](https://img.shields.io/badge/ubuntu-18.04-lightgrey.svg?logo=ubuntu) ![ubuntu 16.04](https://img.shields.io/badge/ubuntu-16.04-lightgrey.svg?logo=ubuntu) ![ubuntu 14.04](https://img.shields.io/badge/ubuntu-14.04-lightgrey.svg?logo=ubuntu)

![mac os x](https://img.shields.io/badge/mac%20os-x-lightgrey.svg?logo=apple)

![android kitkat 4.4](https://img.shields.io/badge/android-kitkat-lightgrey.svg?logo=android)

![ios 8](https://img.shields.io/badge/ios-8-lightgrey.svg?logo=apple)

![web chrome](https://img.shields.io/badge/web--lightgrey.svg?logo=google-chrome)

### Project Info badges

![Project Status](https://img.shields.io/badge/project%20status-NOT%20READY-red.svg) ![Project Status](https://img.shields.io/badge/project%20status-BROKEN-red.svg) ![Project Status](https://img.shields.io/badge/project%20status-ABANDONED-red.svg)
![Project Status](https://img.shields.io/badge/project%20status-ALPHA-orange.svg)
![Project Status](https://img.shields.io/badge/project%20status-BETA-yellow.svg)
![Project Status](https://img.shields.io/badge/project%20status-USABLE-green.svg)

![Project Version](https://img.shields.io/badge/Project%20Version-v0.0.0-blue.svg) ![Project Version](https://img.shields.io/badge/Project%20Version-v0.0.0--alpha-blue.svg)

### Programming Language badges

COMING SOON

## Embedded Shields.io Site

<div style="display: block; border-width: 2px; border-style: outset; border-color: lightgrey; box-shadow: 5px 5px 5px; align: center; text-align: center;">https://shields.io/
    <iframe style="width: 99%; height: 600px; display: block; outline: none;" src="https://shields.io/">https://shields.io/</iframe>
</div>
