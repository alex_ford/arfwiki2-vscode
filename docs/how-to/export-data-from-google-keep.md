# Export Data from Google Keep

<details><summary>Wiki Page Properties</summary>

```json
{
    date-created: '2018-10-03 15:13 EST',
    created-by: 'alex',
    created-host: 'bg-messier87',
}
```

</details>

- [Export Data from Google Keep](#export-data-from-google-keep)
  - [Overview](#overview)
  - [Getting The Data](#getting-the-data)

## Overview

Google Keep is a clean, simple online note taking application that integrates well with the Chrome browser and many other tools. It has a mobile app which is a very convenient place to jot down some notes in free-form without having to worry too much about formatting or organization.

## Getting The Data

Head on over to takeout.google.com and select the appopriate links to get your Google Keep data. Export it to CSV file, and wait for the process to complete. You'll get an email, but also, if you just stay on the page, you'll get a download link once it's ready.

Download the CSV file to your computer, then run the `import-google-keep-csv.py` script on it. (NOT YET IMPLEMENTED)
