# Insert Iframe Content

MP and MPE should be able to just do it! User may need to enable "script/HTML execution".

## Simple Example

<iframe src="https://shd101wyy.github.io/markdown-preview-enhanced/#/markdown-basics?id=references"></iframe>

## Stylized Example

This has been integrated with the `insert-cb-link-as-iframe` snippet.

<div style="display: block; border-width: 2px; border-style: outset; border-color: lightgrey; box-shadow: 5px 5px 5px; align: center; text-align: center;">https://simpleicons.org
    <iframe style="width: 99%; height: 300; display: block; outline: none;" src="https://simpleicons.org">https://simpleicons.org</iframe>
</div>

## Blocked `same-origin`

Not all sites will allow themselves to be embedded.
