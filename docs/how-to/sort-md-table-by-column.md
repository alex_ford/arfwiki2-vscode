# Sort Markdown Table by Column

[TOC]

## Using the Sort Lines by Selection extension

`earshinov.sort-lines-by-selection`

**Status**: WORKING :heavy_check_mark:

| Column A | Column B |
| -------- | -------- |
| 3        | A        |
| 2        | B        |
| 1        | C        |

1. Use the `Add Cursor Below` (or `Above`) command to extend cursor to all affected rows.
2. Select the content of all the affected rows by holding <kbd>Shift</kbd> and using the arrow keys.
3. Use the `Sort lines by Selection` command to sort all the rows!

## Using the Toggle Column Selection extension

`erikphansen.vscode-toggle-column-selection`
TODO
FIXME
DELETE
- [ ] TODO : try out Toggle Column Selection extension for vscode
- [ ] FIXME: this needs to be fixed!
- [ ] DELETE: get rid of this line
