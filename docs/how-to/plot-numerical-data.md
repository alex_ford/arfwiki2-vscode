# Plot Numerical Data

Powered by VS Code Extension: [Markdown Preview Enhanced](../vscode-extension-reviews/markdown-preview-enhanced.md)

The extension allows for rendering of [Plotly](https://plot.ly/) based graphs, as well as Matplotlib and GnuPlot.

See the [extension documentation](https://shd101wyy.github.io/markdown-preview-enhanced/#/code-chunk?id=plotly) for additional details.

**Table of Contents**
[TOC]

## Import the Stylesheet

@import "https://cdn.plot.ly/plotly-latest.min.js"

```markdown
@import "https://cdn.plot.ly/plotly-latest.min.js"
```

## with Python

See the [Python Plotly](https://github.com/plotly/plotly.py/blob/master/README.md) GitHub project.

Install `plotly` module with:

```python
pip install plotly
```

### Python Samples

```python {cmd=true element="<div id='pytest'></div>"}
import plotly.graph_objs as go
dir(go)
fig = go.FigureWidget()
fig
#fig = go.
#fig.
```

## with JavaScript

```javascript {cmd=true  element="<div id='jstest'></div>"}
var TESTER = document.getElementById('jstest')
Plotly.plot(TESTER, [{
    x: [1, 2, 3, 4],
    y: [1, 2, 3, 4]
}, {
    margin: {
        t: 0
    }
}])
''
```
