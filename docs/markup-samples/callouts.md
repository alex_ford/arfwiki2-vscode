Callouts
========
Also known as "admonitions", these are widgets you can use to emphasize certain content on your wiki pages.

https://github.com/vsch/flexmark-java/wiki/Admonition-Extension

https://github.com/vsch/flexmark-java/blob/master/flexmark-ext-admonition/src/main/resources/admonition.css

# Alert / Danger

<div style="display: block;
            width: 99%;
            border-radius: 6px;
            padding-left: 10px;
            margin-bottom: 1em;
            border: 1px solid;
            border-left-width: 4px;
            box-shadow: 2px 2px 6px #cdcdcd;
            border-color: #ebebeb;
            border-bottom-color: #bfbfbf;
            background: #FFEEEE">
    <div style="display: block;
                font-weight: bold;
                font-size: 1.0em;
                height: 0.3em;
                padding-top: 0.3em;
                padding-bottom: 1.4em;
                border-bottom: solid 1px;
                padding-left: 10px;
                margin-left: -10px;
                background: #FF0000;
                color: #FFBBBB;
                border-bottom-color: #dbf3ff;">
        <i class="fa fa-warning">&nbsp;</i>ALERT !
    </div>
    <div style="display: block;
                padding-bottom: 0.5em;
                padding-top: 0.5em;
                margin-left: 1.5em;
                margin-right: 1.5em;">
        The following is dangerous, and should only be attempted by professionals.
    </div>
</div>

# Warning / Attention

<div style="display: block;
            width: 99%;
            border-radius: 6px;
            padding-left: 10px;
            margin-bottom: 1em;
            border: 1px solid;
            border-left-width: 4px;
            box-shadow: 2px 2px 6px #cdcdcd;
            border-color: #ebebeb;
            border-bottom-color: #bfbfbf;
            background: #FFFFEE">
    <div style="display: block;
                font-weight: bold;
                font-size: 1.0em;
                height: 0.3em;
                padding-top: 0.3em;
                padding-bottom: 1.4em;
                border-bottom: solid 1px;
                padding-left: 10px;
                margin-left: -10px;
                background: #EEAA00;
                color: #FFFF55;
                border-bottom-color: #dbf3ff;">
        <i class="fa fa-exclamation-circle">&nbsp;</i>WARNING
    </div>
    <div style="display: block;
                padding-bottom: 0.5em;
                padding-top: 0.5em;
                margin-left: 1.5em;
                margin-right: 1.5em;">
        Be warned, what follows is pretty scary!
    </div>
</div>

# Info / Tips

<div style="display: block;
            width: 99%;
            border-radius: 6px;
            padding-left: 10px;
            margin-bottom: 1em;
            border: 1px solid;
            border-left-width: 4px;
            box-shadow: 2px 2px 6px #cdcdcd;
            border-color: #ebebeb;
            border-bottom-color: #bfbfbf;
            background: #DDDDFF">
    <div style="display: block;
                font-weight: bold;
                font-size: 1.0em;
                height: 0.3em;
                padding-top: 0.3em;
                padding-bottom: 1.4em;
                border-bottom: solid 1px;
                padding-left: 10px;
                margin-left: -10px;
                background: #E8F7FF;
                color: #48C4FF;
                border-bottom-color: #dbf3ff;">
        <i class="fa fa-question-circle">&nbsp;</i>INFO
    </div>
    <div style="display: block;
                padding-bottom: 0.5em;
                padding-top: 0.5em;
                margin-left: 1.5em;
                margin-right: 1.5em;">
        Did you know that you can do almost anything you put your mind to?
    </div>
</div>

