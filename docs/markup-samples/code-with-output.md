# Code With Output

## Option 1

```python
# Change this code fence to the right language
# Then insert your code here!
def myfunc(arg1):
    print("Hello, {}!".format(arg1))
myfunc()
```

Put your output here, either sample or verbatim expected output:

```output
Hello, World!
```

## Option 2

```python
def myfunc(arg1):
    print("Hello, {}!".format(arg1))
    print("Somee, Moree!")
myfunc()
```

> <span style="font-family: monospace;">Hello, World!</span>
> <span style="font-family: monospace;">Somee, Moree!</span>

<blockquote style="font-family: monospace;">
    hi
</blockquote>



- :arrow_double_up:
- ​:arrow_double_down: :+1:  red_triangle_down: the blockquote is not monospace by default.
  - With that said, we can use <span> to force this by setting the `font-family` style attribute to `monospace`.
  - `<span style="font-family: monospace;">output here</span>`
- :small_red_triangle_down: no line numbers, in case you wanted that.



Statement rating icons:

- :arrow_double_up:, :arrow_double_down:
- :arrow_up_small:, :arrow_down_small:
- :arrow_up:, :left_right_arrow:, :arrow_down:
- :+1:, :-1:, :fist:
- :ok:, 
- ​:ballot_box_with_check:, :white_square_button:, :no_entry:
- :white_check_mark:, :negative_squared_cross_mark:
- :heavy_check_mark:, :x: