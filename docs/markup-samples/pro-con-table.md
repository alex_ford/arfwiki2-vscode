### Pro/Con Table

|                             FOR                              |                           AGAINST                            |
| :----------------------------------------------------------: | :----------------------------------------------------------: |
| Displays the message to the user so they can know what's wrong. | Places "temporary" content on a page which must be filtered before sending to VCS. |
|               Relatively simple to implement.                |                                                              |
|                                                              |                                                              |