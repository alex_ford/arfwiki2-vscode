# Snippets

[TOC]

## Overview

This **Special Section** is reserved for "snippets", which are small pre-made and reusable blocks of Markdown/HTML code. These are inserted in other pages, and help to speed up the creation of quality, visually appealing, and consistent content.

## What Makes This Section Special?

Aside from this page, all other pages in this section are **EXEMPT FROM THE PAGE GENERAL PAGE STRUCTURE SPECIFICATIONS**

This section is **PROTECTED**, meaning you cannot change or remove pages. Doing so will simply trigger the wiki engine to restore the page from version control.