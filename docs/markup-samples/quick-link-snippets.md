# Quick Link Snippets

- Implemented via VS Code's built-in snippet feature!
- Best activated by using the Command Palette or by assigning a hotkey.

## Examples

- YouTube
    - Type `hakunamata`, select the text, activate the snippet.
    - [hakunamatata <img src="../../../arfwiki-assets/icons/youtube.svg" style="width: 20px; height: 20px;"/>](https://www.youtube.com/results?search_query=hakunamatata)
