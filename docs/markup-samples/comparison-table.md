## Comparison Table

| :arrow_down: Feature :arrow_down:       :arrow_right: Product :arrow_right: | Product 1 | Product 2 | Product 3 |
| :----------------------------------------------------------: | --------- | --------- | --------- |
|              Feature, quality, or capability 1.              |           |           |           |
|              Feature, quality, or capability 2.              |           |           |           |
|              Feature, quality, or capability 3.              |           |           |           |
|                                                              |           |           |           |
|                                                              |           |           |           |
|                                                              |           |           |           |
|                                                              |           |           |           |
|                                                              |           |           |           |
|                                                              |           |           |           |

