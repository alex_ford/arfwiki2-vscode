# Redacted Text

This is text that appears within the wiki page corpus that is hidden from view by default. It is *not* encrypted, just simply not visible without explicit action to make it so.

## Option 1

This uses HTML/CSS to style a portion of the text with a thin red border, while making the text the same color as the background. It also uses the `warning` emoji to indicate sensitive information.

⚠️ <span style="color: #FFFFFF; font-family: monospace; border-width: 1px;border-style: solid; border-color: red;">Redacted Text Goes Here</span>

⚠️ Label If Desired: <span style="color: #FFFFFF; font-family: monospace; border-width: 1px;border-style: solid; border-color: red;">Redacted Text Goes Here</span>

To see the redacted text, simply select the box and it will be shown!
