Markdown Notes

- Helps create and manage a complimentary notes file associated with any other file in your workspace.
- This notes file contains whatever you want to say about the other file.
- It includes smart links/references back to the target file.
- ...
