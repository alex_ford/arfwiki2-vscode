# Wiki Linter Codex

See [wiki-lint.py](../scripts/wiki-lint.py)

## Overview

ARF Wiki 2 works best when certain opinionated rules and guidelines are followed. A [linter][0] is a software tool that scans files in order to check them in accordance to the rules.

There are basically two categorical levels of results:
- [ WARN ]
    - A warning, as the wiki section/page/line does not follow recommended advice. You can ignore these if you want, but it goes against the recommendation. No functionality will break because of these.
- [ ERRO ]
    - An error, which is a serious violation that will cause functionality to not work. You should **not** leave these alone - fix them!

## Sample Output

```bash
# Provide the wiki data root directory as the first argument
py ./wiki-lint.py ../../arfwiki-data
```

```output
...
```

## Codex Table

| ID       | LEVEL    | DESCRIPTION                                                             | RECOMMENDATION                                                                      |
| -------- | -------- | ----------------------------------------------------------------------- | ----------------------------------------------------------------------------------- |
| LINT-001 | [ WARN ] | Page is more than 500 words in length.                                  | Split up the page into smaller, more focused pages, and use links to connect them.  |
| LINT-002 | [ WARN ] | Section has more than 20 pages.                                         | Add additional sections to organize your pages to help focus general content areas. |
| LINT-003 | [ WARN ] | Page does not start with an `h1` title that is similar to the filename. | Always define a page title and have it be similar to the filename.                  |
| LINT-004 | [ ERRO ] | Section does not define a `home.md` page.                               | &nbsp;                                                                              |
| LINT-005 | [ WARN ] | Page does not link to any other pages.                                  |                                                                                     |
| LINT-006 | [ WARN ] | Page is orphaned as no other pages link to it.                                                                        |                                                                                     |

[0]: https://en.wikipedia.org/wiki/Lint_(software)
