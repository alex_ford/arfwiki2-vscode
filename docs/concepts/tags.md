# Tags

A tag is any single term (i.e. sequence of concatenated words) that starts with one or more special characters. Which special characters used defines the tag type. Each type can then be processed, filtered, sorted, enriched, etc. to help provide you with additional context.

[TOC]

## "!" ... "!"

## "@" People, Mentions, Contacts "@"

## "#" Generic Tag, Hashtag "#"

## "$" ... "$"

## "%" ... "%"

## "^" ... "^"

## "&" ... "&"

## "*" ... "*"

## ";"

## ":"

## "[]"

## "{}"

## "()"

## "<>"

## "+"

+hello
+testing
++hello
+++hello

## "\"

\hello

Reserved by most software to indicate "escaping" of a special character. Use this when you want to **avoid** processing, in most cases.

## "/"

/hello

## "|"

|hello
|testing

## What makes for a good tag?

- <i class="fa fa-plus" style="color: green"></i>
- <i class="fa fa-minus" style="color: red"></i>
-
