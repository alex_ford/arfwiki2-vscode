# Log Streams

Log Streams are "managed" sections of your wiki. They follow a specific directory structure format that is both easy and intuitive for organizing your entries into the Log Stream by time. You can have as many Log Streams as you want in your wiki, just use the reserved directory name `log-stream`.

The whole purpose of the Log Stream is to provide you an easy way to enter comments about whatever is on your mind, without having to worry about organization, classification, etc.
