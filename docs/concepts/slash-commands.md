# Slash Commands

<details><summary>Wiki Page Properties</summary>

```json
{
    date-created: '2018-10-05 09:16 EST',
    created-by: 'alex',
    created-host: 'bg-messier87',
}
```

</details>

- [Slash Commands](#slash-commands)
  - [Overview](#overview)
  - [Commands](#commands)

## Overview

If you're coming from the historic world of IRC and other chat systems, you might already be familiar with "slash commands" which look like this: `/command`

ARF Wiki 2 supports certain slash commands in order to accomplish or denote certain things. These slash commands are used by the wiki-processor in order to edit, index, or compute various things with respect to the wiki.

## Commands

| Slash Command | Description |
| ------------- | ----------- |
| `/end`        |             |
