#!/bin/bash

# --user-data-dir <dir>              Specifies the directory that
#                                     user data is kept in. Can be
#                                     used to open multiple distinct
#                                     instances of Code.
# --disable-extensions is a temporary operation that is meant for debugging and troubleshooting.
code --extensions-dir ~/.vscode/arfwiki-data/extensions/ \
     --user-data-dir ~/.vscode/arfwiki-data/ \
     ~/Workspaces-Personal/arfwiki2-vscode/arfwiki2.code-workspace \
     ~/Workspaces-Personal/arfwiki-data/home.md
