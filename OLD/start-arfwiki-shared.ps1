# NOT A PRIMARY SCRIPT! STOP GAP MEASURE UNTIL A BETTER SOLUTION IS FOUND.
# See 'start-arfwiki-shared.sh' for the primary script!


$WIKI_CODE_DIR = $PSScriptRoot
Write-Output "[ INFO ] WIKI_CODE_DIR is: ${WIKI_CODE_DIR}"
$WIKI_DATA_DIR = Join-Path -Resolve ${WIKI_CODE_DIR} ..\arfwiki-data
Write-Output "[ INFO ] WIKI_DATA_DIR is: ${WIKI_DATA_DIR}"
$WIKI_ASSETS_DIR = Join-Path -Resolve ${WIKI_CODE_DIR} ..\arfwiki-assets
Write-Output "[ INFO ] WIKI_ASSETS_DIR is: ${WIKI_ASSETS_DIR}"

# Intentional blank line
Write-Output ""

# Perform initial pull for all sections
Write-Output "[ INFO ] Updating various ARF Wiki 2 project directories..."
& Push-Location ${WIKI_CODE_DIR} ; git pull ; Pop-Location
& Push-Location ${WIKI_DATA_DIR} ; git pull ; Pop-Location
& Push-Location ${WIKI_ASSETS_DIR} ; git pull ; Pop-Location
Write-Output "[ INFO ] Done."

# Intentional blank line
Write-Output ""

# Launch VS Code
Write-Output "[ INFO ] Launching ARF Wiki 2 - VS Code!"
code --verbose `
    --extensions-dir ${WIKI_CODE_DIR}\.vscode\extensions `
    ${WIKI_CODE_DIR}\arfwiki2.code-workspace `
    ${WIKI_DATA_DIR}\home.md

    #--user-data-dir ${WIKI_CODE_DIR}\.vscode\user-data `