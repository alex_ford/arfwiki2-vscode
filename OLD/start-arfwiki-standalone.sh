#!/bin/bash

# --user-data-dir <dir>              Specifies the directory that
#                                     user data is kept in. Can be
#                                     used to open multiple distinct
#                                     instances of Code.
# --disable-extensions is a temporary operation that is meant for debugging and troubleshooting.
code --extensions-dir ~/.vscode/arfwiki-data/extensions/ \
     --user-data-dir ~/.vscode/arfwiki-data/ \
     ~/Workspaces-Personal/arfwiki2-vscode/arfwiki2.code-workspace \
     ~/Workspaces-Personal/arfwiki-data/home.md

# TODO: start code pointed at today's log-stream!
# TODO: create a separate script that launches bg's log-stream too!
# TODO: is there a way to save editor layouts and recall them so we can get back into things faster?
