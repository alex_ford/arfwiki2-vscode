#!/bin/bash

# Starts ARF Wiki 2 - VS Code using the "shared" context, which means that it
# will share the same user-data, extentions, and configurations with the user's
# regular VS Code.
#
# Author: Alex Ford (arf4188@gmail.com)
# Website: http://www.alexrichardford.com
# License: MIT License

# TODO: convert this script to Python and consolidate for all platforms!

# TODO: most of this script is generalizable! It should be the fundamental base
# for each of the "start-arfwiki-*.sh" scripts! OR - consolidate the 4 start-*
# scripts into one and use parameters/configs to control how it starts.

# TODO: make use of 'arflib-bash' for the standard bash logging

xtitle "term:arfwiki2"

WIKI_CODE_DIR="$(dirname $(realpath ${0}))"
echo "[ INFO ] WIKI_CODE_DIR is: ${WIKI_CODE_DIR}"
WIKI_ROOT_DIR="$(realpath ${WIKI_CODE_DIR}/../)"
echo "[ INFO ] WIKI_ROOT_DIR is: ${WIKI_ROOT_DIR}"
WIKI_DATA_DIR="${WIKI_ROOT_DIR}/arfwiki-data/"
echo "[ INFO ] WIKI_DATA_DIR is: ${WIKI_DATA_DIR}"
WIKI_ASSETS_DIR="${WIKI_ROOT_DIR}/arfwiki-assets/"
echo "[ INFO ] WIKI_ASSETS_DIR is: ${WIKI_ASSETS_DIR}"

# Get today's date in YYYY-MM-DD format
year="$(date +%Y)"
month="$(date +%m)"
day="$(date +%d)"
echo "[ INFO ] Determined today to be: ${year}-${month}-${day}"

# Build path to Log Stream file for today
generalLogStreamFile="${WIKI_DATA_DIR}/log-stream/${year}/${month}/${day}.md"
echo "[ INFO ] Using '${generalLogStreamFile}' for current general log stream page."
touch ${generalLogStreamFile}

# Build path to today's Berkshire Grey Log Stream file
bgLogStreamFile="${WIKI_DATA_DIR}/berkshire-grey/log-stream/${year}/${month}/${day}.md"
echo "[ INFO ] Using '${bgLogStreamFile}' for current Berkshire Grey log stream page."
touch ${bgLogStreamFile}

# TODO: Perform initial git-commit-sync on DATA and ASSETS directory

# TODO: Perform git pull on 'arfwiki2-vscode' to ensure we have the latest app files
# There's a chance changes to the app files will affect this script! So if
# changes are detected, abort and prompt user to re-launch manually.

# Launch VS Code
echo "[ INFO ] Launching ARF Wiki 2 - VS Code!"
code --verbose \
     --extensions-dir ${WIKI_CODE_DIR}/.vscode/extensions \
     ${WIKI_CODE_DIR}/arfwiki2.code-workspace \
     ${WIKI_DATA_DIR}/home.md \
     ${generalLogStreamFile} \
     ${bgLogStreamFile}
