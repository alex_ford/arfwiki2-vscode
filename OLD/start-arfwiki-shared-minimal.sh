#!/bin/bash

echo "[ INFO ] This is the SHARED instance of ARF Wiki 2, but uses a separately managed extensions directory tailored for ARF Wiki 2 purposes. User settings and some other things are shared across the system."


# Swich which of these two lines are enabled to control VS Code's verbosity
#VERBOSE_FLAG=""
VERBOSE_FLAG="--verbose"

code ${VERBOSE_FLAG} --extensions-dir ~/.vscode/extensions-arfwiki2/ \
     ~/Workspaces-Personal/arfwiki2-vscode/arfwiki2.code-workspace \
     ~/Workspaces-Personal/arfwiki-data/home.md

# TODO: consider moving the extensions "env" into the workspace root
